insert into `nastavnik`.`nastavnik` (biografija) values ('Removal of Internal Fixation Device from Thoracic Vertebra, Percutaneous Approach');
insert into `nastavnik`.`nastavnik` (biografija) values ('Occlusion of Left Posterior Tibial Artery with Intraluminal Device, Open Approach');
insert into `nastavnik`.`nastavnik` (biografija) values ('Revision of Nonautologous Tissue Substitute in Right Sternoclavicular Joint, Open Approach');
insert into `nastavnik`.`nastavnik` (biografija) values ('Excision of Oculomotor Nerve, Percutaneous Endoscopic Approach');
insert into `nastavnik`.`nastavnik` (biografija) values ('Dilation of Superior Mesenteric Artery with Four or More Drug-eluting Intraluminal Devices, Percutaneous Endoscopic Approach');

insert into `nastavnik`.`naucna_oblast` (naziv) values ('Mechanical Systems Engineer');
insert into `nastavnik`.`naucna_oblast` (naziv) values ('Senior Sales Associate');
insert into `nastavnik`.`naucna_oblast` (naziv) values ('Desktop Support Technician');
insert into `nastavnik`.`naucna_oblast` (naziv) values ('VP Marketing');
insert into `nastavnik`.`naucna_oblast` (naziv) values ('Information Systems Manager');

INSERT INTO `nastavnik`.`tip_zvanja` (`naziv`) VALUES ('profesor');
INSERT INTO `nastavnik`.`tip_zvanja` (`naziv`) VALUES ('asistent');

insert into `nastavnik`.`zvanje` (datum_izbora, datum_prestanka, nastavnik_id, naucna_oblast_id, tip_zvanja_id) values ('2022-02-03', '2020-03-31', 51, 1, 2);
insert into `nastavnik`.`zvanje` (datum_izbora, datum_prestanka, nastavnik_id, naucna_oblast_id, tip_zvanja_id) values ('2019-05-11', '2017-04-27', 52, 2, 1);
insert into `nastavnik`.`zvanje` (datum_izbora, datum_prestanka, nastavnik_id, naucna_oblast_id, tip_zvanja_id) values ('2003-09-29', '2003-02-23', 53, 3, 2);
insert into `nastavnik`.`zvanje` (datum_izbora, datum_prestanka, nastavnik_id, naucna_oblast_id, tip_zvanja_id) values ('2005-07-19', '2017-09-28', 54, 4, 2);
insert into `nastavnik`.`zvanje` (datum_izbora, datum_prestanka, nastavnik_id, naucna_oblast_id, tip_zvanja_id) values ('2003-05-15', '2021-04-29', 55, 5, 1);
