package com.example.nastavnik.controller.impl;

import com.example.nastavnik.controller.base.BaseController;
import com.example.nastavnik.dto.nastavnik.NastavnikDTO;
import com.example.nastavnik.entity.Nastavnik;
import com.example.nastavnik.service.impl.NastavnikService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/nastavnik/nastavnici")
public class NastavnikController extends BaseController<Nastavnik, NastavnikDTO, Long> {
    public NastavnikController(NastavnikService service) {
        super(service);
    }
}
