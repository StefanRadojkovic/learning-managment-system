package com.example.nastavnik.repository.impl;


import com.example.nastavnik.entity.Zvanje;
import com.example.nastavnik.repository.base.BaseRepository;

public interface ZvanjeRepository extends BaseRepository<Zvanje, Long> {
}
