package com.example.nastavnik.controller.impl;

import com.example.nastavnik.controller.base.BaseController;
import com.example.nastavnik.dto.zvanje.ZvanjeDTO;
import com.example.nastavnik.entity.Zvanje;
import com.example.nastavnik.service.impl.ZvanjeService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/nastavnik/zvanja")
public class ZvanjeController extends BaseController<Zvanje, ZvanjeDTO, Long> {
    public ZvanjeController(ZvanjeService service) {
        super(service);
    }
}
