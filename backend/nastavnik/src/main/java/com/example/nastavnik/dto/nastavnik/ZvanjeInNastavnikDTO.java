package com.example.nastavnik.dto.nastavnik;

import com.example.nastavnik.entity.Nastavnik;
import com.example.nastavnik.entity.NaucnaOblast;
import com.example.nastavnik.entity.TipZvanja;
import com.example.nastavnik.entity.Zvanje;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ZvanjeInNastavnikDTO {
    private Long id;
    private LocalDate datumIzbora;
    private LocalDate datumPrestanka;
    private TipZvanjaInNastavnikDTO tipZvanja;
    private NaucnaOblastInNastavnikDTO naucnaOblast;

    public ZvanjeInNastavnikDTO(Zvanje zvanje) {
        id = zvanje.getId();
        datumIzbora = zvanje.getDatumIzbora();
        datumPrestanka = zvanje.getDatumPrestanka();
        tipZvanja = new TipZvanjaInNastavnikDTO(zvanje.getTipZvanja());
        naucnaOblast = new NaucnaOblastInNastavnikDTO(zvanje.getNaucnaOblast());
    }
}

