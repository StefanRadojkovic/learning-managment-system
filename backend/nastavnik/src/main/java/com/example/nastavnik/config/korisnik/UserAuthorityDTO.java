package com.example.nastavnik.config.korisnik;

import com.example.nastavnik.config.korisnik.surrogate.AuthorityDTOinUser;
import com.example.nastavnik.config.korisnik.surrogate.UserDTOinAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserAuthorityDTO {
    private Long id;
    private UserDTOinAuthority user;
    private AuthorityDTOinUser authority;
}
