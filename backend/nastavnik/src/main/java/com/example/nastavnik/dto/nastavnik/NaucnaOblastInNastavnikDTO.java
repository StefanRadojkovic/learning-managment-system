package com.example.nastavnik.dto.nastavnik;

import com.example.nastavnik.entity.NaucnaOblast;
import com.example.nastavnik.entity.Zvanje;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class NaucnaOblastInNastavnikDTO {
    private Long id;
    private String naziv;

    public NaucnaOblastInNastavnikDTO(NaucnaOblast naucnaOblast) {
        id = naucnaOblast.getId();
        naziv = naucnaOblast.getNaziv();
    }
}

