package com.example.nastavnik.repository.impl;


import com.example.nastavnik.entity.TipZvanja;
import com.example.nastavnik.repository.base.BaseRepository;

public interface TipZvanjaRepository extends BaseRepository<TipZvanja, Long> {
}
