package com.example.nastavnik.service.impl;

import com.example.nastavnik.config.korisnik.UserDTO;
import com.example.nastavnik.config.security.UsernamePasswordAuthenticationWithToken;
import com.example.nastavnik.dto.nastavnik.NastavnikDTO;
import com.example.nastavnik.entity.Nastavnik;
import com.example.nastavnik.repository.impl.NastavnikRepository;
import com.example.nastavnik.service.base.BaseService;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class NastavnikService extends BaseService<Nastavnik, NastavnikDTO, Long> {

    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public NastavnikService(NastavnikRepository repository, WebClient webClient, EurekaClient eurekaClient) {
        super(repository);
        this.webClient = webClient;
        this.eurekaClient = eurekaClient;
    }

    @Override
    public NastavnikDTO convertToDTO(Nastavnik element) {
        NastavnikDTO nastavnikDTO = new NastavnikDTO(element);

        InstanceInfo nastavnik = eurekaClient.getApplication("korisnik").getInstances().get(0);
        UserDTO userDTO;
        if(!"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
            UsernamePasswordAuthenticationWithToken authentication = (UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication();

            userDTO = webClient.get()
                    .uri(String.format(
                            "%sapi/korisnik/users/%s",
                            nastavnik.getHomePageUrl(),
                            element.getId()))
                    .header("Authorization", String.format("Bearer %s", authentication.getToken()))
                    .retrieve().bodyToMono(UserDTO.class).block();
        }else {
            userDTO = webClient.get()
                    .uri(String.format(
                            "%sapi/korisnik/users/%s",
                            nastavnik.getHomePageUrl(),
                            element.getId()))
                    .retrieve().bodyToMono(UserDTO.class).block();
        }

        nastavnikDTO.setIme(userDTO.getIme());
        nastavnikDTO.setJmbg(userDTO.getJmbg());
        nastavnikDTO.setUsername(userDTO.getUsername());

        return nastavnikDTO;
    }
}
