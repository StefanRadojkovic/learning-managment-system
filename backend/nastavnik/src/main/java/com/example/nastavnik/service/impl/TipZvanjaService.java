package com.example.nastavnik.service.impl;

import com.example.nastavnik.dto.tip.zvanja.TipZvanjaDTO;
import com.example.nastavnik.entity.TipZvanja;
import com.example.nastavnik.repository.impl.TipZvanjaRepository;
import com.example.nastavnik.service.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class TipZvanjaService extends BaseService<TipZvanja, TipZvanjaDTO, Long> {
    public TipZvanjaService(TipZvanjaRepository repository) {
        super(repository);
    }

    @Override
    public TipZvanjaDTO convertToDTO(TipZvanja element) {
        return new TipZvanjaDTO(element);
    }
}
