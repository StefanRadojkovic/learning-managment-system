package com.example.nastavnik.repository.impl;


import com.example.nastavnik.entity.NaucnaOblast;
import com.example.nastavnik.repository.base.BaseRepository;

public interface NaucnaOblastRepository extends BaseRepository<NaucnaOblast, Long> {
}
