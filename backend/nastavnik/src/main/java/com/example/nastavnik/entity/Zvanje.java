package com.example.nastavnik.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Zvanje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate datumIzbora;
    private LocalDate datumPrestanka;

    @ManyToOne
    private Nastavnik nastavnik;

    @ManyToOne
    private TipZvanja tipZvanja;

    @ManyToOne
    private NaucnaOblast naucnaOblast;
}

