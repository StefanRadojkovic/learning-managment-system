package com.example.nastavnik.repository.impl;


import com.example.nastavnik.entity.Nastavnik;
import com.example.nastavnik.repository.base.BaseRepository;

public interface NastavnikRepository extends BaseRepository<Nastavnik, Long> {
}
