package com.example.nastavnik.dto.naucna.oblast;

import com.example.nastavnik.entity.NaucnaOblast;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NaucnaOblastDTO {
    private Long id;
    private String naziv;

    public NaucnaOblastDTO(NaucnaOblast naucnaOblast) {
        id = naucnaOblast.getId();
        naziv = naucnaOblast.getNaziv();
    }
}

