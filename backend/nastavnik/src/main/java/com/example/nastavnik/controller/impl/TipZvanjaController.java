package com.example.nastavnik.controller.impl;

import com.example.nastavnik.controller.base.BaseController;
import com.example.nastavnik.dto.tip.zvanja.TipZvanjaDTO;
import com.example.nastavnik.entity.TipZvanja;
import com.example.nastavnik.service.impl.TipZvanjaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/nastavnik/tipovi-zvanja")
public class TipZvanjaController extends BaseController<TipZvanja, TipZvanjaDTO, Long> {
    public TipZvanjaController(TipZvanjaService service) {
        super(service);
    }
}
