package com.example.nastavnik.dto.zvanje;

import com.example.nastavnik.dto.naucna.oblast.NaucnaOblastDTO;
import com.example.nastavnik.dto.tip.zvanja.TipZvanjaDTO;
import com.example.nastavnik.entity.Zvanje;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ZvanjeDTO {
    private Long id;
    private LocalDate datumIzbora;
    private LocalDate datumPrestanka;
    private NastavnikInZvanjeDTO nastavnik;
    private TipZvanjaDTO tipZvanja;
    private NaucnaOblastDTO naucnaOblast;

    public ZvanjeDTO(Zvanje zvanje) {
        id = zvanje.getId();
        datumIzbora = zvanje.getDatumIzbora();
        datumPrestanka = zvanje.getDatumPrestanka();
        nastavnik = new NastavnikInZvanjeDTO(zvanje.getNastavnik());
        tipZvanja = new TipZvanjaDTO(zvanje.getTipZvanja());
        naucnaOblast = new NaucnaOblastDTO(zvanje.getNaucnaOblast());
    }
}

