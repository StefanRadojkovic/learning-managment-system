package com.example.nastavnik.service.impl;

import com.example.nastavnik.dto.zvanje.ZvanjeDTO;
import com.example.nastavnik.entity.Zvanje;
import com.example.nastavnik.repository.impl.ZvanjeRepository;
import com.example.nastavnik.service.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ZvanjeService extends BaseService<Zvanje, ZvanjeDTO, Long> {
    public ZvanjeService(ZvanjeRepository repository) {
        super(repository);
    }

    @Override
    public ZvanjeDTO convertToDTO(Zvanje element) {
        return new ZvanjeDTO(element);
    }
}
