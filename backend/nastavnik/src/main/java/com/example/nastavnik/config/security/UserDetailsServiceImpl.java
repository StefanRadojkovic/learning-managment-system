package com.example.nastavnik.config.security;

import com.example.nastavnik.config.korisnik.UserDTO;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public UserDetailsServiceImpl(WebClient webClient, EurekaClient eurekaClient) {
        this.webClient = webClient;
        this.eurekaClient = eurekaClient;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        InstanceInfo korisnik = eurekaClient.getApplication("korisnik").getInstances().get(0);

        UserDTO user = webClient.get()
                .uri(String.format(
                        "%sapi/korisnik/users?username=%s",
                        korisnik.getHomePageUrl(),
                        username)
                ).retrieve().bodyToMono(UserDTO.class).block();

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();

        user.getAuthorities().forEach(userAuthority -> authorities.add(
                new SimpleGrantedAuthority(
                        userAuthority.getName()))
        );

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }
}
