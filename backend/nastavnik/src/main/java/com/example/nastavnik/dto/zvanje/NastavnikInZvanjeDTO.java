package com.example.nastavnik.dto.zvanje;

import com.example.nastavnik.entity.Nastavnik;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NastavnikInZvanjeDTO {
    private Long id;
    private String ime;
    private String username;
    private String biografija;
    private String jmbg;

    public NastavnikInZvanjeDTO(Nastavnik nastavnik) {
        id = nastavnik.getId();
        biografija = nastavnik.getBiografija();
    }
}

