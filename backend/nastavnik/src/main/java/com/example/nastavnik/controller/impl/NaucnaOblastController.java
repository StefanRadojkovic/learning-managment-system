package com.example.nastavnik.controller.impl;

import com.example.nastavnik.controller.base.BaseController;
import com.example.nastavnik.dto.naucna.oblast.NaucnaOblastDTO;
import com.example.nastavnik.entity.NaucnaOblast;
import com.example.nastavnik.service.impl.NaucnaOblastService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/nastavnik/naucne-oblasti")
public class NaucnaOblastController extends BaseController<NaucnaOblast, NaucnaOblastDTO, Long> {
    public NaucnaOblastController(NaucnaOblastService service) {
        super(service);
    }
}
