package com.example.nastavnik.service.impl;

import com.example.nastavnik.dto.naucna.oblast.NaucnaOblastDTO;
import com.example.nastavnik.entity.NaucnaOblast;
import com.example.nastavnik.repository.impl.NaucnaOblastRepository;
import com.example.nastavnik.service.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class NaucnaOblastService extends BaseService<NaucnaOblast, NaucnaOblastDTO, Long> {
    public NaucnaOblastService(NaucnaOblastRepository repository) {
        super(repository);
    }

    @Override
    public NaucnaOblastDTO convertToDTO(NaucnaOblast element) {
        return new NaucnaOblastDTO(element);
    }
}
