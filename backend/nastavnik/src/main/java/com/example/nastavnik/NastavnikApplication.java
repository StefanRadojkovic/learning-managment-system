package com.example.nastavnik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NastavnikApplication {

	public static void main(String[] args) {
		SpringApplication.run(NastavnikApplication.class, args);
	}

}
