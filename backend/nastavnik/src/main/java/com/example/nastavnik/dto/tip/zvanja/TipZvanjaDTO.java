package com.example.nastavnik.dto.tip.zvanja;

import com.example.nastavnik.entity.TipZvanja;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TipZvanjaDTO {
    private Long id;
    private String naziv;

    public TipZvanjaDTO(TipZvanja tipZvanja) {
        id = tipZvanja.getId();
        naziv = tipZvanja.getNaziv();
    }
}

