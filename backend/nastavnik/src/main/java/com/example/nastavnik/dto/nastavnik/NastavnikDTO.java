package com.example.nastavnik.dto.nastavnik;

import com.example.nastavnik.dto.zvanje.ZvanjeDTO;
import com.example.nastavnik.entity.Nastavnik;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NastavnikDTO {
    private Long id;
    private String ime;
    private String username;
    private String biografija;
    private String jmbg;
    List<ZvanjeInNastavnikDTO> zvanja = new ArrayList<>();

    public NastavnikDTO(Nastavnik nastavnik) {
        id = nastavnik.getId();
        biografija = nastavnik.getBiografija();
        nastavnik.getZvanja().forEach(zvanje -> zvanja.add(new ZvanjeInNastavnikDTO(zvanje)));
    }
}

