package com.example.univerzitet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NazivAlreadyExistException extends RuntimeException{
    public NazivAlreadyExistException() {
        super("Naziv vec postoji");
    }
}
