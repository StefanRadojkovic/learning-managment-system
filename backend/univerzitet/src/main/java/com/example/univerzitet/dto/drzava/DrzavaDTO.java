package com.example.univerzitet.dto.drzava;

import com.example.univerzitet.entity.Drzava;
import com.example.univerzitet.entity.Mesto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class DrzavaDTO {
    private Long id;
    private String name;
    private List<MestoInDrzavaDTO> mesta = new ArrayList<>();

    public DrzavaDTO(Drzava drzava) {
        this.id = drzava.getId();
        this.name = drzava.getNaziv();
        this.mesta = this.getList(drzava.getMesta());
    }
    private List<MestoInDrzavaDTO> getList(List<Mesto> mesta){
        List<MestoInDrzavaDTO> list = new ArrayList<>();
        mesta.forEach(mesto -> list.add(new MestoInDrzavaDTO(mesto)));
        return list;
    }
}
