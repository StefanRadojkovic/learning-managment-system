package com.example.univerzitet.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Fakultet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long dekanId;
    private String naziv;
    @ToString.Exclude
    @OneToMany(mappedBy = "fakultet")
    private List<StudijskiProgram> studijskiProgrami = new ArrayList<>();
    @ToString.Exclude
    @ManyToOne
    private Univerzitet univerzitet;

    @ToString.Exclude
    @ManyToOne
    private Adresa adresa;
}

