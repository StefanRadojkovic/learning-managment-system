package com.example.univerzitet.dto.nastavnik.zvanje;

import com.example.univerzitet.dto.nastavnik.oblast.NaucnaOblastDTO;
import com.example.univerzitet.dto.nastavnik.zvanja.TipZvanjaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ZvanjeDTO {
    private Long id;
    private LocalDate datumIzbora;
    private LocalDate datumPrestanka;
    private TipZvanjaDTO tipZvanja;
    private NaucnaOblastDTO naucnaOblast;
}

