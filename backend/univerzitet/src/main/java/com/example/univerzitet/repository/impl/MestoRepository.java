package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.Mesto;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface MestoRepository extends BaseRepository<Mesto, Long> {
    Optional<Mesto> findByNaziv(String naziv);
    List<Mesto> findAllByNaziv(String naziv);
    List<Mesto> findAllByDrzava_Naziv(String naziv);
    List<Mesto> findAllByDrzava_Id(Long id);
}
