package com.example.univerzitet.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Univerzitet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naziv;
    private LocalDate datumOsnivanja;
    private Long rektorId;

    @OneToMany(mappedBy = "univerzitet")
    private List<Fakultet> fakulteti = new ArrayList<>();

    @ManyToOne
    private Adresa adresa;
}

