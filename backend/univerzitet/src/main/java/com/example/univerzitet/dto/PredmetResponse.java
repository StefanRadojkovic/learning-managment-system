package com.example.univerzitet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetResponse {
    private String naziv;
    private Integer espb;
    private String opis;

    public PredmetResponse(Object pred, String opis1) {
        LinkedHashMap linkedHashMap = (LinkedHashMap) pred;
        naziv = (String) linkedHashMap.get("naziv");
        espb = (Integer) linkedHashMap.get("espb");
        opis = opis1;
    }
}
