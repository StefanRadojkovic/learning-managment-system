package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.PredmetResponse;
import com.example.univerzitet.dto.studijski.program.StudijskiProgramDTO;
import com.example.univerzitet.dto.studijski.program.StudijskiProgramSimpleDTO;
import com.example.univerzitet.entity.StudijskiProgram;
import com.example.univerzitet.service.impl.StudijskiProgramService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/univerzitet/studijski-programi")
public class StudijskiProgramController extends BaseController<StudijskiProgram, StudijskiProgramDTO, Long> {

    private StudijskiProgramService studijskiProgramService;

    public StudijskiProgramController(StudijskiProgramService service) {
        super(service);
        studijskiProgramService = service;
    }

    @GetMapping("fakultet/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<StudijskiProgramSimpleDTO> findAllByFakultetId(@PathVariable("id") Long id)
    {
        return studijskiProgramService.findAllByFakultetId(id);
    }

    @GetMapping("predmeti/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PredmetResponse> findAllByStudijskiProgramId(@PathVariable("id") Long id)
    {
        return studijskiProgramService.findAllByStudijskiProgramId(id);
    }

    @GetMapping(path = "/fakultet/{id}/find/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public List<StudijskiProgramSimpleDTO> findAllByNaziv(@PathVariable("id") Long id, @PathVariable("naziv") String naziv){
        if(naziv.isEmpty()){
            return studijskiProgramService.findAllByFakultetId(id);
        }
        return studijskiProgramService.findAllByNaziv(id, naziv);
    }

    @PostMapping(path = "fakultet/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void findAllByNaziv(@PathVariable("id") Long id, @RequestBody StudijskiProgramSimpleDTO studijskiProgramSimpleDTO){
        studijskiProgramService.createStudProgram(id, studijskiProgramSimpleDTO);
    }

    @PutMapping(path = "update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateStudProgram(@PathVariable("id") Long id, @RequestBody StudijskiProgramSimpleDTO studijskiProgramSimpleDTO){
        studijskiProgramService.updateStudProgram(id, studijskiProgramSimpleDTO);
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudProgram(@PathVariable("id") Long id)
    {
        studijskiProgramService.deleteStudProgram(id);
    }
}
