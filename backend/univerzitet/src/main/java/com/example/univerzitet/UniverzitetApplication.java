package com.example.univerzitet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniverzitetApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniverzitetApplication.class, args);
	}

}
