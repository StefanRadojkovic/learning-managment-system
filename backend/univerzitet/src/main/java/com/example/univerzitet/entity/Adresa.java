package com.example.univerzitet.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Adresa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ulica;
    private String broj;
    @ManyToOne
    private Mesto mesto;

    @OneToMany(mappedBy = "adresa")
    List<Univerzitet> univerziteti = new ArrayList<>();

    @OneToMany(mappedBy = "adresa")
    List<Fakultet> fakulteti = new ArrayList<>();
}

