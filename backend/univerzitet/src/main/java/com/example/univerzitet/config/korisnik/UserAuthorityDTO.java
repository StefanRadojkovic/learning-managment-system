package com.example.univerzitet.config.korisnik;

import com.example.univerzitet.config.korisnik.surrogate.AuthorityDTOinUser;
import com.example.univerzitet.config.korisnik.surrogate.UserDTOinAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserAuthorityDTO {
    private Long id;
    private UserDTOinAuthority user;
    private AuthorityDTOinUser authority;
}
