package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;

public interface AdresaRepository extends BaseRepository<Adresa, Long> {
    List<Adresa> findAllByUlicaAndBroj(String naziv, String broj);
    List<Adresa> findAllByMesto_Naziv(String naziv);
    List<Adresa> findAllByMesto_Id(Long id);
}
