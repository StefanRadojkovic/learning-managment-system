package com.example.univerzitet.dto.nastavnik;

import com.example.univerzitet.dto.nastavnik.zvanje.ZvanjeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class NastavnikDTO {
    private Long id;
    private String ime;
    private String username;
    private String biografija;
    private String jmbg;
    List<ZvanjeDTO> zvanja;
}

