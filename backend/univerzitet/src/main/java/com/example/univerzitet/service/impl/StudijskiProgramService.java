package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.PredmetResponse;
import com.example.univerzitet.dto.studijski.program.StudijskiProgramDTO;
import com.example.univerzitet.dto.studijski.program.StudijskiProgramSimpleDTO;
import com.example.univerzitet.entity.Fakultet;
import com.example.univerzitet.entity.StudijskiProgram;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.repository.impl.FakultetRepository;
import com.example.univerzitet.repository.impl.StudijskiProgramRepository;
import com.example.univerzitet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class StudijskiProgramService extends BaseService<StudijskiProgram, StudijskiProgramDTO, Long> {

    private final NastavnikService nastavnikService;
    private final PredmetService predmetService;
    private final StudijskiProgramRepository studijskiProgramRepository;
    private final FakultetRepository fakultetRepository;

    public StudijskiProgramService(StudijskiProgramRepository repository, NastavnikService nastavnikService, PredmetService predmetService, FakultetRepository fakultetRepository) {
        super(repository);
        this.nastavnikService = nastavnikService;
        this.predmetService = predmetService;
        studijskiProgramRepository = repository;
        this.fakultetRepository = fakultetRepository;
    }

    @Override
    public StudijskiProgramDTO convertToDTO(StudijskiProgram element) {
        StudijskiProgramDTO studijskiProgramDTO = new StudijskiProgramDTO(element);


        if(studijskiProgramDTO.getFakultet().getDekan().getId() != null) {
            studijskiProgramDTO.getFakultet().setDekan(nastavnikService.getNastavnikById(studijskiProgramDTO.getFakultet().getDekan().getId()));
        }else {
            studijskiProgramDTO.getFakultet().setDekan(null);
        }

        if(studijskiProgramDTO.getRukovodilac().getId() != null) {
            studijskiProgramDTO.setRukovodilac(nastavnikService.getNastavnikById(studijskiProgramDTO.getRukovodilac().getId()));
        }else {
            studijskiProgramDTO.setRukovodilac(null);
        }
        studijskiProgramDTO.getFakultet().getUniverzitet().setRektor(nastavnikService.getNastavnikById(studijskiProgramDTO.getFakultet().getUniverzitet().getRektor().getId()));
        studijskiProgramDTO.setGodineStudija(predmetService.getGodineStudijaById(studijskiProgramDTO.getId()));
        return studijskiProgramDTO;
    }

    public List<StudijskiProgramSimpleDTO> findAllByFakultetId(Long id){
        return generateListSimpleDto(studijskiProgramRepository.findAllByFakultet_Id(id));
    }

    public StudijskiProgramSimpleDTO convertToSimpleDTO(StudijskiProgram element) {
        return new StudijskiProgramSimpleDTO(element);
    }

    public List<StudijskiProgramSimpleDTO> generateListSimpleDto(Iterable<StudijskiProgram> elements) {
        List<StudijskiProgramSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }

    public List<PredmetResponse> findAllByStudijskiProgramId(Long id) {
        StudijskiProgramDTO studijskiProgramDTOS = convertToDTO(studijskiProgramRepository.findById(id).get());
        List<PredmetResponse> lista = new ArrayList<>();
        List<LinkedHashMap> godineStudijaById = predmetService.getGodineStudijaById(id);

        godineStudijaById.forEach(god -> {
            List list = (List) god.get("predmeti");
            list.forEach(pred -> lista.add(new PredmetResponse(pred, studijskiProgramDTOS.getOpis())));
        });

        return lista;
    }

    public List<StudijskiProgramSimpleDTO> findAllByNaziv(Long id, String naziv) {
        List<StudijskiProgramSimpleDTO> lista = findAllByFakultetId(id);
        return lista.stream().filter( uni -> uni.getNaziv().toLowerCase(Locale.ROOT).contains(naziv.toLowerCase(Locale.ROOT))).collect(Collectors.toList());
    }

    public void deleteStudProgram(Long id) {
        StudijskiProgram studijskiProgram = studijskiProgramRepository.findById(id).get();
        if(studijskiProgram.getRukovodilacId() != null){
            throw new KoristiSeNegdeException();
        } else if(!predmetService.getGodineStudijaById(id).isEmpty()){
            throw new KoristiSeNegdeException();
        }
        studijskiProgramRepository.deleteById(id);
    }

    public void createStudProgram(Long id, StudijskiProgramSimpleDTO studijskiProgramSimpleDTO) {
        Fakultet fakultet = fakultetRepository.findById(id).get();

        StudijskiProgram studijskiProgram = new StudijskiProgram();
        studijskiProgram.setFakultet(fakultet);
        studijskiProgram.setNaziv(studijskiProgramSimpleDTO.getNaziv());
        studijskiProgram.setOpis(studijskiProgramSimpleDTO.getOpis());
        studijskiProgram.setRukovodilacId(null);

        studijskiProgramRepository.save(studijskiProgram);
    }

    public void updateStudProgram(Long id, StudijskiProgramSimpleDTO studijskiProgramSimpleDTO) {
        StudijskiProgram studijskiProgram = studijskiProgramRepository.findById(id).get();
        if(!studijskiProgramSimpleDTO.getNaziv().isEmpty()) {
            studijskiProgram.setNaziv(studijskiProgramSimpleDTO.getNaziv());
        }
        if(!studijskiProgramSimpleDTO.getOpis().isEmpty()) {
            studijskiProgram.setOpis(studijskiProgramSimpleDTO.getOpis());
        }

        studijskiProgramRepository.save(studijskiProgram);
    }
}
