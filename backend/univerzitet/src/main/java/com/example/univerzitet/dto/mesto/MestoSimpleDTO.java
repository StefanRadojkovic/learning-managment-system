package com.example.univerzitet.dto.mesto;

import com.example.univerzitet.entity.Mesto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MestoSimpleDTO {
    private Long id;
    private String naziv;

    public MestoSimpleDTO(Mesto mesto) {
        id = mesto.getId();
        naziv = mesto.getNaziv();
    }
}

