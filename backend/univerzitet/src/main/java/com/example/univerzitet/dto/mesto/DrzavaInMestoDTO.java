package com.example.univerzitet.dto.mesto;

import com.example.univerzitet.entity.Drzava;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class DrzavaInMestoDTO {
    private Long id;
    private String naziv;

    public DrzavaInMestoDTO(Drzava drzava) {
        id = drzava.getId();
        naziv = drzava.getNaziv();
    }
}

