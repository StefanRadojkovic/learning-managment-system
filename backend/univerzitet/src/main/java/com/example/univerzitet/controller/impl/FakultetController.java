package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.fakultet.FakultetDTO;
import com.example.univerzitet.dto.fakultet.FakultetSimpleDTO;
import com.example.univerzitet.entity.Fakultet;
import com.example.univerzitet.service.impl.FakultetService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/univerzitet/fakulteti")
public class FakultetController extends BaseController<Fakultet, FakultetDTO, Long> {

    private FakultetService fakultetService;

    public FakultetController(FakultetService service) {
        super(service);
        fakultetService = service;
    }

    @GetMapping(path = "/univerzitet/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<FakultetSimpleDTO> findAllByUniverzitet(@PathVariable("id") Long id){
        return fakultetService.findAllByUniverzitet(id);
    }

    @GetMapping(path = "/univerzitet/{id}/find/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public List<FakultetSimpleDTO> findAllByNaziv(@PathVariable("id") Long id, @PathVariable("naziv") String naziv){
        if(naziv.isEmpty()){
            return fakultetService.findAllByUniverzitet(id);
        }
        return fakultetService.findAllByNaziv(id, naziv);
    }


    @PutMapping("update/{id}/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public FakultetDTO update(@PathVariable("id") Long id, @PathVariable("naziv") String naziv, @RequestBody String adresa)
    {
        return fakultetService.update(id, naziv, adresa);
    }

    @PostMapping("create/{id}/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public FakultetDTO createUni(@PathVariable("id") Long id, @PathVariable("naziv") String naziv, @RequestBody String adresa)
    {
        return fakultetService.createFakultet(id, naziv, adresa);
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFakultet(@PathVariable("id") Long id)
    {
        fakultetService.deleteFakultet(id);
    }
}
