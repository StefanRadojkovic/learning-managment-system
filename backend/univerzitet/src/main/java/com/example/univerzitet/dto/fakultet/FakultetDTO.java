package com.example.univerzitet.dto.fakultet;

import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.Fakultet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FakultetDTO {
    private Long id;
    private NastavnikDTO dekan;
    private String naziv;
    private List<StudijskiProgramInFakultetDTO> studijskiProgrami = new ArrayList<>();
    private UniverzitetInFakultetDTO univerzitet;
    private AdresaDTO adresa;

    public FakultetDTO(Fakultet fakultet) {
        id = fakultet.getId();
        naziv = fakultet.getNaziv();
        dekan = NastavnikDTO.builder().id(fakultet.getDekanId()).build();
        fakultet.getStudijskiProgrami().forEach(studijskiProgram -> studijskiProgrami.add(new StudijskiProgramInFakultetDTO(studijskiProgram)));
        univerzitet = new UniverzitetInFakultetDTO(fakultet.getUniverzitet());
        adresa = new AdresaDTO(fakultet.getAdresa());
    }
}

