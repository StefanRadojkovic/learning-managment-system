package com.example.univerzitet.dto.studija;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeInPredmetDTO {
    private Long id;
    private String opis;
}

