package com.example.univerzitet.dto.univerzitet;

import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.Univerzitet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UniverzitetAllDTO {

    private Long id;
    private String naziv;
    private LocalDate datumOsnivanja;
    private AdresaDTO adresa;

    public UniverzitetAllDTO(Univerzitet univerzitet) {
        id = univerzitet.getId();
        naziv = univerzitet.getNaziv();
        datumOsnivanja = univerzitet.getDatumOsnivanja();
        adresa = new AdresaDTO(univerzitet.getAdresa());
    }
}

