package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.Drzava;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface DrzavaRepository extends BaseRepository<Drzava, Long> {
    Optional<Drzava> findByNaziv(String naziv);
    List<Drzava> findAllByNaziv(String naziv);
}
