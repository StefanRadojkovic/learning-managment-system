package com.example.univerzitet.dto.adresa;

import com.example.univerzitet.dto.mesto.DrzavaInMestoDTO;
import com.example.univerzitet.entity.Mesto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MestoInAdresaDTO {
    private Long id;
    private String naziv;
    private DrzavaInMestoDTO drzava;;

    public MestoInAdresaDTO(Mesto mesto) {
        id = mesto.getId();
        naziv = mesto.getNaziv();
        drzava = new DrzavaInMestoDTO(mesto.getDrzava());
    }
}

