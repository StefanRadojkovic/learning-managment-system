package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.mesto.MestoDTO;
import com.example.univerzitet.dto.mesto.MestoSimpleDTO;
import com.example.univerzitet.entity.Mesto;
import com.example.univerzitet.service.impl.MestoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "api/univerzitet/mesta")
public class MestoController extends BaseController<Mesto, MestoDTO, Long> {

    private MestoService mestoService;

    public MestoController(MestoService service) {
        super(service);
        mestoService = service;
    }

    @PostMapping("/drzava/{drzavaId}/{naziv}")
    @ResponseStatus(HttpStatus.CREATED)
    public MestoDTO createMesto(@PathVariable("drzavaId") Long drzavaId, @PathVariable("naziv") String naziv)
    {
        return mestoService.createMesto(drzavaId, naziv);
    }

    @PutMapping("/{id}/{naziv}")
    @ResponseStatus(HttpStatus.CREATED)
    public MestoDTO updateMesto(@PathVariable("id") Long id, @PathVariable("naziv") String naziv)
    {
        return mestoService.updateMesto(id, naziv);
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMesto(@PathVariable("id") Long id)
    {
        mestoService.deleteMesto(id);
    }

    @GetMapping("drzava/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public List<MestoSimpleDTO> findAllByDrzava(@PathVariable("naziv") String naziv)
    {
        return mestoService.findAllByDrzava(naziv);
    }

    @GetMapping("drzavaId/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<MestoSimpleDTO> findAllByDrzava(@PathVariable("id") Long id)
    {
        return mestoService.findAllByDrzavaId(id);
    }
}
