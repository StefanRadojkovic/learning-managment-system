package com.example.univerzitet.dto.drzava;

import com.example.univerzitet.entity.Mesto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MestoInDrzavaDTO {
    private Long id;
    private String naziv;

    public MestoInDrzavaDTO(Mesto mesto) {
        id = mesto.getId();
        naziv = mesto.getNaziv();
    }
}

