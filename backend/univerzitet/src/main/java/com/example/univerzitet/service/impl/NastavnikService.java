package com.example.univerzitet.service.impl;

import com.example.univerzitet.config.security.UsernamePasswordAuthenticationWithToken;
import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@RequiredArgsConstructor
public class NastavnikService {
    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public NastavnikDTO getNastavnikById(Long id){
        if(id == null){
            return null;
        }
        InstanceInfo nastavnik = eurekaClient.getApplication("nastavnik").getInstances().get(0);
        if(!"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
            UsernamePasswordAuthenticationWithToken authentication = (UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication();

            return webClient.get()
                    .uri(String.format(
                            "%sapi/nastavnik/nastavnici/%s",
                            nastavnik.getHomePageUrl(),
                            id))
                    .header("Authorization", String.format("Bearer %s", authentication.getToken()))
                    .retrieve().bodyToMono(NastavnikDTO.class).block();
        }
        return webClient.get()
                .uri(String.format(
                        "%sapi/nastavnik/nastavnici/%s",
                        nastavnik.getHomePageUrl(),
                        id))
                .retrieve().bodyToMono(NastavnikDTO.class).block();
    }
}
