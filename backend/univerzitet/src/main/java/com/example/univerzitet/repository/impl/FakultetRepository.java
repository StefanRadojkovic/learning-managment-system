package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.Fakultet;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;

public interface FakultetRepository extends BaseRepository<Fakultet, Long> {

    List<Fakultet> findAllByNazivLike(String naziv);
    List<Fakultet> findAllByUniverzitet_Id(Long id);
}
