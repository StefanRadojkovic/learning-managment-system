package com.example.univerzitet.service.base;

import com.example.univerzitet.repository.base.BaseRepository;
import lombok.Getter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class BaseService<T, P, I>{
    private final BaseRepository<T, I> repository;
    @PersistenceContext
    protected EntityManager entityManager;

    public BaseService(BaseRepository<T, I> repository) {
        this.repository = repository;
    }

    public abstract P convertToDTO(T element);

    public List<P> generateList(Iterable<T> elements) {
        List<P> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(this.convertToDTO(element));

        });
        return newList;
    }

    public List<P> findAll(){
        return this.generateList(repository.findAll());
    }

    public P findOne(I id){
        return this.convertToDTO(repository.getById(id));
    }

    public void delete(I id){
        repository.deleteById(id);
    }

    public P save(T expenseGroup){
        return this.convertToDTO(repository.save(expenseGroup));
    }
}
