package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.dto.univerzitet.UniverzitetAllDTO;
import com.example.univerzitet.dto.univerzitet.UniverzitetDTO;
import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.entity.Univerzitet;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.repository.impl.AdresaRepository;
import com.example.univerzitet.repository.impl.UniverzitetRepository;
import com.example.univerzitet.service.base.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class UniverzitetService extends BaseService<Univerzitet, UniverzitetDTO, Long> {

    private final NastavnikService nastavnikService;
    private final AdresaRepository adresaRepository;
    private final UniverzitetRepository univerzitetRepository;

    @Autowired
    public UniverzitetService(UniverzitetRepository repository, NastavnikService nastavnikService, AdresaRepository adresaRepository) {
        super(repository);
        this.nastavnikService = nastavnikService;
        univerzitetRepository = repository;
        this.adresaRepository = adresaRepository;
    }

    @Override
    public UniverzitetDTO convertToDTO(Univerzitet element) {
        UniverzitetDTO univerzitetDTO = new UniverzitetDTO(element);

        NastavnikDTO rektor = nastavnikService.getNastavnikById(univerzitetDTO.getRektor().getId());

        univerzitetDTO.setRektor(rektor);
        univerzitetDTO.getFakulteti().forEach(fakultet -> {
            fakultet.setDekan(nastavnikService.getNastavnikById(fakultet.getDekan().getId()));
            fakultet.getStudijskiProgrami().forEach(studijskiProgram -> {
                studijskiProgram.setRukovodilac(
                        nastavnikService.getNastavnikById(studijskiProgram.getRukovodilac().getId())
                );
            });
        });

        return univerzitetDTO;
    }


    public UniverzitetDTO update(Long id, String univerzitetUpdateDTO, String adresa) {
        Univerzitet univerzitet = univerzitetRepository.getById(id);
        if(!StringUtils.isEmpty(adresa)) {
            String[] split = adresa.split(" | ");
            List<Adresa> list = adresaRepository.findAllByUlicaAndBroj(split[0], split[2]);
            univerzitet.setAdresa(list.get(0));
        }

        univerzitet.setNaziv(univerzitetUpdateDTO);

        return convertToDTO(univerzitetRepository.save(univerzitet));
    }

    public UniverzitetDTO createUni(String univerzitetUpdateDTO, String adresa) {
        String[] split = adresa.split(" | ");
        List<Adresa> list = adresaRepository.findAllByUlicaAndBroj(split[0], split[2]);

        return convertToDTO(univerzitetRepository.save(
                new Univerzitet(null, univerzitetUpdateDTO, LocalDate.now(),
                        null, new ArrayList<>(), list.get(0)))
        );
    }

    public List<UniverzitetAllDTO> findAllUniverziteti() {
        return generateListSimpleDto(univerzitetRepository.findAll());
    }

    public UniverzitetAllDTO convertToSimpleDTO(Univerzitet element) {
        return new UniverzitetAllDTO(element);
    }

    public List<UniverzitetAllDTO> generateListSimpleDto(Iterable<Univerzitet> elements) {
        List<UniverzitetAllDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }

    public List<UniverzitetAllDTO> findByName(String naziv) {
        List<UniverzitetAllDTO> univerzitetAllDTOS = generateListSimpleDto(univerzitetRepository.findAll());
        return univerzitetAllDTOS.stream().filter( uni -> uni.getNaziv().toLowerCase(Locale.ROOT).contains(naziv.toLowerCase(Locale.ROOT))).collect(Collectors.toList());
    }

    public void deleteUni(Long id) {
        Univerzitet univerzitet = univerzitetRepository.findById(id).get();
        if(univerzitet.getFakulteti().size() != 0){
            throw new KoristiSeNegdeException();
        } else if(univerzitet.getRektorId() != null){
            throw new KoristiSeNegdeException();
        }
        univerzitetRepository.delete(univerzitet);
    }
}
