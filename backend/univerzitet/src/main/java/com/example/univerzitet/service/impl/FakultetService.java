package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.fakultet.FakultetDTO;
import com.example.univerzitet.dto.fakultet.FakultetSimpleDTO;
import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.entity.Fakultet;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.mapper.Mapper;
import com.example.univerzitet.repository.impl.AdresaRepository;
import com.example.univerzitet.repository.impl.FakultetRepository;
import com.example.univerzitet.repository.impl.UniverzitetRepository;
import com.example.univerzitet.service.base.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class FakultetService extends BaseService<Fakultet, FakultetDTO, Long> {
    private final NastavnikService nastavnikService;
    private FakultetRepository fakultetRepository;
    private final AdresaRepository adresaRepository;
    private final UniverzitetRepository univerzitetRepository;

    public FakultetService(FakultetRepository repository, NastavnikService nastavnikService, AdresaRepository adresaRepository, UniverzitetRepository univerzitetRepository) {
        super(repository);
        this.nastavnikService = nastavnikService;
        fakultetRepository = repository;
        this.adresaRepository = adresaRepository;
        this.univerzitetRepository = univerzitetRepository;
    }

    @Override
    public FakultetDTO convertToDTO(Fakultet element) {
        FakultetDTO fakultetDTO = new FakultetDTO(element);
        fakultetDTO.setDekan(nastavnikService.getNastavnikById(fakultetDTO.getDekan().getId()));
        fakultetDTO.getStudijskiProgrami().forEach(studijskiProgram ->{
            studijskiProgram.setRukovodilac(nastavnikService.getNastavnikById(studijskiProgram.getRukovodilac().getId()));
        });

        fakultetDTO.getUniverzitet().setRektor(nastavnikService.getNastavnikById(fakultetDTO.getUniverzitet().getRektor().getId()));
        return fakultetDTO;
    }

    public List<FakultetSimpleDTO> findAllByUniverzitet(Long id) {
        return generateListSimpleDto(fakultetRepository.findAllByUniverzitet_Id(id));
    }

    public FakultetDTO update(Long id, String naziv, String adresa) {
        Fakultet fakultet = fakultetRepository.findById(id).get();
        fakultet.setNaziv(naziv);

        if(!StringUtils.isEmpty(adresa)) {
            String[] split = adresa.split(" | ");
            List<Adresa> list = adresaRepository.findAllByUlicaAndBroj(split[0], split[2]);
            fakultet.setAdresa(list.get(0));
        }
        return convertToDTO(fakultetRepository.save(fakultet));
    }

    public FakultetDTO createFakultet(Long id, String naziv, String adresa) {
        String[] split = adresa.split(" | ");
        List<Adresa> list = adresaRepository.findAllByUlicaAndBroj(split[0], split[2]);

        return new FakultetDTO(fakultetRepository.save(Mapper.toFakultet(null, naziv, univerzitetRepository.getById(id), list.get(0))));
    }

    public List<FakultetSimpleDTO> findAllFakulteti() {
        return generateListSimpleDto(fakultetRepository.findAll());
    }

    public FakultetSimpleDTO convertToSimpleDTO(Fakultet element) {
        return new FakultetSimpleDTO(element);
    }

    public List<FakultetSimpleDTO> generateListSimpleDto(Iterable<Fakultet> elements) {
        List<FakultetSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }

    public List<FakultetSimpleDTO> findAllByNaziv(Long id, String naziv) {
        List<FakultetSimpleDTO> allByUniverzitet = findAllByUniverzitet(id);
        return allByUniverzitet.stream().filter( uni -> uni.getNaziv().toLowerCase(Locale.ROOT).contains(naziv.toLowerCase(Locale.ROOT))).collect(Collectors.toList());
    }

    public void deleteFakultet(Long id) {
        Fakultet fakultet = fakultetRepository.findById(id).get();
        if(fakultet.getDekanId() != null){
            throw new KoristiSeNegdeException();
        } else if(fakultet.getStudijskiProgrami().size() != 0){
            throw new KoristiSeNegdeException();
        }
        fakultetRepository.delete(fakultet);
    }
}
