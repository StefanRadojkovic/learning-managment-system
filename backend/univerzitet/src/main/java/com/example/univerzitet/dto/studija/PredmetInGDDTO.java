package com.example.univerzitet.dto.studija;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetInGDDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private List<IshodInPredmetDTO> silabusPredavanja = new ArrayList<>();
    private List<IshodInPredmetDTO> silabusVezbe = new ArrayList<>();
    private List<ObavestenjeInPredmetDTO> obavestenja = new ArrayList<>();
}

