package com.example.univerzitet.mapper;

import com.example.univerzitet.entity.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class Mapper {
    public static Adresa toAdresa(String ulica, String broj, Mesto mesto) {
        return Adresa.builder()
                .ulica(ulica)
                .broj(broj)
                .mesto(mesto)
                .univerziteti(new ArrayList<>())
                .fakulteti(new ArrayList<>())
                .build();
    }

    public static Drzava toDrzava(String naziv) {
        return Drzava.builder()
                .naziv(naziv)
                .mesta(new ArrayList<>())
                .build();
    }

    public static Fakultet toFakultet(Long dekanId, String naziv, Univerzitet univerzitet, Adresa adresa) {
        return Fakultet.builder()
                .dekanId(dekanId)
                .studijskiProgrami(new ArrayList<>())
                .univerzitet(univerzitet)
                .adresa(adresa)
                .naziv(naziv)
                .build();
    }

    public static Mesto toMesto(String naziv, Drzava drzava) {
        return Mesto.builder()
                .naziv(naziv)
                .drzava(drzava)
                .adrese(new ArrayList<>())
                .build();
    }

    public static StudijskiProgram toStudijskiProgram(String naziv, String opis, Long rukovodilacId, Fakultet fakultet) {
        return StudijskiProgram.builder()
                .naziv(naziv)
                .opis(opis)
                .rukovodilacId(rukovodilacId)
                .fakultet(fakultet)
                .build();
    }

    public static Univerzitet toUniverzitet(String naziv, LocalDate datumOsnivanja, Adresa adresa, Long rektorId) {
        return Univerzitet.builder()
                .naziv(naziv)
                .datumOsnivanja(datumOsnivanja)
                .fakulteti(new ArrayList<>())
                .adresa(adresa)
                .rektorId(rektorId)
                .build();
    }

//    public static FakultetInUniverzitetDTO toFakultetInUniverzitetDTO(Fakultet fakultet){
//        FakultetInUniverzitetDTO fakultetDTO = new FakultetInUniverzitetDTO();
//        fakultetDTO.setId(fakultet.getId());
//        fakultet.getStudijskiProgrami().forEach(studijskiProgram -> fakultetDTO.getStudijskiProgrami().add(new StudijskiProgramInUverzitetDTO(studijskiProgram)));
//
//    }
}
