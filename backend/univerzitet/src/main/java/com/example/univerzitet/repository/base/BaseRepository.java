package com.example.univerzitet.repository.base;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

@NoRepositoryBean
public interface BaseRepository<T, I> extends PagingAndSortingRepository<T, I>, QueryByExampleExecutor<T> {
    T getById(I id);
}
