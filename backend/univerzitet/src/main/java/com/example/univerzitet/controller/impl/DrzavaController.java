package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.drzava.DrzavaDTO;
import com.example.univerzitet.dto.drzava.DrzavaSimpleDTO;
import com.example.univerzitet.entity.Drzava;
import com.example.univerzitet.service.impl.DrzavaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/univerzitet/drzave")
public class DrzavaController extends BaseController<Drzava, DrzavaDTO, Long> {

    private DrzavaService drzavaService;

    public DrzavaController(DrzavaService service) {
        super(service);
        drzavaService = service;
    }

    @PostMapping("{naziv}")
    @ResponseStatus(HttpStatus.CREATED)
    public DrzavaDTO createDrzava(@PathVariable("naziv") String naziv)
    {
        return drzavaService.createDrzava(naziv);
    }

    @PutMapping("/{id}/{naziv}")
    @ResponseStatus(HttpStatus.CREATED)
    public DrzavaDTO updateDrzava(@PathVariable("id") Long id, @PathVariable("naziv") String naziv)
    {
        return drzavaService.updateDrzava(id, naziv);
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDrzava(@PathVariable("id") Long id)
    {
        drzavaService.deleteDrzava(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<DrzavaSimpleDTO> findAllDrzave()
    {
        return drzavaService.findAllDrzave();
    }
}
