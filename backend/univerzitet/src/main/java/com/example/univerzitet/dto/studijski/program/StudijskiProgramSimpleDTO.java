package com.example.univerzitet.dto.studijski.program;

import com.example.univerzitet.entity.StudijskiProgram;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudijskiProgramSimpleDTO {
    private Long id;
    private String naziv;
    private String opis;

    public StudijskiProgramSimpleDTO(StudijskiProgram element) {
        id = element.getId();
        naziv = element.getNaziv();
        opis = element.getOpis();
    }
}

