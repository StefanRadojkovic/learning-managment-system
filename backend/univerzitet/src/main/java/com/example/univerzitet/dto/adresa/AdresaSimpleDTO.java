package com.example.univerzitet.dto.adresa;

import com.example.univerzitet.entity.Adresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdresaSimpleDTO {
    private Long id;
    private String ulicaIBroj;

    public AdresaSimpleDTO(Adresa adresa) {
        id = adresa.getId();
        ulicaIBroj = adresa.getUlica() + " | " + adresa.getBroj();
    }
}

