package com.example.univerzitet.dto.nastavnik.oblast;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NaucnaOblastDTO {
    private Long id;
    private String naziv;
}

