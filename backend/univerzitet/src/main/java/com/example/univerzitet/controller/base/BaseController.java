package com.example.univerzitet.controller.base;

import com.example.univerzitet.service.base.BaseService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Getter
public abstract class BaseController<T, P, I> {
    private final BaseService<T, P, I> service;

    public BaseController(BaseService<T, P, I> service) {
        this.service = service;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<P> getAll()
    {
        return service.findAll();
    }

    @GetMapping(path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public P getOne(@PathVariable("id") I id){
        return service.findOne(id);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public P create(@RequestBody T element){
        return service.save(element);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public P update(@RequestBody T changedT){
        return service.save(changedT);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") I id){
        service.delete(id);
    }

}
