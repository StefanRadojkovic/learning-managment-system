package com.example.univerzitet.dto.mesto;

import com.example.univerzitet.entity.Adresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdresaInMestoDTO {
    private Long id;
    private String ulica;
    private String broj;

    public AdresaInMestoDTO(Adresa adresa) {
        id = adresa.getId();
        ulica = adresa.getUlica();
        broj = adresa.getBroj();
    }
}

