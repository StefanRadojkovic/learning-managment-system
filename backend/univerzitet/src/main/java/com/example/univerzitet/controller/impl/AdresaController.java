package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.dto.adresa.AdresaSimpleDTO;
import com.example.univerzitet.dto.adresa.AdresaSimpleSecondDTO;
import com.example.univerzitet.dto.mesto.MestoSimpleDTO;
import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.service.impl.AdresaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/univerzitet/adrese")
public class AdresaController extends BaseController<Adresa, AdresaDTO, Long> {

    private AdresaService adresaService;

    public AdresaController(AdresaService service) {
        super(service);
        adresaService = service;
    }

    @PostMapping("/mesto/{mestoId}/{naziv}/{broj}")
    @ResponseStatus(HttpStatus.CREATED)
    public AdresaDTO createAdresa(
            @PathVariable("mestoId") Long mestoId,
            @PathVariable("naziv") String naziv,
            @PathVariable("broj") String broj
    )
    {
        return adresaService.createAdresa(mestoId, naziv, broj);
    }

    @PutMapping("/{id}/{ulica}/{broj}")
    @ResponseStatus(HttpStatus.CREATED)
    public AdresaDTO updateAdresa(
            @PathVariable("id") Long id,
            @PathVariable("ulica") String ulica,
            @PathVariable("broj") String broj
            )
    {
        return adresaService.updateAdresa(id, ulica, broj);
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAdresa(@PathVariable("id") Long id)
    {
        adresaService.deleteAdresa(id);
    }

    @GetMapping("mesto/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdresaSimpleDTO> findAllByMesto(@PathVariable("naziv") String naziv)
    {
        return adresaService.findAllByMesto(naziv);
    }

    @GetMapping("mestoId/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdresaSimpleSecondDTO> findAllByMestoId(@PathVariable("id") Long id)
    {
        return adresaService.findAllByMestoId(id);
    }
}
