package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.mesto.MestoDTO;
import com.example.univerzitet.dto.mesto.MestoSimpleDTO;
import com.example.univerzitet.entity.Drzava;
import com.example.univerzitet.entity.Mesto;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.exception.NazivAlreadyExistException;
import com.example.univerzitet.repository.impl.DrzavaRepository;
import com.example.univerzitet.repository.impl.MestoRepository;
import com.example.univerzitet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MestoService extends BaseService<Mesto, MestoDTO, Long> {

    private MestoRepository mestoRepository;
    private final DrzavaRepository drzavaRepository;

    public MestoService(MestoRepository repository, DrzavaRepository drzavaRepository) {
        super(repository);
        mestoRepository = repository;
        this.drzavaRepository = drzavaRepository;
    }

    @Override
    public MestoDTO convertToDTO(Mesto element) {
        return new MestoDTO(element);
    }

    public MestoDTO createMesto(Long drzavaId, String naziv) {
        Drzava drzava = drzavaRepository.findById(drzavaId).get();
        Optional<Mesto> mesto = mestoRepository.findByNaziv(naziv);
        if(mesto.isPresent()){
            throw new NazivAlreadyExistException();
        }
        return convertToDTO(mestoRepository.save(new Mesto(null, naziv, drzava, new ArrayList<>())));
    }

    public MestoDTO updateMesto(Long id, String naziv) {
        Mesto mesto = mestoRepository.findById(id).get();
        List<Mesto> list = mestoRepository.findAllByNaziv(naziv);
        List<Mesto> collect = list.stream()
                .filter(dr -> dr.getNaziv().equals(naziv))
                .filter(dr -> !dr.getId().equals(mesto.getId()))
                .collect(Collectors.toList());

        if(!collect.isEmpty()){
            throw new NazivAlreadyExistException();
        }
        mesto.setNaziv(naziv);
        return convertToDTO(mestoRepository.save(mesto));
    }

    public void deleteMesto(Long id) {
        MestoDTO mestoDTO = findOne(id);
        if(mestoDTO.getAdrese().size() != 0){
            throw new KoristiSeNegdeException();
        }
        mestoRepository.deleteById(id);
    }

    public MestoSimpleDTO convertToSimpleDTO(Mesto element) {
        return new MestoSimpleDTO(element);
    }

    public List<MestoSimpleDTO> generateListSimpleDto(Iterable<Mesto> elements) {
        List<MestoSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }

    public List<MestoSimpleDTO> findAllByDrzava(String drzavaNaziv) {
        return generateListSimpleDto(mestoRepository.findAllByDrzava_Naziv(drzavaNaziv));
    }

    public List<MestoSimpleDTO> findAllByDrzavaId(Long id) {
        return generateListSimpleDto(mestoRepository.findAllByDrzava_Id(id));
    }
}
