package com.example.univerzitet.dto.drzava;

import com.example.univerzitet.entity.Drzava;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DrzavaSimpleDTO {
    private Long id;
    private String name;

    public DrzavaSimpleDTO(Drzava drzava) {
        this.id = drzava.getId();
        this.name = drzava.getNaziv();
    }
}
