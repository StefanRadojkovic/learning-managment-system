package com.example.univerzitet.dto.nastavnik.zvanja;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TipZvanjaDTO {
    private Long id;
    private String naziv;
}

