package com.example.univerzitet.service.impl;

import com.example.univerzitet.config.security.UsernamePasswordAuthenticationWithToken;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PredmetService {
    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public List getGodineStudijaById(Long id){
        InstanceInfo predmet = eurekaClient.getApplication("predmet").getInstances().get(0);
        if(!"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
            UsernamePasswordAuthenticationWithToken authentication = (UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication();

            return webClient.get()
                    .uri(String.format(
                            "%sapi/predmet/godine-studija/find-by-studijski-program/%s",
                            predmet.getHomePageUrl(),
                            id))
                    .header("Authorization", String.format("Bearer %s", authentication.getToken()))
                    .retrieve().bodyToMono(List.class).block();
        }
        return webClient.get()
                .uri(String.format(
                        "%sapi/predmet/godine-studija/find-by-studijski-program/%s",
                        predmet.getHomePageUrl(),
                        id))
                .retrieve().bodyToMono(List.class).block();
    }
}
