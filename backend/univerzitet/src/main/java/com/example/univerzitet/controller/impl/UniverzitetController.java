package com.example.univerzitet.controller.impl;

import com.example.univerzitet.controller.base.BaseController;
import com.example.univerzitet.dto.univerzitet.UniverzitetAllDTO;
import com.example.univerzitet.dto.univerzitet.UniverzitetDTO;
import com.example.univerzitet.entity.Univerzitet;
import com.example.univerzitet.service.impl.UniverzitetService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/univerzitet/univerziteti")
public class UniverzitetController extends BaseController<Univerzitet, UniverzitetDTO, Long> {
    private UniverzitetService univerzitetService;

    public UniverzitetController(UniverzitetService service) {
        super(service);
        univerzitetService = service;
    }

    @PutMapping("update/{id}/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public UniverzitetDTO update(@PathVariable("id") Long id, @PathVariable("naziv") String naziv, @RequestBody String adresa)
    {
        return univerzitetService.update(id, naziv, adresa);
    }

    @PostMapping("{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public UniverzitetDTO createUni(@PathVariable("naziv") String id, @RequestBody String adresa)
    {
        return univerzitetService.createUni(id, adresa);
    }

    @GetMapping("find/{naziv}")
    @ResponseStatus(HttpStatus.OK)
    public List<UniverzitetAllDTO> find(@PathVariable("naziv") String naziv)
    {
        if(naziv.isEmpty()){
            return univerzitetService.findAllUniverziteti();
        }
        return univerzitetService.findByName(naziv);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<UniverzitetAllDTO> findAllUniverziteti()
    {
        return univerzitetService.findAllUniverziteti();
    }

    @DeleteMapping("delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUni(@PathVariable("id") Long id)
    {
        univerzitetService.deleteUni(id);
    }
}
