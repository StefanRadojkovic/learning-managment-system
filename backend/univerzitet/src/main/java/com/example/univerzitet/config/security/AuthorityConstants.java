package com.example.univerzitet.config.security;

public interface AuthorityConstants {
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_USER = "ROLE_USER";
}
