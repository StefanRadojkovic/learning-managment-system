package com.example.univerzitet.dto.fakultet;

import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.entity.Fakultet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FakultetSimpleDTO {
    private Long id;
    private String naziv;
    private AdresaDTO adresa;

    public FakultetSimpleDTO(Fakultet fakultet) {
        id = fakultet.getId();
        naziv = fakultet.getNaziv();
        adresa = new AdresaDTO(fakultet.getAdresa());
    }
}

