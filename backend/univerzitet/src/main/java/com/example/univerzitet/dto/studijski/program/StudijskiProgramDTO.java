package com.example.univerzitet.dto.studijski.program;

import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.dto.studija.GodinaStudijaDTO;
import com.example.univerzitet.entity.StudijskiProgram;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudijskiProgramDTO {
    private Long id;
    private String naziv;
    private String opis;
    private NastavnikDTO rukovodilac;
    private FakultetInSPDTO fakultet;
    private List<GodinaStudijaDTO> godineStudija = new ArrayList<>();

    public StudijskiProgramDTO(StudijskiProgram element) {
        id = element.getId();
        naziv = element.getNaziv();
        opis = element.getOpis();
        rukovodilac = NastavnikDTO.builder().id(element.getRukovodilacId()).build();
        fakultet = new FakultetInSPDTO(element.getFakultet());
    }
}

