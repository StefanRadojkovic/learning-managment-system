package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.StudijskiProgram;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;

public interface StudijskiProgramRepository extends BaseRepository<StudijskiProgram, Long> {
    List<StudijskiProgram> findAllByFakultet_Id(Long id);
    List<StudijskiProgram> findAllByNazivLike(String naziv);
}
