package com.example.univerzitet.repository.impl;


import com.example.univerzitet.entity.Univerzitet;
import com.example.univerzitet.repository.base.BaseRepository;

import java.util.List;

public interface UniverzitetRepository extends BaseRepository<Univerzitet, Long> {
    List<Univerzitet> findAllByNazivLike(String naziv);
}
