package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.drzava.DrzavaDTO;
import com.example.univerzitet.dto.drzava.DrzavaSimpleDTO;
import com.example.univerzitet.entity.Drzava;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.exception.NazivAlreadyExistException;
import com.example.univerzitet.repository.impl.DrzavaRepository;
import com.example.univerzitet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DrzavaService extends BaseService<Drzava, DrzavaDTO, Long> {

    private DrzavaRepository drzavaRepository;

    public DrzavaService(DrzavaRepository repository) {
        super(repository);
        drzavaRepository = repository;
    }

    @Override
    public DrzavaDTO convertToDTO(Drzava element) {
        return new DrzavaDTO(element);
    }

    public DrzavaDTO createDrzava(String naziv) {
        Optional<Drzava> drzava = drzavaRepository.findByNaziv(naziv);
        if(drzava.isPresent()){
            throw new NazivAlreadyExistException();
        }
        return convertToDTO(drzavaRepository.save(new Drzava(null, naziv, new ArrayList<>())));
    }

    public DrzavaDTO updateDrzava(Long id, String naziv) {
        Drzava drzava = drzavaRepository.findById(id).get();
        List<Drzava> list = drzavaRepository.findAllByNaziv(naziv);
        List<Drzava> collect = list.stream()
                .filter(dr -> dr.getNaziv().equals(naziv))
                .filter(dr -> !dr.getId().equals(drzava.getId()))
                .collect(Collectors.toList());

        if(!collect.isEmpty()){
            throw new NazivAlreadyExistException();
        }
        drzava.setNaziv(naziv);
        return convertToDTO(drzavaRepository.save(drzava));
    }

    public void deleteDrzava(Long id) {
        DrzavaDTO drzavaDTO = findOne(id);
        if(drzavaDTO.getMesta().size() != 0){
            throw new KoristiSeNegdeException();
        }
        drzavaRepository.deleteById(id);
    }

    public List<DrzavaSimpleDTO> findAllDrzave() {
        return generateListSimpleDto(drzavaRepository.findAll());
    }

    public DrzavaSimpleDTO convertToSimpleDTO(Drzava element) {
        return new DrzavaSimpleDTO(element);
    }

    public List<DrzavaSimpleDTO> generateListSimpleDto(Iterable<Drzava> elements) {
        List<DrzavaSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }
}
