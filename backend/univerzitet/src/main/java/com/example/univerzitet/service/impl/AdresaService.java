package com.example.univerzitet.service.impl;

import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.dto.adresa.AdresaSimpleDTO;
import com.example.univerzitet.dto.adresa.AdresaSimpleSecondDTO;
import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.entity.Mesto;
import com.example.univerzitet.exception.KoristiSeNegdeException;
import com.example.univerzitet.exception.NazivAlreadyExistException;
import com.example.univerzitet.repository.impl.AdresaRepository;
import com.example.univerzitet.repository.impl.MestoRepository;
import com.example.univerzitet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdresaService extends BaseService<Adresa, AdresaDTO, Long> {

    private AdresaRepository adresaRepository;
    private final MestoRepository mestoRepository;

    public AdresaService(AdresaRepository repository, MestoRepository mestoRepository) {
        super(repository);
        this.mestoRepository = mestoRepository;
        adresaRepository = repository;
    }

    @Override
    public AdresaDTO convertToDTO(Adresa element) {
        return new AdresaDTO(element);
    }

    public AdresaDTO createAdresa(Long mestoId, String ulica, String broj) {
        Mesto mesto = mestoRepository.findById(mestoId).get();
        List<Adresa> lista = adresaRepository.findAllByUlicaAndBroj(ulica, broj);

        if(!lista.isEmpty()){
            throw new NazivAlreadyExistException();
        }
        return convertToDTO(adresaRepository.save(new Adresa(null, ulica, broj, mesto, new ArrayList<>(), new ArrayList<>())));
    }

    public AdresaDTO updateAdresa(Long id, String ulica, String broj) {
        Adresa adresa = adresaRepository.findById(id).get();

        List<Adresa> lista = adresaRepository.findAllByUlicaAndBroj(ulica, broj);

        List<Adresa> collect = lista.stream().filter(adr -> !adr.getId().equals(adresa.getId())).collect(Collectors.toList());

        if(!collect.isEmpty()){
            throw new NazivAlreadyExistException();
        }
        adresa.setBroj(broj);
        adresa.setUlica(ulica);
        return convertToDTO(adresaRepository.save(adresa));
    }

    public void deleteAdresa(Long id) {
        Adresa adresa = adresaRepository.findById(id).get();
        if(adresa.getFakulteti().size() != 0){
            throw new KoristiSeNegdeException();
        } else if(adresa.getUniverziteti().size() != 0){
            throw new KoristiSeNegdeException();
        }
        adresaRepository.deleteById(id);
    }

    public List<AdresaSimpleDTO> findAllByMesto(String naziv) {
        return generateListSimpleDto(adresaRepository.findAllByMesto_Naziv(naziv));
    }

    public AdresaSimpleDTO convertToSimpleDTO(Adresa element) {
        return new AdresaSimpleDTO(element);
    }
    public AdresaSimpleSecondDTO convertToSimpleSDTO(Adresa element) {
        return new AdresaSimpleSecondDTO(element);
    }

    public List<AdresaSimpleDTO> generateListSimpleDto(Iterable<Adresa> elements) {
        List<AdresaSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }


    public List<AdresaSimpleSecondDTO> generateListSimpleSDto(Iterable<Adresa> elements) {
        List<AdresaSimpleSecondDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleSDTO(element));

        });
        return newList;
    }

    public List<AdresaSimpleSecondDTO> findAllByMestoId(Long id) {
        return generateListSimpleSDto(adresaRepository.findAllByMesto_Id(id));
    }
}
