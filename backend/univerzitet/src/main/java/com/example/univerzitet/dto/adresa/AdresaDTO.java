package com.example.univerzitet.dto.adresa;

import com.example.univerzitet.entity.Adresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdresaDTO {
    private Long id;
    private String ulica;
    private String broj;
    private MestoInAdresaDTO mesto;

    public AdresaDTO(Adresa adresa) {
        id = adresa.getId();
        ulica = adresa.getUlica();
        broj = adresa.getBroj();
        mesto = new MestoInAdresaDTO(adresa.getMesto());
    }
}

