package com.example.univerzitet.dto.fakultet;

import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.StudijskiProgram;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudijskiProgramInFakultetDTO {
    private Long id;
    private String naziv;
    private NastavnikDTO rukovodilac;

    public StudijskiProgramInFakultetDTO(StudijskiProgram studijskiProgram) {
        id = studijskiProgram.getId();
        naziv = studijskiProgram.getNaziv();
        rukovodilac = NastavnikDTO.builder().id(studijskiProgram.getRukovodilacId()).build();
    }
}

