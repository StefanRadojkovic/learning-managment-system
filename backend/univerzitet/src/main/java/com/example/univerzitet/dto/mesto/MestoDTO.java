package com.example.univerzitet.dto.mesto;

import com.example.univerzitet.entity.Adresa;
import com.example.univerzitet.entity.Mesto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MestoDTO {
    private Long id;
    private String naziv;
    private DrzavaInMestoDTO drzava;
    private List<AdresaInMestoDTO> adrese = new ArrayList<>();

    public MestoDTO(Mesto mesto) {
        id = mesto.getId();
        naziv = mesto.getNaziv();
        drzava = new DrzavaInMestoDTO(mesto.getDrzava());
        adrese = this.getList(mesto.getAdrese());
    }
    private List<AdresaInMestoDTO> getList(List<Adresa> adrese){
        List<AdresaInMestoDTO> list = new ArrayList<>();
        adrese.forEach(adresa -> list.add(new AdresaInMestoDTO(adresa)));
        return list;
    }
}

