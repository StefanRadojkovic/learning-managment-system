package com.example.univerzitet.dto.studijski.program;

import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.Fakultet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FakultetInSPDTO {
    private Long id;
    private String naziv;
    private NastavnikDTO dekan;
    private UniverzitetInSPDTO univerzitet;

    public FakultetInSPDTO(Fakultet fakultet) {
        id = fakultet.getId();
        naziv = fakultet.getNaziv();
        dekan = NastavnikDTO.builder().id(fakultet.getDekanId()).build();
        univerzitet = new UniverzitetInSPDTO(fakultet.getUniverzitet());
    }
}

