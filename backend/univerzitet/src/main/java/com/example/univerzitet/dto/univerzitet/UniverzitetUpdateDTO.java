package com.example.univerzitet.dto.univerzitet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UniverzitetUpdateDTO {

    private String naziv;
}

