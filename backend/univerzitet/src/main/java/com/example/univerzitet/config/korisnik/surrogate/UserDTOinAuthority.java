package com.example.univerzitet.config.korisnik.surrogate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTOinAuthority {
    private Long id;
    private String username;
    private String password;
}
