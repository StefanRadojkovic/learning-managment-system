package com.example.univerzitet.dto.studijski.program;

import com.example.univerzitet.dto.adresa.AdresaDTO;
import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.Univerzitet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UniverzitetInSPDTO {
    private Long id;
    private String naziv;
    private LocalDate datumOsnivanja;
    private AdresaDTO adresa;
    private NastavnikDTO rektor;

    public UniverzitetInSPDTO(Univerzitet univerzitet) {
        id = univerzitet.getId();
        naziv = univerzitet.getNaziv();
        datumOsnivanja = univerzitet.getDatumOsnivanja();
        adresa = new AdresaDTO(univerzitet.getAdresa());
        rektor = NastavnikDTO.builder().id(univerzitet.getRektorId()).build();
    }
}

