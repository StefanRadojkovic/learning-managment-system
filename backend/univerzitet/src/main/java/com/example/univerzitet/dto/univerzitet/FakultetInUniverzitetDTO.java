package com.example.univerzitet.dto.univerzitet;

import com.example.univerzitet.dto.nastavnik.NastavnikDTO;
import com.example.univerzitet.entity.Fakultet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FakultetInUniverzitetDTO {
    private Long id;
    private NastavnikDTO dekan;
    private String naziv;
    private List<StudijskiProgramInUverzitetDTO> studijskiProgrami = new ArrayList<>();

    public FakultetInUniverzitetDTO(Fakultet fakultet) {
        id = fakultet.getId();
        naziv = fakultet.getNaziv();
        dekan = NastavnikDTO.builder().id(fakultet.getDekanId()).build();
        fakultet.getStudijskiProgrami().forEach(studijskiProgram -> studijskiProgrami.add(new StudijskiProgramInUverzitetDTO(studijskiProgram)));
    }
}

