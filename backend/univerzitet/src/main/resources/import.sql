INSERT INTO `univerzitet`.`drzava` (`naziv`) VALUES ('Srbija');
INSERT INTO `univerzitet`.`drzava` (`naziv`) VALUES ('Bugarska');

INSERT INTO `univerzitet`.`mesto` (`naziv`, `drzava_id`) VALUES ('Beograd', '1');
INSERT INTO `univerzitet`.`mesto` (`naziv`, `drzava_id`) VALUES ('Sofija', '2');
INSERT INTO `univerzitet`.`mesto` (`naziv`, `drzava_id`) VALUES ('Vrbas', '2');

insert into `univerzitet`.`adresa` (broj, ulica, mesto_id) values ('61342', 'Pearson', 3);
insert into `univerzitet`.`adresa` (broj, ulica, mesto_id) values ('7779', 'Stoughton', 2);
insert into `univerzitet`.`adresa` (broj, ulica, mesto_id) values ('9879', 'Tennyson', 1);
insert into `univerzitet`.`adresa` (broj, ulica, mesto_id) values ('6', 'Declaration', 1);


insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Singidunum', '1991-11-22', 1, 1);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Kirksville College of Osteopathic Medicine', '2011-03-04', 2, 2);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('St. Petersburg Institute of Arts and Restoration', '2005-06-12', 3, 3);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Technological University (Dawei)', '2008-08-23', 4, 4);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Tourist University (Dawei)', '2008-08-23', 4, 4);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Technological 1 University (Dawei)', '2008-08-23', 4, 4);
insert into `univerzitet`.`univerzitet` (naziv, datum_osnivanja, rektor_id, adresa_id) values ('Technological 2 University (Dawei)', '2008-08-23', 4, 4);

insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Tehnicki fakultet', 1, 1, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Pravni fakultet', 1, 1, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Ekonomski fakultet', 2, 1, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Filozofski fakultet', 2, 1, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Medicinski fakultet', 2, 1, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Umetnicki fakultet', 2, 1, 2);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Institute of Public Administration', 3, 3, 3);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Moscow State Faculty of Geodesy and Cartography', 4, 1, 4);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Faculty of Education', 1, 2, 5);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Faculty of Lodz', 2, 2, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Faculty of Northern British Columbia', 3, 2, 2);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Strayer Faculty', 4, 3, 3);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Trakia Faculty Stara Zagora', 1, 3, 4);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Universidade Federal de Campina Grande', 2, 3, 5);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Bashkir State Pedagogical Faculty named After M. Akmullah', 3, 4, 1);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Kyoritsu Woman''s Faculty', 4, 4, 2);
insert into `univerzitet`.`fakultet` (naziv, adresa_id, univerzitet_id, dekan_id) values ('Shanxi Faculty', 1, 4, 3);



insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Sales', 'Dilation of Inferior Mesenteric Artery with Four or More Intraluminal Devices, Percutaneous Endoscopic Approach', 1, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Mena', 'Supplement Right Abdomen Muscle with Autologous Tissue Substitute, Percutaneous Endoscopic Approach', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Marketing', 'Primer opisa 1', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Menadzment', 'Primer opisa 2', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Data science', 'Primer opisa 3', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Softversko inzenjerstvo', 'Primer opisa 4', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Racunarstvo i automatika', 'Primer opisa 5', 2, 1);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Marketing', 'Bypass Right Axillary Artery to Right Lower Arm Artery with Nonautologous Tissue Substitute, Open Approach', 3, 2);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Engineering', 'Excision of Adenoids, External Approach', 4, 2);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Accounting', 'Beam Radiation of Head and Neck using Photons 1 - 10 MeV', 5, 3);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Support', 'Revision of Spacer in Left Toe Phalangeal Joint, Open Approach', 1, 3);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Sales', 'Replacement of Right Lens with Synthetic Substitute, Percutaneous Approach', 2, 4);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Business Development', 'Repair Lower Esophagus, Percutaneous Endoscopic Approach', 3, 4);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Product Management', 'Dilation of Left Upper Lobe Bronchus with Intraluminal Device, Percutaneous Approach', 4, 5);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Engineering', 'Removal of Synthetic Substitute from Coccyx, Percutaneous Approach', 5, 5);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Business Development', 'Drainage of Left Kidney, Percutaneous Approach', 1, 6);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Services', 'Ultrasonography of Multiple Coronary Arteries', 2, 6);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Business Development', 'Dilation of Left Common Iliac Artery, Bifurcation, with Four or More Drug-eluting Intraluminal Devices, Percutaneous Approach', 3, 7);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Sales', 'Removal of Autologous Tissue Substitute from Chest Wall, Percutaneous Endoscopic Approach', 4, 7);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Sales', 'Aural Rehabilitation Treatment using Assistive Listening Equipment', 5, 8);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Accounting', 'Supplement Ileocecal Valve with Synthetic Substitute, Open Approach', 1, 8);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Legal', 'Inspection of Right Ankle Region, Percutaneous Approach', 2, 9);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Legal', 'Reattachment of Cervix, Open Approach', 3, 9);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Legal', 'Release Right Internal Jugular Vein, Percutaneous Endoscopic Approach', 4, 10);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Legal', 'Reposition Left Tarsal, Open Approach', 5, 10);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Support', 'Replacement of Left Ureter with Nonautologous Tissue Substitute, Via Natural or Artificial Opening Endoscopic', 1, 11);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Marketing', 'Fusion of Lumbar Vertebral Joint with Autologous Tissue Substitute, Posterior Approach, Posterior Column, Percutaneous Approach', 2, 11);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Marketing', 'Plain Radiography of Superior Vena Cava using Other Contrast', 3, 12);
insert into `univerzitet`.`studijski_program` (naziv, opis, rukovodilac_id, fakultet_id) values ('Legal', 'Tomographic (Tomo) Nuclear Medicine Imaging of Left Upper Extremity using Other Radionuclide', 4, 12);
