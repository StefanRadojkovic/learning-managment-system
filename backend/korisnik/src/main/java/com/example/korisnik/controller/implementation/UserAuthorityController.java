package com.example.korisnik.controller.implementation;

import com.example.korisnik.controller.definition.BaseController;
import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.dto.UserAuthorityDTO;
import com.example.korisnik.model.Authority;
import com.example.korisnik.model.User;
import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.service.impl.UserAuthorityService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/korisnik/user-authority")
public class UserAuthorityController extends BaseController<UserAuthority, UserAuthorityDTO, Long> {
    public UserAuthorityController(UserAuthorityService service) {
        super(service);
    }

    @GetMapping(path = "/find")
    @ResponseStatus(HttpStatus.OK)
    public Page<UserAuthorityDTO> find(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                                      @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                                      @RequestParam(value = "sortBy", defaultValue = "id") Sort sort,
                                      @RequestParam(required = false, value = "id") Long id,
                                      @RequestParam(required = false, value = "username") String username,
                                      @RequestParam(required = false, value = "name") String name)
    {
        User user = new User(username, null, null);
        Authority authority = new Authority(name, null);

        UserAuthority userAuthority = new UserAuthority(id, user, authority);

        PageTracker pageTracker = new PageTracker(pageNumber, sort);
        return this.getService().findAll(userAuthority, pageTracker);
    }
}
