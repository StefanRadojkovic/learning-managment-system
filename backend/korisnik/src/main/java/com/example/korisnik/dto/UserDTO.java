package com.example.korisnik.dto;

import com.example.korisnik.dto.surrogate.AuthorityDTOinUser;
import com.example.korisnik.model.User;
import com.example.korisnik.model.UserAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Long id;
    private String username;
    private String password;
    private String jmbg;
    private String ime;
    Set<AuthorityDTOinUser> authorities;

    public UserDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.jmbg = user.getJmbg();
        this.ime = user.getIme();
        this.authorities = this.getList(user.getAuthorities());
    }
    private Set<AuthorityDTOinUser> getList(Set<UserAuthority> users){
        Set<AuthorityDTOinUser> list = new HashSet<>();
        if(users != null) {
            users.forEach(user -> list.add(new AuthorityDTOinUser(user.getAuthority())));
        }
        return list;
    }
}
