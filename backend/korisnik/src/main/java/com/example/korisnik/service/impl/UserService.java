package com.example.korisnik.service.impl;

import com.example.korisnik.dto.AddRoleDTO;
import com.example.korisnik.dto.UserALLDTO;
import com.example.korisnik.dto.UserDTO;
import com.example.korisnik.model.Authority;
import com.example.korisnik.model.User;
import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.repository.implementation.AuthorityRepository;
import com.example.korisnik.repository.implementation.UserAuthorityRepository;
import com.example.korisnik.repository.implementation.UserRepository;
import com.example.korisnik.service.definition.GroupService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService extends GroupService<User, UserDTO, Long> {
    UserRepository userRepository;
    AuthorityRepository authorityRepository;
    UserAuthorityRepository userAuthorityRepository;
    PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository,
                       AuthorityRepository authorityRepository,
                       UserAuthorityRepository userAuthorityRepository,
                       PasswordEncoder passwordEncoder) {
        super(userRepository);
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.userAuthorityRepository = userAuthorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User save = userRepository.save(user);
        UserAuthority role_registrovan_korisnik = userAuthorityRepository.save(new UserAuthority(save, authorityRepository.findByName("ROLE_REGISTROVAN_KORISNIK")));
        save.getAuthorities().add(role_registrovan_korisnik);
        return convertToDTO(save);
    }

    @Override
    public UserDTO update(Long id, User entityDto) {
        User user = getRepository().findById(id).get();
        user.setPassword(passwordEncoder.encode(entityDto.getPassword()));
        user.setIme(entityDto.getIme());
        user.setJmbg(entityDto.getJmbg());
        return super.save(user);
    }

    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public UserDTO convertToDTO(User element) {
        return new UserDTO(element);
    }

    public UserDTO findByUsername(String username){
        return convertToDTO(userRepository.findByUsername(username).get());
    }

    public List<UserALLDTO> getAll() {
        List<User> all = userRepository.findAll();
        return generateSimpleList(all);
    }

    public List<UserALLDTO> generateSimpleList(List<User> all){
        List<UserALLDTO> response = new ArrayList<>();

        all.forEach(user -> {
            UserALLDTO userALLDTO = new UserALLDTO();
            userALLDTO.setIme(user.getIme());
            userALLDTO.setJmbg(user.getJmbg());
            userALLDTO.setId(user.getId());
            userALLDTO.setUsername(user.getUsername());
            String roles = "";
            List<String> rolesList = new ArrayList<>();

            user.getAuthorities().forEach(auth -> {
                if(auth.getUser().getId().equals(user.getId())) {
                    rolesList.add(auth.getAuthority().getName());
                }
            });

            for (String s : rolesList) {
                roles += s + ", ";
            }

            if(roles.length() > 3) {
                roles = roles.substring(0, roles.length() - 2);
            }

            userALLDTO.setRoles(roles);
            response.add(userALLDTO);
        });

        return response;
    }

    public void addRoleToUser(Long id, AddRoleDTO addRoleDTO) {
        UserDTO userDTO = findOne(id);
        User user = userRepository.findById(id).get();

        if(userDTO.getAuthorities().stream().noneMatch(auth -> auth.getName().equals(addRoleDTO.getNaziv()))){
            Authority authority = authorityRepository.findByName(addRoleDTO.getNaziv());
            userAuthorityRepository.save(new UserAuthority(user, authority));
        }
    }

    public void removeRoleToUser(Long id, String addRoleDTO) {
        UserDTO userDTO = findOne(id);
        User user = userRepository.findById(id).get();

        if(userDTO.getAuthorities().stream().anyMatch(auth -> auth.getName().equals(addRoleDTO))){
            Authority authority = authorityRepository.findByName(addRoleDTO);
            UserAuthority userAuthority = userAuthorityRepository.findByUser_idAndAuthority_id(user.getId(), authority.getId());
            userAuthorityRepository.deleteById(userAuthority.getId());
        }
    }

    public List<UserALLDTO> findAllByIme(String ime) {
        List<User> all = userRepository.findAllByImeContaining(ime);
        return generateSimpleList(all);
    }
}


















