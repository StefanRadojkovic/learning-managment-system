package com.example.korisnik.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentShowOneDTO implements Serializable {
    private Long id;
    private String ime;
    private String username;
    private String jmbg;
    private String brojIndeksa;
    private Integer prosecnaOcena;
    private Integer ukupnoEspb;
}
