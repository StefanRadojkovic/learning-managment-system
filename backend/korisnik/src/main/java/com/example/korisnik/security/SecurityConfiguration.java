package com.example.korisnik.security;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.example.korisnik.security.AuthorityConstants.ROLE_NASTAVNIK;
import static com.example.korisnik.security.AuthorityConstants.ROLE_STUDENT;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@AllArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    UserDetailsService userDetailsService;
    PasswordEncoder passwordEncoder;
    FilterChainExceptionHandler filterChainExceptionHandler;

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder).and().jdbcAuthentication();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Bean
    public JwtRequestFilter authenticationTokenFilterBean() throws Exception {
        JwtRequestFilter jwtRequestFilter = new JwtRequestFilter();
        jwtRequestFilter.setAuthenticationManager(authenticationManagerBean());
        return jwtRequestFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.csrf().disable();
        http.formLogin().disable();
        http.authorizeRequests()
                .antMatchers("/api/korisnik/login").permitAll()
                .antMatchers(HttpMethod.GET, "/api/korisnik/users/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/korisnik/users/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/predmet/godine-studija/**").permitAll()
                .antMatchers("/api/korisnik/studenti/**").hasAnyAuthority(ROLE_STUDENT, ROLE_NASTAVNIK)
                .antMatchers("/api/korisnik/studenti/**").hasAnyAuthority(ROLE_STUDENT, ROLE_NASTAVNIK)
                .antMatchers("/api/**").authenticated();
//                .antMatchers("/api/admin/**").hasAnyAuthority(AuthorityConstants.ROLE_ADMIN);
        http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(filterChainExceptionHandler, JwtRequestFilter.class);
    }
}
