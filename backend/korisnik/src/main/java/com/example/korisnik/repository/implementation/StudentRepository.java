package com.example.korisnik.repository.implementation;


import com.example.korisnik.model.Student;
import com.example.korisnik.repository.definition.GroupRepository;

import java.util.List;

public interface StudentRepository extends GroupRepository<Student, Long> {
    List<Student> findAll();
}
