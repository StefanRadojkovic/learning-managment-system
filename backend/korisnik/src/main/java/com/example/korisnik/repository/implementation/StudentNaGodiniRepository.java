package com.example.korisnik.repository.implementation;


import com.example.korisnik.model.StudentNaGodini;
import com.example.korisnik.repository.definition.GroupRepository;

public interface StudentNaGodiniRepository extends GroupRepository<StudentNaGodini, Long> {
}
