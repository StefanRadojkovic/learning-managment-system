package com.example.korisnik.dto.student.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeInPPDTO {
    private Long id;
    private String opis;
}

