package com.example.korisnik.repository.implementation;


import com.example.korisnik.model.User;
import com.example.korisnik.repository.definition.GroupRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends GroupRepository<User, Long> {
    Optional<User> findByUsername(String username);

    List<User> findAll();

    List<User> findAllByImeContaining(String ime);
}
