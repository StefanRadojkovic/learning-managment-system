package com.example.korisnik.service.definition;

import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.repository.definition.GroupRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class GroupService<T, P, I>{
    private GroupRepository<T, I> repository;
    @Value("${itemsPerPage}")
    int itemsPerPage;

    public GroupService(GroupRepository<T, I> repository) {
        this.repository = repository;
    }


    public abstract P convertToDTO(T element);

    public List<P> generateList(Iterable<T> elements){
        List<P> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(this.convertToDTO(element));

        });
        return newList;
    }

    public Page<P> findAll(T type, PageTracker pageTracker) {
        Page<T> page = repository.findAll(
                Example.of(type),
                PageRequest.of(pageTracker.getPageNumber(), itemsPerPage, pageTracker.getSort())
        );
        List<P> newContent = this.generateList(page.getContent());
        return new PageImpl<>(newContent);
    }

    public Page<P> getPage(PageTracker pageTracker){
        Page<T> page = repository.findAll(PageRequest.of(pageTracker.getPageNumber(), itemsPerPage, pageTracker.getSort()));
        List<P> newContent = this.generateList(page.getContent());
        return new PageImpl<>(newContent);
    }

    public P findOne(I id){
        return this.convertToDTO(repository.findById(id).get());
    }

    public void delete(I id){
        repository.deleteById(id);
    }

    public P save(T expenseGroup){
        return this.convertToDTO(repository.save(expenseGroup));
    }

    public abstract P update(I id, T dto);

    public T findById(I id){
        return repository.findById(id).get();
    }

}
