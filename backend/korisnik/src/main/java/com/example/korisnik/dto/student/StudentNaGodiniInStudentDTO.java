package com.example.korisnik.dto.student;

import com.example.korisnik.dto.student.studija.GodinaStudijaDTO;
import com.example.korisnik.model.StudentNaGodini;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentNaGodiniInStudentDTO {
    private Long id;
    private LocalDate datumUpisa;
    private String brojIndeksa;
    private GodinaStudijaDTO godinaStudija;

    public StudentNaGodiniInStudentDTO(StudentNaGodini godini) {
        id = godini.getId();
        datumUpisa = godini.getDatumUpisa();
        brojIndeksa = godini.getBrojIndeksa();
    }
}

