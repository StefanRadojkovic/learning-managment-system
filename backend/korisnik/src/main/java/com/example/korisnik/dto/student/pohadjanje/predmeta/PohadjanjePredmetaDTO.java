package com.example.korisnik.dto.student.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PohadjanjePredmetaDTO {
    private Long id;
    private Integer konacnaOcena;
    private Integer brojPokusaja;
    private PredmetInPPDTO predmet;
}

