package com.example.korisnik.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column
    private String ime;

    @Column
    private String jmbg;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Set<UserAuthority> authorities = new HashSet<>();

    public User(String username, String password, Set<UserAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public User(Long id, String username, Object o, Set<UserAuthority> userAuthorities) {
        this.id = id;
        this.username = username;
        this.authorities = userAuthorities;
    }
}
