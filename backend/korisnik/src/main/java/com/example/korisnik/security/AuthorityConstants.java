package com.example.korisnik.security;

public interface AuthorityConstants {
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_STUDENT = "ROLE_STUDENT";
    String ROLE_NASTAVNIK = "ROLE_NASTAVNIK";
    String ROLE_REGISTROVAN_KORISNIK = "ROLE_REGISTROVAN_KORISNIK";
}
