package com.example.korisnik.service.impl;

import com.example.korisnik.dto.student.pohadjanje.predmeta.ListOfPohadjanjePredmetaDTO;
import com.example.korisnik.dto.student.studija.GodinaStudijaDTO;
import com.example.korisnik.security.UsernamePasswordAuthenticationWithToken;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class PredmetService {

    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public GodinaStudijaDTO getGodinaStudijaById(Long id){
        return webClient.get()
                .uri(String.format(
                        "%sapi/predmet/godine-studija/%s",
                        getPredmetInstance().getHomePageUrl(),
                        id))
                .header("Authorization", getBearerToken())
                .retrieve()
                .bodyToMono(GodinaStudijaDTO.class)
                .block();
    }



    public ListOfPohadjanjePredmetaDTO getPohadjanjePredmetaByStudentId(Long id){
        return webClient.get()
                .uri(String.format(
                        "%sapi/predmet/pohadjanje-predmeta/student/%s",
                        getPredmetInstance().getHomePageUrl(),
                        id))
                .header("Authorization", getBearerToken())
                .retrieve()
                .bodyToMono(ListOfPohadjanjePredmetaDTO.class)
                .block();
    }

    private InstanceInfo getPredmetInstance(){
        return eurekaClient.getApplication("predmet").getInstances().get(0);
    }

    private String getBearerToken(){
        String token = ((UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication()).getToken();
        return String.format("Bearer %s", token);
    }
}
