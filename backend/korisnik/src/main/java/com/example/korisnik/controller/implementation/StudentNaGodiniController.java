package com.example.korisnik.controller.implementation;

import com.example.korisnik.controller.definition.BaseController;
import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.dto.student.StudentDTO;
import com.example.korisnik.dto.student.na.godini.StudentNaGodiniDTO;
import com.example.korisnik.model.SearchCriteria;
import com.example.korisnik.model.StudentNaGodini;
import com.example.korisnik.service.impl.StudentNaGodiniService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/korisnik/student-na-godini")
public class StudentNaGodiniController extends BaseController<StudentNaGodini, StudentNaGodiniDTO, Long> {

    private StudentNaGodiniService studentNaGodiniService;

    public StudentNaGodiniController(StudentNaGodiniService service) {
        super(service);
        studentNaGodiniService = service;
    }

    @GetMapping(path = "/find")
    @ResponseStatus(HttpStatus.OK)
    public Page<StudentNaGodiniDTO> find(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                                 @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                                 @RequestParam(value = "sortBy", defaultValue = "user") String sortBy,
                                 @RequestBody List<SearchCriteria> searchCriteria)
    {
        PageTracker pageTracker = new PageTracker(pageNumber, Sort.by(Sort.Direction.valueOf(sortDirection), sortBy));
        return new PageImpl<>(studentNaGodiniService.pretrazi(pageTracker, searchCriteria));
    }
}
