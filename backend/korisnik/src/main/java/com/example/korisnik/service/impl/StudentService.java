package com.example.korisnik.service.impl;

import com.example.korisnik.dto.FindStudentRequest;
import com.example.korisnik.dto.FindStudentResponse;
import com.example.korisnik.dto.student.StudentDTO;
import com.example.korisnik.dto.student.StudentShowOneDTO;
import com.example.korisnik.model.Student;
import com.example.korisnik.model.User;
import com.example.korisnik.repository.implementation.StudentRepository;
import com.example.korisnik.repository.implementation.UserRepository;
import com.example.korisnik.service.definition.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class StudentService extends GroupService<Student, StudentDTO, Long> {

    @PersistenceContext
    EntityManager entityManager;
    private final PredmetService predmetService;
    private final UserRepository userRepository;
    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository repository, PredmetService predmetService, UserRepository userRepository) {
        super(repository);
        this.predmetService = predmetService;
        this.userRepository = userRepository;
        studentRepository = repository;
    }

    @Override
    public StudentDTO convertToDTO(Student element) {
        StudentDTO studentDTO = new StudentDTO(element);
        for (int i = 0; i < element.getStudentNaGodini().size(); i++){
            studentDTO.getStudentNaGodini().get(i).setGodinaStudija(predmetService.getGodinaStudijaById(element.getStudentNaGodini().get(i).getGodinaStudija()));
        }
        predmetService.getPohadjanjePredmetaByStudentId(element.getUser()).getList()
                .forEach(
                        predmet ->
                        studentDTO.getPohadjanjePredmeta().add(predmet)
                );
        User user = userRepository.findById(studentDTO.getId()).get();
        studentDTO.setIme(user.getIme());
        studentDTO.setJmbg(user.getJmbg());
        studentDTO.setUsername(user.getUsername());
        return studentDTO;
    }

    @Override
    public StudentDTO update(Long id, Student dto) {
        Student student = getRepository().findById(id).get();
        return convertToDTO(getRepository().save(student));
    }

    public List<FindStudentResponse> pretrazi(FindStudentRequest findStudentRequest) {
        List<StudentDTO> studenti = generateList(studentRepository.findAll());
        List<FindStudentResponse> response = new ArrayList<>();

        if (findStudentRequest.getIme() != null) {
            studenti = studenti.stream().filter(studentDTO -> studentDTO.getIme().contains(findStudentRequest.getIme())).collect(Collectors.toList());
        }

        if (findStudentRequest.getBrojIndeksa() != null) {
            studenti = studenti.stream().filter(studentDTO -> (studentDTO.getStudentNaGodini().stream().anyMatch(godina -> godina.getBrojIndeksa().contains(
                    findStudentRequest.getBrojIndeksa()
            )))).collect(Collectors.toList());
        }

        if (findStudentRequest.getGodinaUpisa() != null) {
            studenti = studenti.stream().filter(studentDTO -> studentDTO.getStudentNaGodini().stream()
                    .anyMatch(godina -> godina.getDatumUpisa().toString().contains(findStudentRequest.getGodinaUpisa()))
            ).collect(Collectors.toList());
        }

        if (findStudentRequest.getProsecnaOcena() != null) {
            int prosecnaOcena = findStudentRequest.getProsecnaOcena().intValue();
            studenti = studenti.stream().filter(studentDTO -> {
                    List<Integer> ocene = new ArrayList<>();
                    studentDTO.getPohadjanjePredmeta().forEach(pohadjanje -> {
                        if(pohadjanje.getKonacnaOcena() != null) {
                            ocene.add(pohadjanje.getKonacnaOcena());
                        }
                    });

                    AtomicReference<Integer> sumaOcena = new AtomicReference<>(0);
                    ocene.forEach(ocena -> sumaOcena.updateAndGet(v -> v + ocena));

                    if(ocene.size() != 0) {
                        int ocena = sumaOcena.get() / ocene.size();
                        return prosecnaOcena == ocena;
                    }
                    return false;
            }).collect(Collectors.toList());
        }

        studenti.forEach(studentDTO -> {
            FindStudentResponse studentResponse = new FindStudentResponse();

            List<Integer> ocene = new ArrayList<>();
            studentDTO.getPohadjanjePredmeta().forEach(pohadjanje -> {
                if(pohadjanje.getKonacnaOcena() != null) {
                    ocene.add(pohadjanje.getKonacnaOcena());
                }
            });

            AtomicReference<Integer> sumaOcena = new AtomicReference<>(0);
            ocene.forEach(ocena -> sumaOcena.updateAndGet(v -> v + ocena));

            if(ocene.size() != 0) {
                studentResponse.setProsecnaOcena(sumaOcena.get() / ocene.size());
            }
            studentResponse.setIme(studentDTO.getIme());

            if(!studentDTO.getStudentNaGodini().isEmpty()) {
                studentResponse.setBrojIndeksa(studentDTO.getStudentNaGodini().get(0).getBrojIndeksa());
                if(studentDTO.getStudentNaGodini().get(0) != null){
                    studentResponse.setGodinaUpisa(studentDTO.getStudentNaGodini().get(0).getDatumUpisa().toString());
                }
            }
            response.add(studentResponse);
        });

        return response;
    }

    public StudentShowOneDTO findOneShowOne(Long id) {
        StudentShowOneDTO studentDTO = new StudentShowOneDTO();
        StudentDTO student = findOne(id);
        User user = userRepository.findById(student.getId()).get();

        studentDTO.setId(student.getId());
        studentDTO.setIme(user.getIme());
        studentDTO.setUsername(user.getUsername());
        studentDTO.setJmbg(user.getJmbg());
        if(!student.getStudentNaGodini().isEmpty()) {
            studentDTO.setBrojIndeksa(student.getStudentNaGodini().get(0).getBrojIndeksa());
        }
        List<Integer> ocene = new ArrayList<>();
        student.getPohadjanjePredmeta().forEach(pohadjanje -> {
            if(pohadjanje.getKonacnaOcena() != null) {
                ocene.add(pohadjanje.getKonacnaOcena());
            }
        });

        AtomicReference<Integer> sumaOcena = new AtomicReference<>(0);
        ocene.forEach(ocena -> sumaOcena.updateAndGet(v -> v + ocena));
        studentDTO.setProsecnaOcena(sumaOcena.get());

        List<Integer> espb = new ArrayList<>();
        student.getPohadjanjePredmeta().forEach(pohadjanje -> espb.add(pohadjanje.getPredmet().getEspb()));

        AtomicReference<Integer> sumaEspb = new AtomicReference<>(0);
        espb.forEach(espb1 -> sumaEspb.updateAndGet(v -> v + espb1));
        studentDTO.setUkupnoEspb(sumaEspb.get());

        return studentDTO;
    }
}
