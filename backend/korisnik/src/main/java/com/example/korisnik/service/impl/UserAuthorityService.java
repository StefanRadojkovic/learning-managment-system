package com.example.korisnik.service.impl;

import com.example.korisnik.dto.UserAuthorityDTO;
import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.repository.implementation.UserAuthorityRepository;
import com.example.korisnik.service.definition.GroupService;
import org.springframework.stereotype.Service;

@Service
public class UserAuthorityService extends GroupService<UserAuthority, UserAuthorityDTO, Long> {
    public UserAuthorityService(UserAuthorityRepository repository) {
        super(repository);
    }

    @Override
    public UserAuthorityDTO convertToDTO(UserAuthority element) {
        return new UserAuthorityDTO(element);
    }

    @Override
    public UserAuthorityDTO update(Long id, UserAuthority dto) {
        return null;
    }
}
