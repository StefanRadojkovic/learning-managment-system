package com.example.korisnik.service.impl;

import com.example.korisnik.dto.student.studija.GodinaStudijaDTO;
import com.example.korisnik.security.UsernamePasswordAuthenticationWithToken;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class GodinaStudijaService{

    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public GodinaStudijaDTO getGodinaStudijaById(Long id){
        InstanceInfo predmet = eurekaClient.getApplication("predmet").getInstances().get(0);
        UsernamePasswordAuthenticationWithToken authentication = (UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication();

        return webClient.get()
                .uri(String.format(
                        "%sapi/predmet/godine-studija/%s",
                        predmet.getHomePageUrl(),
                        id))
                .header("Authorization", String.format("Bearer %s", authentication.getToken()))
                .retrieve().bodyToMono(GodinaStudijaDTO.class).block();
    }
}
