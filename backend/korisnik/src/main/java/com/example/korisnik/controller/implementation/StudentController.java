package com.example.korisnik.controller.implementation;

import com.example.korisnik.controller.definition.BaseController;
import com.example.korisnik.dto.FindStudentRequest;
import com.example.korisnik.dto.FindStudentResponse;
import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.dto.student.StudentDTO;
import com.example.korisnik.dto.student.StudentShowOneDTO;
import com.example.korisnik.model.Student;
import com.example.korisnik.service.impl.StudentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/korisnik/studenti")
public class StudentController extends BaseController<Student, StudentDTO, Long> {

    private final StudentService studentService;

    public StudentController(StudentService service) {
        super(service);
        studentService = service;
    }


    @Override
    @GetMapping(path = "/")
    @ResponseStatus(HttpStatus.OK)
    public Page<StudentDTO> getPage(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                                    @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                                    @RequestParam(value = "sortBy", defaultValue = "user") String sortBy)
    {
        PageTracker pageTracker = new PageTracker(pageNumber, Sort.by(Sort.Direction.valueOf(sortDirection), sortBy));
        return studentService.getPage(pageTracker);
    }

    @PostMapping(path = "/find")
    @ResponseStatus(HttpStatus.OK)
    public List<FindStudentResponse> find(@RequestBody FindStudentRequest findStudentRequest)
    {
        return studentService.pretrazi(findStudentRequest);
    }

    @GetMapping(path = "/show-one/{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentShowOneDTO find(@PathVariable("id") Long id)
    {
        return studentService.findOneShowOne(id);
    }
}
