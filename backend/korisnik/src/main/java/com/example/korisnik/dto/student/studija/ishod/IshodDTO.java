package com.example.korisnik.dto.student.studija.ishod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IshodDTO {
    private Long id;
    private String opis;
}

