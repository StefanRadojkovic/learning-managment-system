package com.example.korisnik.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserALLDTO {
    private Long id;
    private String username;
    private String jmbg;
    private String ime;
    private String roles;
}
