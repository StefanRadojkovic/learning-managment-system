package com.example.korisnik.service.impl;

import com.example.korisnik.dto.AuthorityDTO;
import com.example.korisnik.model.Authority;
import com.example.korisnik.repository.implementation.AuthorityRepository;
import com.example.korisnik.service.definition.GroupService;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService extends GroupService<Authority, AuthorityDTO, Long> {
    public AuthorityService(AuthorityRepository repository) {
        super(repository);
    }

    @Override
    public AuthorityDTO convertToDTO(Authority element) {
        return new AuthorityDTO(element);
    }

    @Override
    public AuthorityDTO update(Long id, Authority dto) {
        Authority authority = this.getRepository().findById(id).get();
        authority.setName(dto.getName());
        return convertToDTO(getRepository().save(authority));
    }
}
