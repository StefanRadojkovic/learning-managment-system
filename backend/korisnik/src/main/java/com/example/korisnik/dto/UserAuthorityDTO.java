package com.example.korisnik.dto;

import com.example.korisnik.dto.surrogate.AuthorityDTOinUser;
import com.example.korisnik.dto.surrogate.UserDTOinAuthority;
import com.example.korisnik.model.UserAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserAuthorityDTO {
    private Long id;
    private UserDTOinAuthority user;
    private AuthorityDTOinUser authority;

    public UserAuthorityDTO(UserAuthority userAuthority) {
        this.id = userAuthority.getId();
        this.user = new UserDTOinAuthority(userAuthority.getUser());
        this.authority = new AuthorityDTOinUser(userAuthority.getAuthority());
    }

    public UserAuthorityDTO(UserDTOinAuthority user, AuthorityDTOinUser authority) {
        this.user = user;
        this.authority = authority;
    }
}
