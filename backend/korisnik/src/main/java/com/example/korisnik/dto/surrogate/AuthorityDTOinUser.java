package com.example.korisnik.dto.surrogate;

import com.example.korisnik.model.Authority;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthorityDTOinUser {
    private Long id;
    private String name;

    public AuthorityDTOinUser(Authority authority) {
        this.id = authority.getId();
        this.name = authority.getName();
    }
}
