package com.example.korisnik.controller.definition;

import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.service.definition.GroupService;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
public abstract class BaseController<T, P, I> {
    private final GroupService<T, P, I> service;

    public BaseController(GroupService<T, P, I> service) {
        this.service = service;
    }

    @GetMapping(path = "/")
    @ResponseStatus(HttpStatus.OK)
    public Page<P> getPage(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                           @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                           @RequestParam(value = "sortBy", defaultValue = "id") String sortBy)
    {
        PageTracker pageTracker = new PageTracker(pageNumber, Sort.by(Sort.Direction.valueOf(sortDirection), sortBy));
        return service.getPage(pageTracker);
    }

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public P getOne(@PathVariable("id") I id){
        return service.findOne(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') && hasAuthority('ADD')")
    @PostMapping(path = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public P create(@RequestBody T element){
        return service.save(element);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') && hasAuthority('UPDATE')")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public P update(@PathVariable("id") I id, @RequestBody T changedT){
        return service.update(id, changedT);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') && hasAuthority('DELETE')")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") I id){
        service.delete(id);
    }
}
