package com.example.korisnik.dto.student.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetInPPDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private GodinaStudijaInPPDTO godinaStudija;
    private List<IshodInPPDTO> silabus = new ArrayList<>();
    private List<ObavestenjeInPPDTO> obavestenja = new ArrayList<>();
}

