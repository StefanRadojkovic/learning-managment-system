package com.example.korisnik.dto.student.na.godini;

import com.example.korisnik.model.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentInSNGDTO implements Serializable {
    protected Long user;

    public StudentInSNGDTO(Student student) {
        user = student.getUser();
    }
}
