package com.example.korisnik.dto.student.studija;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GodinaStudijaDTO {
    private Long id;
    private Integer godina;
    List<PredmetInGDDTO> predmeti = new ArrayList<>();
    private Long studijskiProgram;
}

