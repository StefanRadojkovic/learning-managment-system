package com.example.korisnik.dto;

import com.example.korisnik.dto.surrogate.UserDTOinAuthority;
import com.example.korisnik.model.Authority;
import com.example.korisnik.model.UserAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
public class AuthorityDTO {
    private Long id;
    private String name;
    Set<UserDTOinAuthority> users;

    public AuthorityDTO(Authority authority) {
        this.id = authority.getId();
        this.name = authority.getName();
        this.users = this.getList(authority.getUsers());
    }
    private Set<UserDTOinAuthority> getList(Set<UserAuthority> users){
        Set<UserDTOinAuthority> list = new HashSet<>();
        users.forEach(user -> {
            list.add(new UserDTOinAuthority(user.getUser()));
        });
        return list;
    }

    public AuthorityDTO(String name, Set<UserDTOinAuthority> users) {
        this.name = name;
        this.users = users;
    }
}
