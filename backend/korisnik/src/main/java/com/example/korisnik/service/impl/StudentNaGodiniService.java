package com.example.korisnik.service.impl;

import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.dto.student.na.godini.StudentNaGodiniDTO;
import com.example.korisnik.model.SearchCriteria;
import com.example.korisnik.model.StudentNaGodini;
import com.example.korisnik.repository.definition.SearchQueryConsumer;
import com.example.korisnik.repository.implementation.StudentNaGodiniRepository;
import com.example.korisnik.service.definition.GroupService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class StudentNaGodiniService extends GroupService<StudentNaGodini, StudentNaGodiniDTO, Long> {

    private final PredmetService predmetService;

    @PersistenceContext
    EntityManager entityManager;

    public StudentNaGodiniService(StudentNaGodiniRepository repository, PredmetService predmetService) {
        super(repository);
        this.predmetService = predmetService;
    }

    @Override
    public StudentNaGodiniDTO convertToDTO(StudentNaGodini element) {
        StudentNaGodiniDTO studentNaGodiniDTO = new StudentNaGodiniDTO(element);
        studentNaGodiniDTO.setGodinaStudija(predmetService.getGodinaStudijaById(element.getGodinaStudija()));
        return studentNaGodiniDTO;
    }

    @Override
    public StudentNaGodiniDTO update(Long id, StudentNaGodini dto) {
        StudentNaGodini studentNaGodini = getRepository().findById(id).get();
        studentNaGodini.setDatumUpisa(dto.getDatumUpisa());
        studentNaGodini.setBrojIndeksa(dto.getBrojIndeksa());
        return convertToDTO(getRepository().save(studentNaGodini));
    }

    public List<StudentNaGodiniDTO> pretrazi(PageTracker pageTracker, List<SearchCriteria> searchCriteria) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<StudentNaGodini> query = builder.createQuery(StudentNaGodini.class);
        Root r = query.from(StudentNaGodini.class);

        Predicate predicate = builder.conjunction();

        SearchQueryConsumer searchConsumer =
                new SearchQueryConsumer(predicate, builder, r);
        searchCriteria.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        return this.generateList(entityManager.createQuery(query)
                .setMaxResults(this.getItemsPerPage())
                .setFirstResult(pageTracker.getPageNumber() * this.getItemsPerPage())
                .getResultList());
    }
}
