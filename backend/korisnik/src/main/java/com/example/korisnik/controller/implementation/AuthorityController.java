package com.example.korisnik.controller.implementation;

import com.example.korisnik.controller.definition.BaseController;
import com.example.korisnik.dto.AuthorityDTO;
import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.model.Authority;
import com.example.korisnik.model.User;
import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.service.impl.AuthorityService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/korisnik/authorities")
public class AuthorityController extends BaseController<Authority, AuthorityDTO, Long> {
    public AuthorityController(AuthorityService service) {
        super(service);
    }

    @GetMapping(path = "/find")
    @ResponseStatus(HttpStatus.OK)
    public Page<AuthorityDTO> find(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                                   @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                                   @RequestParam(value = "sortBy", defaultValue = "id") Sort sortBy,
                                   @RequestParam(required = false, value = "id") Long id,
                                   @RequestParam(required = false, value = "name") String name,
                                   @RequestParam(required = false, value = "username") String username)
    {
        UserAuthority user = new UserAuthority(null, new User(username, null, null), null);
        Set<UserAuthority> users = new HashSet<>();
        users.add(user);

        Authority authority = new Authority(id, name, users);

        PageTracker pageTracker = new PageTracker(pageNumber, sortBy);
        return this.getService().findAll(authority, pageTracker);
    }
}
