package com.example.korisnik.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Student implements Serializable {
    @Id
    protected Long user;

    @OneToMany(mappedBy = "student")
    List<StudentNaGodini> studentNaGodini;

}
