package com.example.korisnik.controller.implementation;

import com.example.korisnik.dto.Token;
import com.example.korisnik.model.User;
import com.example.korisnik.security.TokenProvider;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class LoginController {
    private AuthenticationManager authenticationManager;
    private TokenProvider tokenProvider;
    private UserDetailsService userDetailsService;

    @PostMapping(path = "/korisnik/login")
    public ResponseEntity<Token> login(@RequestBody User user){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        String jwt = tokenProvider.generateToken(userDetails);
        Token token = new Token(jwt);

        return new ResponseEntity<>(token, HttpStatus.OK);
    }
}
