package com.example.korisnik.repository.implementation;


import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.repository.definition.GroupRepository;

import java.util.List;

public interface UserAuthorityRepository extends GroupRepository<UserAuthority, Long> {
    UserAuthority findByUser_idAndAuthority_id(Long id, Long id1);

    List<UserAuthority> findAll();
}
