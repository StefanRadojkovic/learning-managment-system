package com.example.korisnik.dto.surrogate;

import com.example.korisnik.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTOinAuthority {
    private Long id;
    private String username;
    private String password;

    public UserDTOinAuthority(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
    }
}
