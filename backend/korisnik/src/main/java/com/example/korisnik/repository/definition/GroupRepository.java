package com.example.korisnik.repository.definition;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

@NoRepositoryBean
public interface GroupRepository<T, I> extends PagingAndSortingRepository<T, I>, QueryByExampleExecutor<T> {
}
