package com.example.korisnik.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindStudentRequest {
    private String ime;
    private String brojIndeksa;
    private String godinaUpisa;
    private Integer prosecnaOcena;
}
