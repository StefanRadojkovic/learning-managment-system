package com.example.korisnik.dto.student;

import com.example.korisnik.dto.student.pohadjanje.predmeta.PohadjanjePredmetaDTO;
import com.example.korisnik.model.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentDTO implements Serializable {
    private Long id;
    private String username;
    private String ime;
    private String jmbg;
    List<StudentNaGodiniInStudentDTO> studentNaGodini = new ArrayList<>();
    List<PohadjanjePredmetaDTO> pohadjanjePredmeta = new ArrayList<>();

    public StudentDTO(Student element) {
        id = element.getUser();
        element.getStudentNaGodini().forEach(godini -> studentNaGodini.add(new StudentNaGodiniInStudentDTO(godini)));
    }
}
