package com.example.korisnik.repository.implementation;


import com.example.korisnik.model.Authority;
import com.example.korisnik.repository.definition.GroupRepository;

public interface AuthorityRepository extends GroupRepository<Authority, Long> {

    Authority findByName(String naziv);
}
