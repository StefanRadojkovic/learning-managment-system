package com.example.korisnik.dto.student.studija.obavestenje;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeDTO {
    private Long id;
    private String opis;
}

