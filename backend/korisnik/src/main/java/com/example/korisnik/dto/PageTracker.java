package com.example.korisnik.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@Data
@NoArgsConstructor
public class PageTracker {
    @NotNull
    private int pageNumber;
    @NotNull
    private Sort sort;

    public PageTracker(int pageNumber, Sort sort) {
        this.pageNumber = pageNumber;
        this.sort = sort;
    }
}
