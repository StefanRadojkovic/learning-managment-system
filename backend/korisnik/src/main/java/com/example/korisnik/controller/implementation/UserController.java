package com.example.korisnik.controller.implementation;

import com.example.korisnik.controller.definition.BaseController;
import com.example.korisnik.dto.AddRoleDTO;
import com.example.korisnik.dto.PageTracker;
import com.example.korisnik.dto.UserALLDTO;
import com.example.korisnik.dto.UserDTO;
import com.example.korisnik.model.Authority;
import com.example.korisnik.model.User;
import com.example.korisnik.model.UserAuthority;
import com.example.korisnik.service.impl.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/korisnik/users")
public class UserController extends BaseController<User, UserDTO, Long> {
    private UserService userService;

    public UserController(UserService service) {
        super(service);
        userService = service;
    }

    @PostMapping(path = "/find")
    @ResponseStatus(HttpStatus.OK)
    public Page<UserDTO> find(@RequestParam(value = "pageNumber", defaultValue = "0") int pageNumber,
                                      @RequestParam(value = "sortDirection", defaultValue = "ASC") String sortDirection,
                                      @RequestParam(value = "sortBy", defaultValue = "id") Sort sort,
                                      @RequestParam(required = false, value = "id") Long id,
                                      @RequestParam(required = false, value = "username") String username,
                                      @RequestParam(required = false, value = "name") String name)
    {
        Set<UserAuthority> userAuthorities = new HashSet<>();
        userAuthorities.add(new UserAuthority(null, new Authority(name, null)));

        User user = new User(id, username, null, userAuthorities);

        PageTracker pageTracker = new PageTracker(pageNumber, sort);
        return this.getService().findAll(user, pageTracker);
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getOne(@RequestParam("username") String username){
        return userService.findByUsername(username);
    }

    @GetMapping("get-all")
    @ResponseStatus(HttpStatus.OK)
    public List<UserALLDTO> getAll(){
        return userService.getAll();
    }

    @GetMapping("find/{ime}")
    @ResponseStatus(HttpStatus.OK)
    public List<UserALLDTO> findAllByIme(@PathVariable("ime") String ime){
        return userService.findAllByIme(ime);
    }

    @PutMapping("add-role/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void addRoleToUser(@PathVariable("id") Long id, @RequestBody AddRoleDTO addRoleDTO){
        userService.addRoleToUser(id, addRoleDTO);
    }

    @DeleteMapping("remove-role/{id}/{aut}")
    @ResponseStatus(HttpStatus.OK)
    public void removeRoleToUser(@PathVariable("id") Long id, @PathVariable("aut") String aut){
        userService.removeRoleToUser(id, aut);
    }
}
