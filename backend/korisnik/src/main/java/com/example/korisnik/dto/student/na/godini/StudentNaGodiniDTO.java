package com.example.korisnik.dto.student.na.godini;

import com.example.korisnik.dto.student.studija.GodinaStudijaDTO;
import com.example.korisnik.model.StudentNaGodini;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentNaGodiniDTO {
    private Long id;
    private LocalDate datumUpisa;
    private String brojIndeksa;
    private StudentInSNGDTO student;
    private GodinaStudijaDTO godinaStudija;

    public StudentNaGodiniDTO(StudentNaGodini element) {
        id = element.getId();
        datumUpisa = element.getDatumUpisa();
        brojIndeksa = element.getBrojIndeksa();
        student = new StudentInSNGDTO(element.getStudent());
    }
}

