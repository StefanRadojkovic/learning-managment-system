package com.example.korisnik.dto.student.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GodinaStudijaInPPDTO {
    private Long id;
    private Integer godina;
    private Long studijskiProgram;
}

