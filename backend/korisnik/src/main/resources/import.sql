INSERT INTO `korisnik`.`authority` (`name`) VALUES ('ROLE_ADMIN');
INSERT INTO `korisnik`.`authority` (`name`) VALUES ('ROLE_NASTAVNIK');
INSERT INTO `korisnik`.`authority` (`name`) VALUES ('ROLE_STUDENT');
INSERT INTO `korisnik`.`authority` (`name`) VALUES ('ROLE_REGISTROVAN_KORISNIK');


insert into `korisnik`.`user` (jmbg, username, password, ime) values ('6917824286635', 'student1', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dyanne');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0368813592790', 'student2', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Melita');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7794982420543', 'student3', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Brannon');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0338899593599', 'student4', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Jonathon');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3227739231058', 'student5', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Seth');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7133125351386', 'student6', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Maris');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1567941147296', 'student7', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Wilden');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0795461087945', 'student8', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dimitri');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5745509193427', 'student9', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Robbin');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1327508985400', 'student10', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Adam');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7408784766137', 'student11', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Darci');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5147647267401', 'student12', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Romain');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3888247586328', 'student13', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Cal');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3764487497123', 'student14', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Agata');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('2278664755421', 'student15', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Vlad');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0045378559274', 'student16', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Nadya');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5070092734198', 'student17', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Maison');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('6402538136290', 'student18', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Maureene');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('4574756831646', 'student19', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Loralyn');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1712828970102', 'student20', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Genvieve');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('2762658309732', 'student21', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Bale');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0932976176857', 'student22', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Sarina');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('4532330537995', 'student23', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dosi');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('8529106718124', 'student24', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Elyse');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5033961005116', 'student25', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Silas');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7754064296470', 'student26', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Margy');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3653083087557', 'student27', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Devy');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7314143487566', 'student28', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Iormina');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0317896776304', 'student29', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Jenelle');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3214211974501', 'student30', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Kattie');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('8787284651113', 'student31', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Vania');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('9786024646429', 'student32', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Binky');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('8796216567623', 'student33', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Krishnah');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('8791806170865', 'student34', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Jacquelin');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1298405290945', 'student35', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Carl');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0040558058467', 'student36', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Elonore');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('2468696513528', 'student37', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Claretta');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('0748034888324', 'student38', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Manuel');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1582035734303', 'student39', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dareen');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3898538776577', 'student40', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Braden');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('4947667146632', 'student41', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Nester');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7258265176787', 'student42', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Denny');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('9067310272035', 'student43', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Iolande');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('9665632867674', 'student44', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Madelene');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7328995670538', 'student45', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Angele');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('1821944876286', 'student46', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Arabelle');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5092793741068', 'student47', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Marcella');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('4755923177340', 'student48', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Jocelyne');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5303082650757', 'student49', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Lon');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('5317294599825', 'student50', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Mohandas');


insert into `korisnik`.`user` (jmbg, username, password, ime) values ('4494237930875', 'nastavnik1', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dyanne');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('2471269909448', 'nastavnik2', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Simone');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('7441341587707', 'nastavnik3', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dennison');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('3890510666371', 'nastavnik4', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Lottie');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('2759825620263', 'nastavnik5', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Rikki');

insert into `korisnik`.`user` (jmbg, username, password, ime) values ('6981673370966', 'admin', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Dorolice');
insert into `korisnik`.`user` (jmbg, username, password, ime) values ('6981673370966', 'korisnik', '$2a$10$0klkC1aNsU1km31nAvayPeQXHA/riCf0sJdTmePaMxNaG4ImoYc1O', 'Zacharia');

INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '1');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '2');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '3');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '4');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '5');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '6');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '7');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '8');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '9');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '10');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '11');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '12');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '13');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '14');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '15');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '16');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '17');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '18');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '19');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '20');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '21');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '22');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '23');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '24');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '25');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '26');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '27');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '28');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '29');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '30');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '31');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '32');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '33');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '34');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '35');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '36');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '37');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '38');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '39');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '40');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '41');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '42');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '43');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '44');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '45');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '46');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '47');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '48');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '49');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('3', '50');


INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('2', '51');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('2', '52');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('2', '53');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('2', '54');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('2', '55');

INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('1', '56');
INSERT INTO `korisnik`.`user_authority` (`authority_id`, `user_id`) VALUES ('4', '57');


INSERT INTO `korisnik`.`student` (`user`) VALUES ('1');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('2');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('3');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('4');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('5');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('6');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('7');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('8');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('9');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('10');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('11');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('12');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('13');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('14');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('15');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('16');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('17');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('18');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('19');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('20');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('21');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('22');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('23');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('24');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('25');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('26');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('27');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('28');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('29');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('30');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('31');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('32');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('33');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('34');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('35');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('36');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('37');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('38');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('39');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('40');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('41');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('42');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('43');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('44');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('45');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('46');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('47');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('48');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('49');
INSERT INTO `korisnik`.`student` (`user`) VALUES ('50');

insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3062652575', '2011-06-14', 1, 1);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('9478658185', '2021-11-20', 2, 2);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1171868624', '2019-07-08', 3, 3);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('9926347370', '2010-05-06', 4, 4);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3083336934', '2016-11-28', 5, 5);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7980632417', '2005-12-14', 6, 6);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('2037226547', '2019-06-28', 7, 7);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('2665408519', '2020-01-12', 8, 8);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1027461581', '2004-10-30', 9, 9);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5178915922', '2014-05-05', 10, 10);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('8473154088', '2004-11-13', 11, 11);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('2103456255', '2014-05-30', 12, 12);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3290642725', '2003-05-09', 13, 13);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4376343803', '2008-10-05', 14, 14);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3668981510', '2007-06-02', 15, 15);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('8754767582', '2006-02-06', 16, 16);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4762532114', '2002-11-25', 17, 17);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5971763213', '2011-04-22', 18, 18);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4208564372', '2001-10-25', 19, 19);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3312805445', '2011-09-22', 20, 20);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('2628790158', '2006-04-06', 21, 21);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7431352826', '2019-05-17', 22, 22);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7953147227', '2017-04-19', 23, 23);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1607026499', '2020-11-01', 24, 24);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3453708542', '2015-05-07', 25, 25);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6567518069', '2010-09-25', 26, 26);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4587922366', '2004-10-04', 27, 27);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6228326548', '2017-12-19', 28, 28);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('2703362169', '2005-03-11', 29, 29);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1270687009', '2004-06-29', 30, 30);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4546379169', '2016-07-17', 31, 31);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('3681664924', '2017-04-22', 32, 32);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4279713712', '1999-12-15', 33, 33);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('8624165383', '2014-01-26', 34, 34);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5014433843', '2001-06-25', 35, 35);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1172544737', '2000-02-24', 36, 36);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7740054947', '2001-12-03', 37, 37);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5157557748', '2002-12-06', 38, 38);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6866270984', '2010-10-13', 39, 39);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('1177906389', '2010-06-07', 40, 40);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6737770813', '2001-03-31', 41, 41);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6084997492', '2010-04-01', 42, 42);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5645919147', '2018-09-07', 43, 43);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6813328698', '2015-03-24', 44, 44);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('4791357332', '2004-06-04', 45, 45);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('5079296252', '2014-12-18', 46, 46);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('6137412602', '2019-02-19', 47, 47);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7156057575', '2019-06-29', 48, 48);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('8621580065', '2002-10-26', 49, 49);
insert into `korisnik`.`student_na_godini` (broj_indeksa, datum_upisa, godina_studija, student_user) values ('7717484967', '2007-06-19', 50, 50);
