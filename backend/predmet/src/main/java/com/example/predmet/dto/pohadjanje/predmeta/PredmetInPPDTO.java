package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetInPPDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private GodinaStudijaInPPDTO godinaStudija;
    private List<IshodInPPDTO> silabusPredavanja = new ArrayList<>();
    private List<IshodInPPDTO> silabusVezbe = new ArrayList<>();
    private List<ObavestenjeInPPDTO> obavestenja = new ArrayList<>();

    public PredmetInPPDTO(Predmet predmet) {
        id = predmet.getId();
        naziv = predmet.getNaziv();
        espb = predmet.getEspb();
        obavezan = predmet.getObavezan();
        brojPredavanja = predmet.getBrojPredavanja();
        brojVezbi = predmet.getBrojVezbi();
        drugiObliciNastave = predmet.getDrugiObliciNastave();
        istrazivackiRad = predmet.getIstrazivackiRad();
        ostaliCasovi = predmet.getOstaliCasovi();
        godinaStudija = new GodinaStudijaInPPDTO(predmet.getGodinaStudija());
        predmet.getSilabusPredavanja().forEach(ishod -> silabusPredavanja.add(new IshodInPPDTO(ishod)));
        predmet.getSilabusVezbe().forEach(ishod -> silabusVezbe.add(new IshodInPPDTO(ishod)));
        predmet.getObavestenja().forEach(obavestenje -> obavestenja.add(new ObavestenjeInPPDTO(obavestenje)));
    }
}

