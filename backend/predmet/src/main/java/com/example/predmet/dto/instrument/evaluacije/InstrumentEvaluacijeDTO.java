package com.example.predmet.dto.instrument.evaluacije;

import com.example.predmet.entity.InstrumentEvaluacije;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InstrumentEvaluacijeDTO {
    private Long id;
    private String opis;
    private String instrument;
    private PredmetInInstrumentEvaluacijeDTO predmet;

    public InstrumentEvaluacijeDTO(InstrumentEvaluacije element) {
        id = element.getId();
        opis = element.getOpis();
        instrument = element.getInstrument();
        predmet = new PredmetInInstrumentEvaluacijeDTO(element.getPredmet());
    }
}

