package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.entity.Ishod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IshodInPPDTO {
    private Long id;
    private String opis;
    private Integer termin;

    public IshodInPPDTO(Ishod ishod) {
        id = ishod.getId();
        opis = ishod.getOpis();
        termin = ishod.getTermin();
    }
}

