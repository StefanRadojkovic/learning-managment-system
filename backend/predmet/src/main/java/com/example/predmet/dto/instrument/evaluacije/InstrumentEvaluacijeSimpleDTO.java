package com.example.predmet.dto.instrument.evaluacije;

import com.example.predmet.entity.InstrumentEvaluacije;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InstrumentEvaluacijeSimpleDTO {
    private Long id;
    private String opis;
    private String instrument;

    public InstrumentEvaluacijeSimpleDTO(InstrumentEvaluacije element) {
        id = element.getId();
        opis = element.getOpis();
        instrument = element.getInstrument();
    }
}

