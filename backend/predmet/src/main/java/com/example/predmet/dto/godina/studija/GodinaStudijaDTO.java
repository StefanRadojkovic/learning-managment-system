package com.example.predmet.dto.godina.studija;

import com.example.predmet.entity.GodinaStudija;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GodinaStudijaDTO {
    private Long id;
    private Integer godina;
    List<PredmetInGDDTO> predmeti = new ArrayList<>();
    private Long studijskiProgram;

    public GodinaStudijaDTO(GodinaStudija godinaStudija) {
        id = godinaStudija.getId();
        godina = godinaStudija.getGodina();
        studijskiProgram = godinaStudija.getStudijskiProgram();
        godinaStudija.getPredmeti().forEach(predmet -> predmeti.add(new PredmetInGDDTO(predmet)));
    }
}

