package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.pohadjanje.predmeta.ObavestenjeInPPDTO;
import com.example.predmet.dto.predmet.PredmetDTO;
import com.example.predmet.dto.predmet.PredmetSimpleDTO;
import com.example.predmet.entity.Predmet;
import com.example.predmet.service.impl.PredmetService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/predmeti")
public class PredmetController extends BaseController<Predmet, PredmetDTO, Long> {

    private PredmetService predmetService;

    public PredmetController(PredmetService service) {
        super(service);
        predmetService = service;
    }

    @GetMapping(path = "/nastavnik-predmeti/obavestenja/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ObavestenjeInPPDTO> findAllByNastavnikIdReturnListOfObavestenja(@PathVariable("id") Long id){
        return predmetService.findAllByNastavnikIdReturnListOfObavestenja(id);
    }

    @DeleteMapping(path = "/delete/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public void deletePredmet(@PathVariable("predmetId") Long predmetId){
        predmetService.deletePredmet(predmetId);
    }

    @PostMapping(path = "/godina/stud/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void createPredmet(
            @PathVariable("id") Long id,
            @RequestBody PredmetSimpleDTO predmetSimpleDTO){
        predmetService.createPredmet(id, predmetSimpleDTO);
    }

    @PutMapping(path = "/update/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePredmet(
            @PathVariable("predmetId") Long predmetId,
            @RequestBody PredmetSimpleDTO predmetSimpleDTO){
        predmetService.updatePredmet(predmetId, predmetSimpleDTO);
    }

    @GetMapping("/all-godine/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Integer> findGodineByPredmetId(@PathVariable("id") Long predmetId)
    {
        return predmetService.findGodineByPredmetId(predmetId);
    }
}
