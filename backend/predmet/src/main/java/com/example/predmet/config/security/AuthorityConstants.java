package com.example.predmet.config.security;

public interface AuthorityConstants {
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_STUDENT = "ROLE_STUDENT";
    String ROLE_NASTAVNIK = "ROLE_NASTAVNIK";
}
