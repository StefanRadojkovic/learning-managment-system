package com.example.predmet.config.korisnik.surrogate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthorityDTOinUser {
    private Long id;
    private String name;
}
