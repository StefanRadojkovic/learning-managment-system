package com.example.predmet.dto.obavestenje;

import com.example.predmet.dto.predmet.GodinaStudijaInPredmetDTO;
import com.example.predmet.dto.predmet.IshodInPredmetDTO;
import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetInObavestenjeDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private GodinaStudijaInPredmetDTO godinaStudija;
    private List<IshodInPredmetDTO> silabusPredavanja = new ArrayList<>();
    private List<IshodInPredmetDTO> silabusVezbe = new ArrayList<>();

    public PredmetInObavestenjeDTO(Predmet predmet) {
        id = predmet.getId();
        naziv = predmet.getNaziv();
        espb = predmet.getEspb();
        obavezan = predmet.getObavezan();
        brojPredavanja = predmet.getBrojPredavanja();
        brojVezbi = predmet.getBrojVezbi();
        drugiObliciNastave = predmet.getDrugiObliciNastave();
        istrazivackiRad = predmet.getIstrazivackiRad();
        ostaliCasovi = predmet.getOstaliCasovi();
        godinaStudija = new GodinaStudijaInPredmetDTO(predmet.getGodinaStudija());
        predmet.getSilabusPredavanja().forEach(ishod -> silabusPredavanja.add(new IshodInPredmetDTO(ishod)));
        predmet.getSilabusVezbe().forEach(ishod -> silabusVezbe.add(new IshodInPredmetDTO(ishod)));
    }
}

