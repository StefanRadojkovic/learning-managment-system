package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.obavestenje.ObavestenjeDTO;
import com.example.predmet.dto.obavestenje.ObavestenjeRequest;
import com.example.predmet.dto.obavestenje.ObavestenjeRequestUpdate;
import com.example.predmet.entity.Obavestenje;
import com.example.predmet.service.impl.ObavestenjeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/predmet/obavestenja")
public class ObavestenjeController extends BaseController<Obavestenje, ObavestenjeDTO, Long> {

    private ObavestenjeService obavestenjeService;

    public ObavestenjeController(ObavestenjeService service) {
        super(service);
        obavestenjeService = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ObavestenjeDTO createObavestenje(@RequestBody ObavestenjeRequest request){
        return obavestenjeService.create(request);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public ObavestenjeDTO updateObavestenje(@PathVariable("id") Long id, @RequestBody ObavestenjeRequestUpdate request){
        return obavestenjeService.update(id, request);
    }
}
