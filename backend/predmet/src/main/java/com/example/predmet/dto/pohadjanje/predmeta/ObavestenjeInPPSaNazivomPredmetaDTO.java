package com.example.predmet.dto.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeInPPSaNazivomPredmetaDTO {
    private Long id;
    private String opis;
    private String title;
    private String nazivPredmeta;

    public ObavestenjeInPPSaNazivomPredmetaDTO(ObavestenjeInPPDTO obavestenje) {
        id = obavestenje.getId();
        opis = obavestenje.getOpis();
        title = obavestenje.getTitle();
    }
}

