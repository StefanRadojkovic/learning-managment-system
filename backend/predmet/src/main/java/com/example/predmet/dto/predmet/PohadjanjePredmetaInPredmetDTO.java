package com.example.predmet.dto.predmet;

import com.example.predmet.dto.student.StudentDTO;
import com.example.predmet.entity.PohadjanjePredmeta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PohadjanjePredmetaInPredmetDTO {
    private Long id;
    private Integer konacnaOcena;
    private Integer brojPokusaja;
    private StudentDTO student;

    public PohadjanjePredmetaInPredmetDTO(PohadjanjePredmeta element) {
        id = element.getId();
        brojPokusaja = element.getBrojPokusaja();
        konacnaOcena = element.getKonacnaOcena();
    }
}

