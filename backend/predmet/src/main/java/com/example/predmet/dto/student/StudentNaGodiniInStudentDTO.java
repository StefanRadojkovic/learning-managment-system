package com.example.predmet.dto.student;

import com.example.predmet.dto.godina.studija.GodinaStudijaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentNaGodiniInStudentDTO {
    private Long id;
    private LocalDate datumUpisa;
    private String brojIndeksa;
    private GodinaStudijaDTO godinaStudija;
}

