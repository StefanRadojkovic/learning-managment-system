package com.example.predmet.dto.nastavnik.na.realizaciji;

import com.example.predmet.dto.student.StudentNaGodiniInStudentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentInPredmetDTO implements Serializable {
    protected Long user;
    List<StudentNaGodiniInStudentDTO> studentNaGodini = new ArrayList<>();
}
