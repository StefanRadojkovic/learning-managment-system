package com.example.predmet.dto.pohadjanje.predmeta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ListOfPohadjanjePredmetaDTO {
    private List<PohadjanjePredmetaWithoutStudentDTO> list;
}
