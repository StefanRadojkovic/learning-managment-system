package com.example.predmet.dto.predmet;

import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetSimpleDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer godina;

    public PredmetSimpleDTO(Predmet predmet) {
        id = predmet.getId();
        naziv = predmet.getNaziv();
        espb = predmet.getEspb();
        brojPredavanja = predmet.getBrojPredavanja();
        brojVezbi = predmet.getBrojVezbi();
        godina = predmet.getGodinaStudija().getGodina();
    }
}

