package com.example.predmet.repository.impl;


import com.example.predmet.entity.GodinaStudija;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface GodinaStudijaRepository extends BaseRepository<GodinaStudija, Long> {

    List<GodinaStudija> findByStudijskiProgram(Long aLong);
    List<GodinaStudija> findByGodinaAndStudijskiProgram(Integer godina, Long studijskiProgram);
}
