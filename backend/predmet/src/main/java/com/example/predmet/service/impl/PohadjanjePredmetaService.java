package com.example.predmet.service.impl;

import com.example.predmet.config.security.UsernamePasswordAuthenticationWithToken;
import com.example.predmet.dto.pohadjanje.predmeta.ListOfPohadjanjePredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.ObavestenjeInPPSaNazivomPredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PohadjanjePredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PohadjanjePredmetaWithoutStudentDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PredmetInPPDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PredmetInPPPolozeniDTO;
import com.example.predmet.dto.student.StudentDTO;
import com.example.predmet.entity.PohadjanjePredmeta;
import com.example.predmet.repository.impl.PohadjanjePredmetaRepository;
import com.example.predmet.service.base.BaseService;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Service
public class PohadjanjePredmetaService extends BaseService<PohadjanjePredmeta, PohadjanjePredmetaDTO, Long> {

    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;
    private final PohadjanjePredmetaRepository repository;

    public PohadjanjePredmetaService(PohadjanjePredmetaRepository repository, WebClient webClient, EurekaClient eurekaClient) {
        super(repository);
        this.webClient = webClient;
        this.eurekaClient = eurekaClient;
        this.repository = repository;
    }

    @Override
    public PohadjanjePredmetaDTO convertToDTO(PohadjanjePredmeta element) {
        PohadjanjePredmetaDTO pohadjanjePredmetaDTO = new PohadjanjePredmetaDTO(element);

        InstanceInfo predmet = eurekaClient.getApplication("korisnik").getInstances().get(0);
        UsernamePasswordAuthenticationWithToken authentication = (UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication();

        StudentDTO student = webClient.get()
                .uri(String.format(
                        "%sapi/korisnik/studenti/%s",
                        predmet.getHomePageUrl(),
                        element.getStudentId()))
                .header("Authorization", String.format("Bearer %s", authentication.getToken()))
                .retrieve().bodyToMono(StudentDTO.class).block();

        pohadjanjePredmetaDTO.setStudent(student);

        return pohadjanjePredmetaDTO;
    }


    public PohadjanjePredmetaWithoutStudentDTO convertToDTOWithoutStudent(PohadjanjePredmeta element) {
        return new PohadjanjePredmetaWithoutStudentDTO(element);
    }

    public ListOfPohadjanjePredmetaDTO findAllByStudentId(Long id){
        return new ListOfPohadjanjePredmetaDTO(generateList(repository.findAllByStudentId(id)));
    }

    public ListOfPohadjanjePredmetaDTO findAllByStudentIdCurrentlyFollowed(Long id){
        return new ListOfPohadjanjePredmetaDTO(generateList(repository.findAllByStudentIdAndKonacnaOcenaIsNull(id)));
    }

    public List<PredmetInPPPolozeniDTO> findAllByStudentIdPolozeni(Long id){
        List<PohadjanjePredmetaWithoutStudentDTO> pohadjanjePredmetaWithoutStudentDTOS = generateList(repository.findAllByStudentIdAndKonacnaOcenaIsNotNull(id));
        List<PredmetInPPPolozeniDTO> predmeti = new ArrayList<>();
        pohadjanjePredmetaWithoutStudentDTOS.forEach(pohadjanje -> {
            PredmetInPPPolozeniDTO predmet = new PredmetInPPPolozeniDTO(pohadjanje.getPredmet());
            predmet.setBrojPokusaja(pohadjanje.getBrojPokusaja());
            predmet.setKonacnaOcena(pohadjanje.getKonacnaOcena());
            predmeti.add(predmet);
        });
        return predmeti;
    }

    public List<PredmetInPPDTO> findAllByStudentIdCurrentlyFollowedReturnList(Long id){
        List<PohadjanjePredmetaWithoutStudentDTO> pohadjanjePredmetaWithoutStudentDTOS = generateList(repository.findAllByStudentIdAndKonacnaOcenaIsNull(id));
        List<PredmetInPPDTO> predmeti = new ArrayList<>();
        pohadjanjePredmetaWithoutStudentDTOS.forEach(pred -> predmeti.add(pred.getPredmet()));
        return predmeti;
    }

    public List<PohadjanjePredmetaWithoutStudentDTO> generateList(List<PohadjanjePredmeta> elements) {
        List<PohadjanjePredmetaWithoutStudentDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(this.convertToDTOWithoutStudent(element));

        });
        return newList;
    }

    public List<ObavestenjeInPPSaNazivomPredmetaDTO> findAllByStudentIdCurrentlyFollowedReturnListOfObavestenja(Long id){
        List<PohadjanjePredmetaWithoutStudentDTO> pohadjanjePredmetaWithoutStudentDTOS = generateList(repository.findAllByStudentIdAndKonacnaOcenaIsNull(id));
        List<ObavestenjeInPPSaNazivomPredmetaDTO> obavestenja = new ArrayList<>();
        pohadjanjePredmetaWithoutStudentDTOS
                .forEach(pred -> {
                    PredmetInPPDTO predmet = pred.getPredmet();
                    predmet.getObavestenja().forEach(obavestenje -> {
                        ObavestenjeInPPSaNazivomPredmetaDTO obavestenje1 = new ObavestenjeInPPSaNazivomPredmetaDTO(obavestenje);
                        obavestenje1.setNazivPredmeta(predmet.getNaziv());
                        obavestenja.add(obavestenje1);
                    });
                });

        return obavestenja;
    }
}
