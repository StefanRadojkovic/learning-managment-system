package com.example.predmet.config.korisnik;

import com.example.predmet.config.korisnik.surrogate.AuthorityDTOinUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Long id;
    private String username;
    private String password;
    Set<AuthorityDTOinUser> authorities;
}
