package com.example.predmet.dto.obavestenje;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeRequest {
    private String opis;
    private String title;
    private Long predmetId;
}

