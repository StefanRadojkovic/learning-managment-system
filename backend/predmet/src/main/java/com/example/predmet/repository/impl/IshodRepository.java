package com.example.predmet.repository.impl;


import com.example.predmet.entity.Ishod;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface IshodRepository extends BaseRepository<Ishod, Long> {

    List<Ishod> findAllByPredmetPredavanje_IdOrPredmetVezbe_Id(Long predavanjeId, Long vezbeId);

    List<Ishod> findAllByPredmetVezbe_Id(Long predavanjeId);

    List<Ishod> findAllByPredmetPredavanje_Id(Long predavanjeId);
}
