package com.example.predmet.dto.ishod;

import com.example.predmet.entity.Ishod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IshodDTO {
    private Long id;
    private String opis;
    private Integer termin;
    private PredmetInIshodDTO predmetPredavanje;
    private PredmetInIshodDTO predmetVezbe;

    public IshodDTO(Ishod ishod) {
        id = ishod.getId();
        opis = ishod.getOpis();
        termin = ishod.getTermin();
        if(ishod.getPredmetPredavanje() != null) {
            predmetPredavanje = new PredmetInIshodDTO(ishod.getPredmetPredavanje());
        }
        if(ishod.getPredmetVezbe() != null) {
            predmetVezbe = new PredmetInIshodDTO(ishod.getPredmetVezbe());
        }
    }
}

