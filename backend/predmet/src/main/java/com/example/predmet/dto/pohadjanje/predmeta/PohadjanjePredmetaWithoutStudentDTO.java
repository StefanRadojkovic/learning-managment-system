package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.entity.PohadjanjePredmeta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PohadjanjePredmetaWithoutStudentDTO {
    private Long id;
    private Integer konacnaOcena;
    private Integer brojPokusaja;
    private PredmetInPPDTO predmet;

    public PohadjanjePredmetaWithoutStudentDTO(PohadjanjePredmeta element) {
        id = element.getId();
        konacnaOcena = element.getKonacnaOcena();
        brojPokusaja = element.getBrojPokusaja();
        predmet = new PredmetInPPDTO(element.getPredmet());
    }
}

