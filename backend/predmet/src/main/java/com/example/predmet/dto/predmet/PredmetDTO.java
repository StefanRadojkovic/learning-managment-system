package com.example.predmet.dto.predmet;

import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private GodinaStudijaInPredmetDTO godinaStudija;
    private List<IshodInPredmetDTO> silabusPredavanja = new ArrayList<>();
    private List<IshodInPredmetDTO> silabusVezbe = new ArrayList<>();
    private List<ObavestenjeInPredmetDTO> obavestenja = new ArrayList<>();
    private List<PohadjanjePredmetaInPredmetDTO> pohadjanjePredmeta = new ArrayList<>();
    private List<NastavnikNaRealizacijiInPredmetDTO> nastavniciNaRealizaciji = new ArrayList<>();
    private List<InstrumentEvaluacijeInPredmetDTO> instrumentiEvaluacije = new ArrayList<>();

    public PredmetDTO(Predmet predmet) {
        id = predmet.getId();
        naziv = predmet.getNaziv();
        espb = predmet.getEspb();
        obavezan = predmet.getObavezan();
        brojPredavanja = predmet.getBrojPredavanja();
        brojVezbi = predmet.getBrojVezbi();
        drugiObliciNastave = predmet.getDrugiObliciNastave();
        istrazivackiRad = predmet.getIstrazivackiRad();
        ostaliCasovi = predmet.getOstaliCasovi();
        godinaStudija = new GodinaStudijaInPredmetDTO(predmet.getGodinaStudija());
        predmet.getSilabusPredavanja().forEach(ishod -> silabusPredavanja.add(new IshodInPredmetDTO(ishod)));
        predmet.getSilabusVezbe().forEach(ishod -> silabusVezbe.add(new IshodInPredmetDTO(ishod)));
        predmet.getObavestenja().forEach(obavestenje -> obavestenja.add(new ObavestenjeInPredmetDTO(obavestenje)));
        predmet.getPohadjanjePredmeta().forEach(pohadjanjePredmeta1 -> pohadjanjePredmeta.add(new PohadjanjePredmetaInPredmetDTO(pohadjanjePredmeta1)));
        predmet.getNastavniciNaRealizaciji().forEach(nastavnik -> nastavniciNaRealizaciji.add(new NastavnikNaRealizacijiInPredmetDTO(nastavnik)));
        predmet.getInstrumentiEvaluacije().forEach(evaluacija -> instrumentiEvaluacije.add(new InstrumentEvaluacijeInPredmetDTO(evaluacija)));
    }
}

