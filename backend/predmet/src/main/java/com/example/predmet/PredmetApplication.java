package com.example.predmet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PredmetApplication {

	public static void main(String[] args) {
		SpringApplication.run(PredmetApplication.class, args);
	}

}
