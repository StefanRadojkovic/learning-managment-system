package com.example.predmet.repository.impl;


import com.example.predmet.entity.NastavnikNaRealizaciji;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface NastavnikNaRealizacijiRepository extends BaseRepository<NastavnikNaRealizaciji, Long> {

    List<NastavnikNaRealizaciji> findAllByNastavnikId(Long id);
}
