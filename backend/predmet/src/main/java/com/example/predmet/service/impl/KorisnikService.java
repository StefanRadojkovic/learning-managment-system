package com.example.predmet.service.impl;

import com.example.predmet.config.security.UsernamePasswordAuthenticationWithToken;
import com.example.predmet.dto.student.StudentDTO;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class KorisnikService {

    @Autowired
    private final WebClient webClient;
    private final EurekaClient eurekaClient;

    public StudentDTO getStudentByIdForNastavnikNaRealizaciji(Long id){
        return webClient.get()
                .uri(String.format(
                        "%sapi/korisnik/studenti/%s",
                        getPredmetInstance().getHomePageUrl(),
                        id))
                .header("Authorization", getBearerToken())
                .retrieve()
                .bodyToMono(StudentDTO.class)
                .block();
    }

    private InstanceInfo getPredmetInstance(){
        return eurekaClient.getApplication("korisnik").getInstances().get(0);
    }

    private String getBearerToken(){
        String token = ((UsernamePasswordAuthenticationWithToken) SecurityContextHolder.getContext().getAuthentication()).getToken();
        return String.format("Bearer %s", token);
    }
}
