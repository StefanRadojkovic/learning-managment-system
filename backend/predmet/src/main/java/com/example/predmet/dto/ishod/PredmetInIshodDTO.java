package com.example.predmet.dto.ishod;

import com.example.predmet.dto.predmet.GodinaStudijaInPredmetDTO;
import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetInIshodDTO {
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;
    private GodinaStudijaInPredmetDTO godinaStudija;

    public PredmetInIshodDTO(Predmet predmet) {
        id = predmet.getId();
        naziv = predmet.getNaziv();
        espb = predmet.getEspb();
        obavezan = predmet.getObavezan();
        brojPredavanja = predmet.getBrojPredavanja();
        brojVezbi = predmet.getBrojVezbi();
        drugiObliciNastave = predmet.getDrugiObliciNastave();
        istrazivackiRad = predmet.getIstrazivackiRad();
        ostaliCasovi = predmet.getOstaliCasovi();
        godinaStudija = new GodinaStudijaInPredmetDTO(predmet.getGodinaStudija());
    }
}

