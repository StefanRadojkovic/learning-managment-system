package com.example.predmet.dto.tretnutni.studenti;


import com.example.predmet.entity.PohadjanjePredmeta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PohadjanjePredmetaTrenutniStud {
    private Long studentId;

    public PohadjanjePredmetaTrenutniStud(PohadjanjePredmeta poh) {
        studentId = poh.getStudentId();
    }
}
