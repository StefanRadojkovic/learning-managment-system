package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.entity.GodinaStudija;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GodinaStudijaInPPDTO {
    private Long id;
    private Integer godina;
    private Long studijskiProgram;

    public GodinaStudijaInPPDTO(GodinaStudija godinaStudija) {
        id = godinaStudija.getId();
        godina = godinaStudija.getGodina();
        studijskiProgram = godinaStudija.getStudijskiProgram();
    }
}

