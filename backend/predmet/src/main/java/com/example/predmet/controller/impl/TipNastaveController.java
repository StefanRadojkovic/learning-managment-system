package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.tip.nastave.TipNastaveDTO;
import com.example.predmet.entity.TipNastave;
import com.example.predmet.service.impl.TipNastaveService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/predmet/tipovi-nastave")
public class TipNastaveController extends BaseController<TipNastave, TipNastaveDTO, Long> {
    public TipNastaveController(TipNastaveService service) {
        super(service);
    }
}
