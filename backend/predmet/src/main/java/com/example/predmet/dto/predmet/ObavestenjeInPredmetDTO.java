package com.example.predmet.dto.predmet;

import com.example.predmet.entity.Obavestenje;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeInPredmetDTO {
    private Long id;
    private String opis;
    private String title;

    public ObavestenjeInPredmetDTO(Obavestenje obavestenje) {
        id = obavestenje.getId();
        opis = obavestenje.getOpis();
        title = obavestenje.getTitle();
    }
}

