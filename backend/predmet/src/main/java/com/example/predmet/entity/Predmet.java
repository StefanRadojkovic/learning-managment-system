package com.example.predmet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Predmet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naziv;
    private Integer espb;
    private Boolean obavezan;
    private Integer brojPredavanja;
    private Integer brojVezbi;
    private Integer drugiObliciNastave;
    private Integer istrazivackiRad;
    private Integer ostaliCasovi;

    @ToString.Exclude
    @ManyToOne
    private GodinaStudija godinaStudija;

    @ToString.Exclude
    @OneToMany(mappedBy = "predmetPredavanje")
    private List<Ishod> silabusPredavanja = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "predmetVezbe")
    private List<Ishod> silabusVezbe = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "predmet")
    private List<Obavestenje> obavestenja = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "predmet")
    private List<PohadjanjePredmeta> pohadjanjePredmeta = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "predmet")
    private List<NastavnikNaRealizaciji> nastavniciNaRealizaciji = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "predmet")
    private List<InstrumentEvaluacije> instrumentiEvaluacije = new ArrayList<>();
}

