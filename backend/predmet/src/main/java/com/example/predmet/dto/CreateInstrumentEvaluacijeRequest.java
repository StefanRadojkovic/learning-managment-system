package com.example.predmet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateInstrumentEvaluacijeRequest {

    private String opis;
    private String instrument;
    private Long predmetId;
}
