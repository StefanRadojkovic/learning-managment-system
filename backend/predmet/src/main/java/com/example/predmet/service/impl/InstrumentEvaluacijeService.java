package com.example.predmet.service.impl;

import com.example.predmet.dto.CreateInstrumentEvaluacijeRequest;
import com.example.predmet.dto.instrument.evaluacije.InstrumentEvaluacijeDTO;
import com.example.predmet.dto.instrument.evaluacije.InstrumentEvaluacijeSimpleDTO;
import com.example.predmet.entity.InstrumentEvaluacije;
import com.example.predmet.repository.impl.InstrumentEvaluacijeRepository;
import com.example.predmet.repository.impl.PredmetRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InstrumentEvaluacijeService extends BaseService<InstrumentEvaluacije, InstrumentEvaluacijeDTO, Long> {

    private final InstrumentEvaluacijeRepository repository;
    private final PredmetRepository predmetRepository;

    public InstrumentEvaluacijeService(InstrumentEvaluacijeRepository repository, InstrumentEvaluacijeRepository repository1, PredmetRepository predmetRepository) {
        super(repository);
        this.repository = repository1;
        this.predmetRepository = predmetRepository;
    }

    @Override
    public InstrumentEvaluacijeDTO convertToDTO(InstrumentEvaluacije element) {
        return new InstrumentEvaluacijeDTO(element);
    }

    public InstrumentEvaluacijeDTO update(Long id, InstrumentEvaluacije changedT){
        InstrumentEvaluacije instrumentEvaluacije = repository.getById(id);

        instrumentEvaluacije.setInstrument(changedT.getInstrument());
        instrumentEvaluacije.setOpis(changedT.getOpis());

        return convertToDTO(repository.save(instrumentEvaluacije));
    }

    public InstrumentEvaluacijeDTO createEvaluacija(CreateInstrumentEvaluacijeRequest changedT) {
        InstrumentEvaluacije instrumentEvaluacije = new InstrumentEvaluacije();
        instrumentEvaluacije.setInstrument(changedT.getInstrument());
        instrumentEvaluacije.setPredmet(predmetRepository.getById(changedT.getPredmetId()));
        instrumentEvaluacije.setOpis(changedT.getOpis());

        return convertToDTO(repository.save(instrumentEvaluacije));
    }

    public List<InstrumentEvaluacijeSimpleDTO> getInstrumenteForPredmet(Long predmetId) {
        return generateSimpleList(repository.findAllByPredmet_Id(predmetId));
    }

    public InstrumentEvaluacijeSimpleDTO convertToSimpleDTO(InstrumentEvaluacije element){
        return new InstrumentEvaluacijeSimpleDTO(element);
    }

    public List<InstrumentEvaluacijeSimpleDTO> generateSimpleList(Iterable<InstrumentEvaluacije> elements) {
        List<InstrumentEvaluacijeSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> newList.add(this.convertToSimpleDTO(element)));
        return newList;
    }
}
