package com.example.predmet.dto.predmet;

import com.example.predmet.entity.NastavnikNaRealizaciji;
import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NastavnikNaRealizacijiInPredmetDTO {
    private Long id;
    private Integer brojCasova;
    private Predmet predmet;

    public NastavnikNaRealizacijiInPredmetDTO(NastavnikNaRealizaciji element) {
        id = element.getId();
        brojCasova = element.getBrojCasova();
    }
}

