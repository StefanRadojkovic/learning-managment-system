package com.example.predmet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class KoristiSeNegdeException extends RuntimeException{
    public KoristiSeNegdeException() {
        super("Entitet se koristi negde");
    }
}
