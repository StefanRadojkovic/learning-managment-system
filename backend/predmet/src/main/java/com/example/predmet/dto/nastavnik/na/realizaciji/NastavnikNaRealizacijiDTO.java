package com.example.predmet.dto.nastavnik.na.realizaciji;

import com.example.predmet.entity.NastavnikNaRealizaciji;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NastavnikNaRealizacijiDTO {
    private Long id;
    private Integer brojCasova;
    private PredmetInNastavnikNaRealizacijiDTO predmet;

    public NastavnikNaRealizacijiDTO(NastavnikNaRealizaciji element) {
        id = element.getId();
        brojCasova = element.getBrojCasova();
        predmet = new PredmetInNastavnikNaRealizacijiDTO(element.getPredmet());
    }
}

