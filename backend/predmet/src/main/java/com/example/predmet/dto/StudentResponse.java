package com.example.predmet.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentResponse {
    private String ime;
    private String brojIndeksa;
    private String godinaUpisa;
    private String konacnaOcena;
}
