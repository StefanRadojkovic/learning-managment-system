package com.example.predmet.repository.impl;


import com.example.predmet.entity.Obavestenje;
import com.example.predmet.repository.base.BaseRepository;

public interface ObavestenjeRepository extends BaseRepository<Obavestenje, Long> {
}
