package com.example.predmet.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentDTO implements Serializable {
    private Long id;
    private String username;
    private String ime;
    private String jmbg;
    List<StudentNaGodiniInStudentDTO> studentNaGodini = new ArrayList<>();
}
