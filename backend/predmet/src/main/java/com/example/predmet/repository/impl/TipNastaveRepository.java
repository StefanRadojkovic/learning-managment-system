package com.example.predmet.repository.impl;


import com.example.predmet.entity.TipNastave;
import com.example.predmet.repository.base.BaseRepository;

public interface TipNastaveRepository extends BaseRepository<TipNastave, Long> {
}
