package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.dto.student.StudentDTO;
import com.example.predmet.entity.PohadjanjePredmeta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PohadjanjePredmetaDTO {
    private Long id;
    private Integer konacnaOcena;
    private Integer brojPokusaja;
    private PredmetInPPDTO predmet;
    private StudentDTO student;

    public PohadjanjePredmetaDTO(PohadjanjePredmeta element) {
        id = element.getId();
        brojPokusaja = element.getBrojPokusaja();
        konacnaOcena = element.getKonacnaOcena();
        predmet = new PredmetInPPDTO(element.getPredmet());
    }
}

