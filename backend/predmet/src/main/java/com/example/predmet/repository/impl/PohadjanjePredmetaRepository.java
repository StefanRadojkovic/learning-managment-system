package com.example.predmet.repository.impl;


import com.example.predmet.entity.PohadjanjePredmeta;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface PohadjanjePredmetaRepository extends BaseRepository<PohadjanjePredmeta, Long> {

    List<PohadjanjePredmeta> findAllByStudentId(Long id);

    List<PohadjanjePredmeta> findAllByStudentIdAndKonacnaOcenaIsNull(Long id);

    List<PohadjanjePredmeta> findAllByStudentIdAndKonacnaOcenaIsNotNull(Long id);

    List<PohadjanjePredmeta> findAllByPredmet_IdAndStudentId(Long predmetId, Long studentId);
}
