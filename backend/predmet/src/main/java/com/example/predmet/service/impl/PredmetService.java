package com.example.predmet.service.impl;

import com.example.predmet.dto.pohadjanje.predmeta.ObavestenjeInPPDTO;
import com.example.predmet.dto.predmet.PredmetDTO;
import com.example.predmet.dto.predmet.PredmetSimpleDTO;
import com.example.predmet.entity.GodinaStudija;
import com.example.predmet.entity.Predmet;
import com.example.predmet.exception.KoristiSeNegdeException;
import com.example.predmet.repository.impl.GodinaStudijaRepository;
import com.example.predmet.repository.impl.PredmetRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PredmetService extends BaseService<Predmet, PredmetDTO, Long> {

    private PredmetRepository predmetRepository;
    private final GodinaStudijaRepository godinaStudijaRepository;

    public PredmetService(PredmetRepository repository, GodinaStudijaRepository godinaStudijaRepository) {
        super(repository);
        predmetRepository = repository;
        this.godinaStudijaRepository = godinaStudijaRepository;
    }

    @Override
    public PredmetDTO convertToDTO(Predmet element) {
        return new PredmetDTO(element);
    }

    public List<ObavestenjeInPPDTO> findAllByNastavnikIdReturnListOfObavestenja(Long id) {
        Predmet predmet = predmetRepository.findById(id).get();
        List<ObavestenjeInPPDTO> obavestenja = new ArrayList<>();

        predmet.getObavestenja().forEach(obavestenje -> obavestenja.add(new ObavestenjeInPPDTO(obavestenje)));

        return obavestenja;
    }

    public List<PredmetSimpleDTO> findAllByGodinaStudija(Long godinaStudijaId) {
        List<Predmet> predmeti = predmetRepository.findAllByGodinaStudija_Id(godinaStudijaId);
        return generateListSimpleDto(predmeti);
    }

    public PredmetSimpleDTO convertToSimpleDTO(Predmet element) {
        return new PredmetSimpleDTO(element);
    }

    public List<PredmetSimpleDTO> generateListSimpleDto(List<Predmet> elements) {
        List<PredmetSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            newList.add(convertToSimpleDTO(element));

        });
        return newList;
    }

    public void createPredmet(Long id, PredmetSimpleDTO simpleDTO) {
        GodinaStudija godinaStudija = godinaStudijaRepository.findByGodinaAndStudijskiProgram(simpleDTO.getGodina(), id).get(0);

        Predmet predmet = new Predmet();
        predmet.setBrojPredavanja(simpleDTO.getBrojPredavanja());
        predmet.setBrojVezbi(simpleDTO.getBrojVezbi());
        predmet.setEspb(simpleDTO.getEspb());
        predmet.setNaziv(simpleDTO.getNaziv());
        predmet.setGodinaStudija(godinaStudija);

        predmetRepository.save(predmet);
    }

    public void updatePredmet(Long predmetId, PredmetSimpleDTO simpleDTO) {
        Predmet predmet = predmetRepository.findById(predmetId).get();
        GodinaStudija godinaStudija = godinaStudijaRepository.findByGodinaAndStudijskiProgram(
                simpleDTO.getGodina(),
                predmet.getGodinaStudija().getStudijskiProgram()
                ).get(0);

        predmet.setBrojPredavanja(simpleDTO.getBrojPredavanja());
        predmet.setBrojVezbi(simpleDTO.getBrojVezbi());
        predmet.setEspb(simpleDTO.getEspb());
        predmet.setNaziv(simpleDTO.getNaziv());
        predmet.setGodinaStudija(godinaStudija);

        predmetRepository.save(predmet);
    }

    public List<Integer> findGodineByPredmetId(Long predmetId) {
        Predmet predmet = predmetRepository.findById(predmetId).get();

        GodinaStudija godinaStudija = godinaStudijaRepository.findById(predmet.getGodinaStudija().getId()).get();
        List<GodinaStudija> byStudijskiProgram = godinaStudijaRepository.findByStudijskiProgram(godinaStudija.getStudijskiProgram());
        List<Integer> response = new ArrayList<>();

        byStudijskiProgram.forEach(godina -> response.add(godina.getGodina()));

        return response;
    }

    public void deletePredmet(Long predmetId) {
        PredmetDTO predmet = findOne(predmetId);
        if(!predmet.getPohadjanjePredmeta().isEmpty()){
            throw new KoristiSeNegdeException();
        }
        predmetRepository.deleteById(predmetId);
    }
}
