package com.example.predmet.dto.predmet;

import com.example.predmet.entity.InstrumentEvaluacije;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class InstrumentEvaluacijeInPredmetDTO {
    private Long id;
    private String opis;
    private String instrument;

    public InstrumentEvaluacijeInPredmetDTO(InstrumentEvaluacije evaluacija) {
        id = evaluacija.getId();
        opis = evaluacija.getOpis();
        instrument = evaluacija.getInstrument();
    }
}

