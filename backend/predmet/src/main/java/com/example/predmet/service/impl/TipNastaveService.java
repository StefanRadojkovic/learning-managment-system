package com.example.predmet.service.impl;

import com.example.predmet.dto.tip.nastave.TipNastaveDTO;
import com.example.predmet.entity.TipNastave;
import com.example.predmet.repository.impl.TipNastaveRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class TipNastaveService extends BaseService<TipNastave, TipNastaveDTO, Long> {
    public TipNastaveService(TipNastaveRepository repository) {
        super(repository);
    }

    @Override
    public TipNastaveDTO convertToDTO(TipNastave element) {
        return new TipNastaveDTO(element);
    }
}
