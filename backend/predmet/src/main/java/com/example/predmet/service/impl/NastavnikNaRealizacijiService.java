package com.example.predmet.service.impl;

import com.example.predmet.dto.nastavnik.na.realizaciji.NastavnikNaRealizacijiDTO;
import com.example.predmet.dto.nastavnik.na.realizaciji.PredmetInNastavnikNaRealizacijiDTO;
import com.example.predmet.dto.student.StudentDTO;
import com.example.predmet.dto.student.StudentInNastavnikTrenutniDTO;
import com.example.predmet.dto.tretnutni.studenti.PredmetTrenutniStudenti;
import com.example.predmet.entity.NastavnikNaRealizaciji;
import com.example.predmet.entity.PohadjanjePredmeta;
import com.example.predmet.repository.impl.NastavnikNaRealizacijiRepository;
import com.example.predmet.repository.impl.PohadjanjePredmetaRepository;
import com.example.predmet.repository.impl.PredmetRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NastavnikNaRealizacijiService extends BaseService<NastavnikNaRealizaciji, NastavnikNaRealizacijiDTO, Long> {

    private final KorisnikService korisnikService;
    private final PredmetRepository predmetRepository;
    private final PohadjanjePredmetaRepository pohadjanjePredmetaRepository;

    private NastavnikNaRealizacijiRepository nastavnikNaRealizacijiRepository;

    public NastavnikNaRealizacijiService(KorisnikService korisnikService, PredmetRepository predmetRepository, PohadjanjePredmetaRepository pohadjanjePredmetaRepository, NastavnikNaRealizacijiRepository repository) {
        super(repository);
        this.korisnikService = korisnikService;
        this.predmetRepository = predmetRepository;
        this.pohadjanjePredmetaRepository = pohadjanjePredmetaRepository;
        nastavnikNaRealizacijiRepository = repository;
    }

    @Override
    public NastavnikNaRealizacijiDTO convertToDTO(NastavnikNaRealizaciji element) {
        return new NastavnikNaRealizacijiDTO(element);
    }

    public List<NastavnikNaRealizacijiDTO> findAllByNastavnikId(Long id){
        List<NastavnikNaRealizaciji> nastavniciNaRealizaciji = nastavnikNaRealizacijiRepository.findAllByNastavnikId(id);
        List<NastavnikNaRealizacijiDTO> nastavnikNaRealizacijiDTO = generateList(nastavniciNaRealizaciji);

        for (int i=0; i < nastavniciNaRealizaciji.size(); i++){
            for (int n=0; n < nastavniciNaRealizaciji.get(i).getPredmet().getPohadjanjePredmeta().size(); n++){

                PohadjanjePredmeta pohadjanjePredmeta = nastavniciNaRealizaciji.get(i).getPredmet().getPohadjanjePredmeta().get(n);
                StudentDTO student = korisnikService.getStudentByIdForNastavnikNaRealizaciji(pohadjanjePredmeta.getStudentId());

                nastavnikNaRealizacijiDTO.get(i).getPredmet().getPohadjanjePredmeta().get(n).setStudent(student);
            }
        }

        return nastavnikNaRealizacijiDTO;
    }

    public List<PredmetInNastavnikNaRealizacijiDTO> findAllByNastavnikIdReturnListOfPredmet(Long id){
        List<NastavnikNaRealizaciji> nastavniciNaRealizaciji = nastavnikNaRealizacijiRepository.findAllByNastavnikId(id);
        List<NastavnikNaRealizacijiDTO> nastavnikNaRealizacijiDTO = generateList(nastavniciNaRealizaciji);
        List<PredmetInNastavnikNaRealizacijiDTO> predmeti = new ArrayList<>();

        nastavnikNaRealizacijiDTO.forEach(realizacija -> predmeti.add(realizacija.getPredmet()));

        return predmeti;
    }

    public List<StudentInNastavnikTrenutniDTO> findAllStudentiByPredmetId(Long id) {
        PredmetTrenutniStudenti predmet = new PredmetTrenutniStudenti(predmetRepository.findById(id).get());
        List<StudentInNastavnikTrenutniDTO> studenti = new ArrayList<>();
        predmet.getPohadjanjePredmeta()
                .forEach(pohadjanjePredmeta ->
                        studenti.add(new StudentInNastavnikTrenutniDTO(korisnikService.getStudentByIdForNastavnikNaRealizaciji(pohadjanjePredmeta.getStudentId())))
                );

        return studenti;
    }

    public void dodajOcenu(Long studentId, Long predmetId, Integer ocena) {
        PohadjanjePredmeta pohadjanjePredmeta = pohadjanjePredmetaRepository.findAllByPredmet_IdAndStudentId(predmetId, studentId).get(0);
        pohadjanjePredmeta.setKonacnaOcena(ocena);
        pohadjanjePredmeta.setBrojPokusaja(1);
        pohadjanjePredmetaRepository.save(pohadjanjePredmeta);
    }
}
