package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.godina.studija.GodinaStudijaDTO;
import com.example.predmet.dto.predmet.PredmetSimpleDTO;
import com.example.predmet.entity.GodinaStudija;
import com.example.predmet.service.impl.GodinaStudijaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/godine-studija")
public class GodinaStudijaController extends BaseController<GodinaStudija, GodinaStudijaDTO, Long> {

    private GodinaStudijaService godinaStudijaService;

    public GodinaStudijaController(GodinaStudijaService service) {
        super(service);
        godinaStudijaService = service;
    }

    @GetMapping("/find-by-studijski-program/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<GodinaStudijaDTO> findByStudijskiProgram(@PathVariable("id") Long id)
    {
        return godinaStudijaService.findByStudijskiProgram(id);
    }

    @GetMapping("/studijski-program/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PredmetSimpleDTO> findByStudijskiProgramReturnPredmeti(@PathVariable("id") Long id)
    {
        return godinaStudijaService.findByStudijskiProgramReturnPredmeti(id);
    }

    @GetMapping("/all/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Integer> findGodineByStudijskiProgramId(@PathVariable("id") Long studijskiProgramId)
    {
        return godinaStudijaService.findGodineByStudijskiProgramId(studijskiProgramId);
    }
}
