package com.example.predmet.repository.impl;


import com.example.predmet.entity.Predmet;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface PredmetRepository extends BaseRepository<Predmet, Long> {

    List<Predmet> findAllByGodinaStudija_Id(Long godinaStudijaId);
}
