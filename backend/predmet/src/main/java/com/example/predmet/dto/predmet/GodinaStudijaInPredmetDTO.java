package com.example.predmet.dto.predmet;

import com.example.predmet.entity.GodinaStudija;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GodinaStudijaInPredmetDTO {
    private Long id;
    private Integer godina;
    private Long studijskiProgram;

    public GodinaStudijaInPredmetDTO(GodinaStudija godinaStudija) {
        id = godinaStudija.getId();
        godina = godinaStudija.getGodina();
        studijskiProgram = godinaStudija.getStudijskiProgram();
    }
}

