package com.example.predmet.dto.pohadjanje.predmeta;

import com.example.predmet.entity.Obavestenje;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeInPPDTO {
    private Long id;
    private String opis;
    private String title;

    public ObavestenjeInPPDTO(Obavestenje obavestenje) {
        id = obavestenje.getId();
        opis = obavestenje.getOpis();
        title = obavestenje.getTitle();
    }
}

