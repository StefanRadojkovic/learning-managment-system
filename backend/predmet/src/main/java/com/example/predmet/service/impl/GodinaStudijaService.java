package com.example.predmet.service.impl;

import com.example.predmet.dto.godina.studija.GodinaStudijaDTO;
import com.example.predmet.dto.predmet.PredmetSimpleDTO;
import com.example.predmet.entity.GodinaStudija;
import com.example.predmet.entity.Predmet;
import com.example.predmet.repository.impl.GodinaStudijaRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GodinaStudijaService extends BaseService<GodinaStudija, GodinaStudijaDTO, Long> {

    private GodinaStudijaRepository godinaStudijaRepository;

    public GodinaStudijaService(GodinaStudijaRepository repository) {
        super(repository);
        godinaStudijaRepository = repository;
    }

    @Override
    public GodinaStudijaDTO convertToDTO(GodinaStudija element) {
        return new GodinaStudijaDTO(element);
    }

    public List<GodinaStudijaDTO> findByStudijskiProgram(Long id){
        return generateList(godinaStudijaRepository.findByStudijskiProgram(id));
    }

    public List<PredmetSimpleDTO> findByStudijskiProgramReturnPredmeti(Long id) {
        return generateListSimpleDto(godinaStudijaRepository.findByStudijskiProgram(id));
    }

    public PredmetSimpleDTO convertToSimpleDTO(Predmet element) {
        return new PredmetSimpleDTO(element);
    }

    public List<PredmetSimpleDTO> generateListSimpleDto(List<GodinaStudija> elements) {
        List<PredmetSimpleDTO> newList = new ArrayList<>();
        elements.forEach(element -> {
            element.getPredmeti().forEach(predmet -> newList.add(convertToSimpleDTO(predmet)));
        });
        return newList;
    }

    public List<Integer> findGodineByStudijskiProgramId(Long studijskiProgramId) {
        List<GodinaStudija> godinaStudija = godinaStudijaRepository.findByStudijskiProgram(studijskiProgramId);

        List<Integer> godine = new ArrayList<>();
        godinaStudija.forEach(god -> godine.add(god.getGodina()));
        return godine;
    }
}
