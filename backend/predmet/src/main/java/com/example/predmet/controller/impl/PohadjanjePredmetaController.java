package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.pohadjanje.predmeta.ListOfPohadjanjePredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.ObavestenjeInPPSaNazivomPredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PohadjanjePredmetaDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PredmetInPPDTO;
import com.example.predmet.dto.pohadjanje.predmeta.PredmetInPPPolozeniDTO;
import com.example.predmet.entity.PohadjanjePredmeta;
import com.example.predmet.service.impl.PohadjanjePredmetaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/pohadjanje-predmeta")
public class PohadjanjePredmetaController extends BaseController<PohadjanjePredmeta, PohadjanjePredmetaDTO, Long> {
    private final PohadjanjePredmetaService pohadjanjePredmetaService;

    public PohadjanjePredmetaController(PohadjanjePredmetaService service) {
        super(service);
        pohadjanjePredmetaService = service;
    }

    @GetMapping(path = "/student/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ListOfPohadjanjePredmetaDTO findOneWithoutStudent(@PathVariable("id") Long id){
        return pohadjanjePredmetaService.findAllByStudentId(id);
    }

    @GetMapping(path = "/student/trenutno-slusani/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ListOfPohadjanjePredmetaDTO findOneWithoutStudentCurrentlyFollowed(@PathVariable("id") Long id){
        return pohadjanjePredmetaService.findAllByStudentIdCurrentlyFollowed(id);
    }

    @GetMapping(path = "/student/polozeni-predmeti/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PredmetInPPPolozeniDTO> findOneWithoutStudentPolozeni(@PathVariable("id") Long id){
        return pohadjanjePredmetaService.findAllByStudentIdPolozeni(id);
    }

    @GetMapping(path = "/student/trenutno-slusani-predmet/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PredmetInPPDTO> findOneWithoutStudentCurrentlyFollowedReturnList(@PathVariable("id") Long id){
        return pohadjanjePredmetaService.findAllByStudentIdCurrentlyFollowedReturnList(id);
    }

    @GetMapping(path = "/student/trenutno-slusani-predmet/obavestenja/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ObavestenjeInPPSaNazivomPredmetaDTO> findOneWithoutStudentCurrentlyFollowedReturnListOfObavestenja(@PathVariable("id") Long id){
        return pohadjanjePredmetaService.findAllByStudentIdCurrentlyFollowedReturnListOfObavestenja(id);
    }
}
