package com.example.predmet.dto.predmet;

import com.example.predmet.entity.Ishod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IshodInPredmetDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String opis;
    private Integer termin;

    public IshodInPredmetDTO(Ishod ishod) {
        id = ishod.getId();
        opis = ishod.getOpis();
        termin = ishod.getTermin();
    }
}

