package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.nastavnik.na.realizaciji.NastavnikNaRealizacijiDTO;
import com.example.predmet.dto.nastavnik.na.realizaciji.PredmetInNastavnikNaRealizacijiDTO;
import com.example.predmet.dto.student.StudentInNastavnikTrenutniDTO;
import com.example.predmet.entity.NastavnikNaRealizaciji;
import com.example.predmet.service.impl.NastavnikNaRealizacijiService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/nastavnik-na-realizaciji")
public class NastavnikNaRealizacijiController extends BaseController<NastavnikNaRealizaciji, NastavnikNaRealizacijiDTO, Long> {

    private NastavnikNaRealizacijiService nastavnikNaRealizacijiService;

    public NastavnikNaRealizacijiController(NastavnikNaRealizacijiService service) {
        super(service);
        nastavnikNaRealizacijiService = service;
    }

    @GetMapping(path = "/nastavnik/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<NastavnikNaRealizacijiDTO> findAllByNastavnikId(@PathVariable("id") Long id){
        return nastavnikNaRealizacijiService.findAllByNastavnikId(id);
    }

    @GetMapping(path = "/nastavnik-predmeti/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PredmetInNastavnikNaRealizacijiDTO> findAllByNastavnikIdReturnListOfPredmet(@PathVariable("id") Long id){
        return nastavnikNaRealizacijiService.findAllByNastavnikIdReturnListOfPredmet(id);
    }

    @GetMapping(path = "/nastavnik-predmeti/studenti/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentInNastavnikTrenutniDTO> findAllStudentiByPredmetId(@PathVariable("id") Long id){
        return nastavnikNaRealizacijiService.findAllStudentiByPredmetId(id);
    }

    @PostMapping(path = "/nastavnik-predmeti/studenti/{studentId}/predmet/{predmetId}/ocena/{ocena}")
    @ResponseStatus(HttpStatus.OK)
    public void findAllStudentiByPredmetId(
            @PathVariable("studentId") Long studentId,
            @PathVariable("predmetId") Long predmetId,
            @PathVariable("ocena") Integer ocena
    ){
        nastavnikNaRealizacijiService.dodajOcenu(studentId, predmetId, ocena);
    }
}
