package com.example.predmet.dto.obavestenje;

import com.example.predmet.entity.Obavestenje;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ObavestenjeDTO {
    private Long id;
    private String opis;
    private String title;
    private PredmetInObavestenjeDTO predmet;

    public ObavestenjeDTO(Obavestenje element) {
        id = element.getId();
        opis = element.getOpis();
        title = element.getTitle();
        predmet = new PredmetInObavestenjeDTO(element.getPredmet());
    }
}

