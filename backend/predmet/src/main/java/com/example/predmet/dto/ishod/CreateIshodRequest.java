package com.example.predmet.dto.ishod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateIshodRequest {
    private String opis;
    private Integer termin;
    private Long predmetId;
    private Boolean isPredavanje;
}

