package com.example.predmet.service.impl;

import com.example.predmet.dto.ishod.CreateIshodRequest;
import com.example.predmet.dto.ishod.IshodDTO;
import com.example.predmet.dto.predmet.PredmetDTO;
import com.example.predmet.entity.Ishod;
import com.example.predmet.exception.TerminAlreadyExistException;
import com.example.predmet.repository.impl.IshodRepository;
import com.example.predmet.repository.impl.PredmetRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IshodService extends BaseService<Ishod, IshodDTO, Long> {

    private IshodRepository ishodRepository;
    private final PredmetService predmetService;
    private final PredmetRepository predmetRepository;

    public IshodService(IshodRepository repository, PredmetService predmetService, PredmetRepository predmetRepository) {
        super(repository);
        ishodRepository = repository;
        this.predmetService = predmetService;
        this.predmetRepository = predmetRepository;
    }

    @Override
    public IshodDTO convertToDTO(Ishod element) {
        return new IshodDTO(element);
    }

    public IshodDTO update(Long id, Ishod ishod1) {
        Ishod ishod = ishodRepository.getById(id);
        ishod.setOpis(ishod1.getOpis());


        if(ishod.getPredmetPredavanje() != null){
            ishod.getPredmetPredavanje().getSilabusPredavanja().forEach( silabus -> {
                if (silabus.getTermin() == ishod1.getTermin()){
                    throw new TerminAlreadyExistException();
                } else if (silabus.getTermin() < 0 && silabus.getTermin() > ishod.getPredmetPredavanje().getBrojPredavanja()){
                    throw new TerminAlreadyExistException();
                }
            });

        } else if (ishod.getPredmetVezbe() != null){
            ishod.getPredmetVezbe().getSilabusPredavanja().forEach( silabus -> {
                if (silabus.getTermin() == ishod1.getTermin()){
                    throw new TerminAlreadyExistException();
                } else if (silabus.getTermin() < 0 && silabus.getTermin() > ishod.getPredmetVezbe().getBrojVezbi()){
                    throw new TerminAlreadyExistException();
                }
            });
        }

        ishod.setTermin(ishod1.getTermin());

        return convertToDTO(ishodRepository.save(ishod));
    }

    public List<IshodDTO> getByPredmet(Long id) {
        List<Ishod> ishodi = ishodRepository.findAllByPredmetPredavanje_IdOrPredmetVezbe_Id(id, id);
        return generateList(ishodi);
    }

    public List<IshodDTO> getByPredmetPredavanja(Long id) {
        List<Ishod> ishodi = ishodRepository.findAllByPredmetPredavanje_Id(id);
        return generateList(ishodi);
    }

    public List<IshodDTO> getByPredmetVezbe(Long id) {
        List<Ishod> ishodi = ishodRepository.findAllByPredmetVezbe_Id(id);
        return generateList(ishodi);
    }

    public IshodDTO createIshod(CreateIshodRequest changedT) {
        Ishod ishod = new Ishod();
        ishod.setTermin(changedT.getTermin());
        ishod.setOpis(changedT.getOpis());

        PredmetDTO predmet = predmetService.findOne(changedT.getPredmetId());

        if(changedT.getIsPredavanje()){
            predmet.getSilabusPredavanja().forEach( silabus -> {
                if (silabus.getTermin() == changedT.getTermin()){
                    throw new TerminAlreadyExistException();
                } else if (silabus.getTermin() < 0 && silabus.getTermin() > predmet.getBrojPredavanja()){
                    throw new TerminAlreadyExistException();
                }
            });
            ishod.setPredmetPredavanje(predmetRepository.getById(predmet.getId()));
        } else {
            predmet.getSilabusVezbe().forEach( silabus -> {
                if (silabus.getTermin() == changedT.getTermin()){
                    throw new TerminAlreadyExistException();
                } else if (silabus.getTermin() < 0 && silabus.getTermin() > predmet.getBrojVezbi()){
                    throw new TerminAlreadyExistException();
                }
            });
            ishod.setPredmetVezbe(predmetRepository.getById(predmet.getId()));
        }

        return convertToDTO(ishodRepository.save(ishod));
    }
}
