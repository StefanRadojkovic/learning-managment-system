package com.example.predmet.dto.tip.nastave;

import com.example.predmet.entity.TipNastave;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TipNastaveDTO {
    private Long id;
    private String naziv;

    public TipNastaveDTO(TipNastave tipNastave) {
        id = tipNastave.getId();
        naziv = tipNastave.getNaziv();
    }
}

