package com.example.predmet.repository.impl;


import com.example.predmet.entity.InstrumentEvaluacije;
import com.example.predmet.repository.base.BaseRepository;

import java.util.List;

public interface InstrumentEvaluacijeRepository extends BaseRepository<InstrumentEvaluacije, Long> {
    List<InstrumentEvaluacije> findAllByPredmet_Id(Long predmetId);
}
