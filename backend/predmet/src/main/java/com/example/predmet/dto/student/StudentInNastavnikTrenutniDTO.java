package com.example.predmet.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class StudentInNastavnikTrenutniDTO implements Serializable {
    private Long id;
    private String username;
    private String ime;
    private String jmbg;

    public StudentInNastavnikTrenutniDTO(StudentDTO student) {
        id = student.getId();
        username = student.getUsername();
        ime = student.getIme();
        jmbg = student.getJmbg();
    }
}
