package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.ishod.CreateIshodRequest;
import com.example.predmet.dto.ishod.IshodDTO;
import com.example.predmet.entity.Ishod;
import com.example.predmet.service.impl.IshodService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/ishodi")
public class IshodController extends BaseController<Ishod, IshodDTO, Long> {

    private IshodService ishodService;

    public IshodController(IshodService service) {
        super(service);
        ishodService = service;
    }

    @PutMapping("update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public IshodDTO update(@PathVariable("id") Long id, @RequestBody Ishod changedT){
        return ishodService.update(id, changedT);
    }

    @GetMapping("by-predmet/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public List<IshodDTO> getByPredmet(@PathVariable("predmetId") Long predmetId){
        return ishodService.getByPredmet(predmetId);
    }

    @GetMapping("by-predmet/predavanja/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public List<IshodDTO> getByPredmetPredavanja(@PathVariable("predmetId") Long predmetId){
        return ishodService.getByPredmetPredavanja(predmetId);
    }

    @GetMapping("by-predmet/vezbe/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public List<IshodDTO> getByPredmetVezbe(@PathVariable("predmetId") Long predmetId){
        return ishodService.getByPredmetVezbe(predmetId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public IshodDTO createEvaluacija(@RequestBody CreateIshodRequest changedT){
        return ishodService.createIshod(changedT);
    }
}
