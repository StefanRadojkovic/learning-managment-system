package com.example.predmet.service.impl;

import com.example.predmet.dto.obavestenje.ObavestenjeDTO;
import com.example.predmet.dto.obavestenje.ObavestenjeRequest;
import com.example.predmet.dto.obavestenje.ObavestenjeRequestUpdate;
import com.example.predmet.entity.Obavestenje;
import com.example.predmet.repository.impl.ObavestenjeRepository;
import com.example.predmet.repository.impl.PredmetRepository;
import com.example.predmet.service.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ObavestenjeService extends BaseService<Obavestenje, ObavestenjeDTO, Long> {

    private ObavestenjeRepository obavestenjeRepository;
    private final PredmetRepository predmetRepository;

    public ObavestenjeService(ObavestenjeRepository repository, PredmetRepository predmetRepository) {
        super(repository);
        obavestenjeRepository = repository;
        this.predmetRepository = predmetRepository;
    }

    @Override
    public ObavestenjeDTO convertToDTO(Obavestenje element) {
        return new ObavestenjeDTO(element);
    }

    public ObavestenjeDTO create(ObavestenjeRequest request) {
        Obavestenje obavestenje = new Obavestenje();

        obavestenje.setOpis(request.getOpis());
        obavestenje.setTitle(request.getTitle());
        obavestenje.setPredmet(predmetRepository.getById(request.getPredmetId()));

        return convertToDTO(obavestenjeRepository.save(obavestenje));
    }

    public ObavestenjeDTO update(Long id, ObavestenjeRequestUpdate request) {
        Obavestenje obavestenje = obavestenjeRepository.getById(id);
        obavestenje.setOpis(request.getOpis());
        obavestenje.setTitle(request.getTitle());
        return convertToDTO(obavestenjeRepository.save(obavestenje));
    }
}
