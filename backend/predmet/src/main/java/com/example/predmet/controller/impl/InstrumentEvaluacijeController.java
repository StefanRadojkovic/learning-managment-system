package com.example.predmet.controller.impl;

import com.example.predmet.controller.base.BaseController;
import com.example.predmet.dto.CreateInstrumentEvaluacijeRequest;
import com.example.predmet.dto.instrument.evaluacije.InstrumentEvaluacijeDTO;
import com.example.predmet.dto.instrument.evaluacije.InstrumentEvaluacijeSimpleDTO;
import com.example.predmet.entity.InstrumentEvaluacije;
import com.example.predmet.service.impl.InstrumentEvaluacijeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/predmet/instrumenti-evaluacije")
public class InstrumentEvaluacijeController extends BaseController<InstrumentEvaluacije, InstrumentEvaluacijeDTO, Long> {

    private final InstrumentEvaluacijeService service;

    public InstrumentEvaluacijeController(InstrumentEvaluacijeService service, InstrumentEvaluacijeService service1) {
        super(service);
        this.service = service1;
    }

    @PutMapping("update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public InstrumentEvaluacijeDTO update(@PathVariable("id") Long id, @RequestBody InstrumentEvaluacije changedT){
        return service.update(id, changedT);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public InstrumentEvaluacijeDTO createEvaluacija(@RequestBody CreateInstrumentEvaluacijeRequest changedT){
        return service.createEvaluacija(changedT);
    }

    @GetMapping("predmet/{predmetId}")
    @ResponseStatus(HttpStatus.OK)
    public List<InstrumentEvaluacijeSimpleDTO> getInstrumenteForPredmet(@PathVariable("predmetId") Long predmetId){
        return service.getInstrumenteForPredmet(predmetId);
    }
}
