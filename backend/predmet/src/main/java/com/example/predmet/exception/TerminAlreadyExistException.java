package com.example.predmet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TerminAlreadyExistException extends RuntimeException{
    public TerminAlreadyExistException() {
        super("Izabrani termin je zauzet");
    }
}
