package com.example.predmet.dto.tretnutni.studenti;


import com.example.predmet.entity.Predmet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredmetTrenutniStudenti {
    private List<PohadjanjePredmetaTrenutniStud> pohadjanjePredmeta = new ArrayList<>();

    public PredmetTrenutniStudenti(Predmet predmet) {
        predmet.getPohadjanjePredmeta().forEach(poh -> {
            if(poh.getKonacnaOcena() == null) {
                pohadjanjePredmeta.add(new PohadjanjePredmetaTrenutniStud(poh));
            }
        });
    }
}
