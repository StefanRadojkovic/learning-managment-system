import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UniverzitetService {

  constructor(private client:HttpClient) { }
  dobaviUniverzitete(){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/univerziteti/all`)
  }
  dobaviGodine(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/godine-studija/all/${id}`)
  }
  dobaviGodineIzmena(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/predmeti/all-godine/${id}`)
  }
  dobaviDrzave(){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/drzave/all`)
  }
  dobaviMesta(naziv:any){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/mesta/drzava/${naziv}`)
  }
  dobaviAdrese(naziv:any){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/adrese/mesto/${naziv}`)
  }
  deleteUniverzitet(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/univerziteti/delete/${id}`)
  }

  dodajStudijskiProgram(id:number, studijskiProgram:any){
    return this.client.post<any>(`http://localhost:8002/api/univerzitet/studijski-programi/fakultet/${id}`, studijskiProgram)
  }
  
  izmeniStudijskiProgram(id:number, studijskiProgram:any){
    return this.client.put<any>(`http://localhost:8002/api/univerzitet/studijski-programi/update/${id}`, studijskiProgram)
  }

  dobaviPretrazeneUniverzitete(forma:any){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/univerziteti/find/${forma}`)
  }

  dobaviPretrazeneFakultete(forma:any, id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/fakulteti/univerzitet/${id}/find/${forma}`)
  }
  deleteFakultet(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/fakulteti/delete/${id} `)
  }

  dobaviPretrazeneStudijskePrograme(forma:any, id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/studijski-programi/fakultet/${id}/find/${forma}`)
  }
  
  dodajUniverzitet(forma:any, adresa:any){
    return this.client.post<any>(`http://localhost:8002/api/univerzitet/univerziteti/${forma}`, adresa)
  }
  dobaviFakultete(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/fakulteti/univerzitet/${id}`)
  }
  dobaviStudijskePrograme(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/studijski-programi/fakultet/${id}`)
  }
  deleteStudijskiProgram(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/studijski-programi/delete/${id}`)
  }
  dobaviJedanStudijskiProgram(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/studijski-programi/${id}`)
  }
  dobaviPredmete(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/studijski-programi/predmeti/${id}`)
  }

  dobaviPredmeteAdmin(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/godine-studija/studijski-program/${id}`)
  }
  izmeniPredmetAdmin(id:number, predmet:any){
    return this.client.put<any[]>(`http://localhost:8002/api/predmet/predmeti/update/${id}`, predmet)
  }
  deletePredmet(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/univerziteti/all`)
  }

  deletePredmetAdmin(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/predmet/predmeti/delete/${id}`)
  }
  dodajPredmetAdmin( predmet:any, id:number){
    return this.client.post<any[]>(`http://localhost:8002/api/predmet/predmeti/godina/stud/${id}`,predmet)
  }

  izmeniUniverzitet(id:number, forma:any, adresa:any){
    return this.client.put<any[]>(`http://localhost:8002/api/univerzitet/univerziteti/update/${id}/${forma}`, adresa)
    
  }

  izmeniFakultet(id:number, forma:any,adresa:any){
    return this.client.put<any[]>(`http://localhost:8002/api/univerzitet/fakulteti/update/${id}/${forma}`, adresa)
    
  }
  dodajFakultet(id:number,forma:any, adresa:any){
    return this.client.post<any[]>(`http://localhost:8002/api/univerzitet/fakulteti/create/${id}/${forma}`, adresa)
  }
}
