import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Predmet } from 'src/app/model/predmet';

@Injectable({
  providedIn: 'root'
})
export class PredmetiService {
  upisiOcenu(idStudenta: number, ocena: any, idPredmeta:number) {
    return this.client.post<any[]>(`http://localhost:8002/api/predmet/nastavnik-na-realizaciji/nastavnik-predmeti/studenti/${idStudenta}/predmet/${idPredmeta}/ocena/${ocena}`, ocena)
  }

  constructor(private client:HttpClient) { }

  dobaviSvePredmete(){
    return this.client.get<Predmet[]>("http://localhost:3000/predmeti")
  }
  dobaviPretrazeneStudente(forma:any){
    return this.client.post<any[]>("http://localhost:8002/api/korisnik/studenti/find", forma)
  }
  dobaviSveTrenutnoSlusani(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/pohadjanje-predmeta/student/trenutno-slusani-predmet/${id}`)
  }

  dobaviObavestenjaPredmeta(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/predmeti/nastavnik-predmeti/obavestenja/${id}`)
  }
  dobaviObavestenja(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/pohadjanje-predmeta/student/trenutno-slusani-predmet/obavestenja/${id}`)
  }
  deleteObavestenja(id:number){
    return this.client.delete<Predmet[]>(`http://localhost:8002/api/predmet/obavestenja/${id}`)
  }

  dobaviStudente(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/nastavnik-na-realizaciji/nastavnik-predmeti/studenti/${id}`)
  }

  dobaviJednogStudenta(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/nastavnik-na-realizaciji/nastavnik-predmeti/studenti/${id}`)
  }

  dodajObavestenja(obavestenje:any){
    return this.client.post<any[]>(`http://localhost:8002/api/predmet/obavestenja`, obavestenje)
  }
  updateObavestenja(id:number, obavestenje:any){
    return this.client.put<any[]>(`http://localhost:8002/api/predmet/obavestenja/${id}`,obavestenje)
  }
  dobaviInstrumentEvaluacije(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/instrumenti-evaluacije/predmet/${id}`)
  }

  dobaviTrenutnePredmeteNastavnik(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/nastavnik-na-realizaciji/nastavnik-predmeti/${id}`)
  }

  updatePredmet(id:number,predmet:Predmet){
    return this.client.put<Predmet[]>(`http://localhost:3000/predmeti/${id}`,predmet)
  }
  dobaviPolozenePredmete(id:number){
    return this.client.get<Predmet[]>(`http://localhost:8002/api/predmet/pohadjanje-predmeta/student/polozeni-predmeti/${id}`)
  }
  dobaviIshodePredavanja(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/ishodi/by-predmet/predavanja/${id}`)
  }
  dobaviIshodeVezbe(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/predmet/ishodi/by-predmet/vezbe/${id}`)
  }
  updateIshod(id:number, ishod:any){
    return this.client.put<any[]>(`http://localhost:8002/api/predmet/ishodi/update/${id}`,ishod)
  }
  deleteIshod(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/predmet/ishodi/${id}`)
  }

  dodajIshod(ishod:any){
    return this.client.post<any[]>(`http://localhost:8002/api/predmet/ishodi`, ishod)
  }

  dodajInstrumentEvaluacije(instrument:any){
    return this.client.post<any[]>(`http://localhost:8002/api/predmet/instrumenti-evaluacije`, instrument)
  }

  deleteInstrumentEvaluacije(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/predmet/instrumenti-evaluacije/${id}`)
  }
  updateInstrumentEvaluacije(id:number, instrument:any){
    return this.client.put<any[]>(`http://localhost:8002/api/predmet/instrumenti-evaluacije/update/${id}`,instrument)
  }
}
