import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/model/user';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private client:HttpClient,private loginService:LoginService) { }

  dodajKorisnika(user:User){
    return this.client.post("http://localhost:8002/api/korisnik/users/", user)
  }
  updateKorisnika(id:number, user:User){
    return this.client.put(`http://localhost:8002/api/korisnik/users/${id}`, user)
  }
  dobaviStudente(user:User){
    if(user.roles.includes("ROLE_STUDENT")){
      return this.client.get<User[]>("http://localhost:3000/users")
    }else{
      return false
    }
  }
}
