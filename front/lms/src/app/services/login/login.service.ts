import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { Token } from 'src/app/model/token';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token: null | string = null;
  user: any = null;

  constructor(private httpClient:HttpClient) { }

  login(user: User) {

    return this.httpClient.post<Token>("http://localhost:8002/api/korisnik/login", user)
    .pipe(
      tap(token => {
        this.token = token.token;
        this.user = JSON.parse(atob(token.token.split(".")[1]));
      })
    )
  }

  logout(){
    this.user = null;
    this.token = null;
  }


  validateRoles(roles:any, method="any") {
    if(this.user){
      let userRoles = new Set(this.user.roles);
      roles = new Set(roles)
      let intersection = new Set();

      for(let r of roles){
        if(userRoles.has(r)){
          intersection.add(r);
        }
      }
      if(method == "any"){
        return intersection.size > 0;
      }else if (method == "all"){
        return intersection.size == roles.size
      }


    }
    return false;
  }
}
