import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SifarnikService {

  constructor(private client:HttpClient) { }

  dobaviDrzave(){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/drzave`)
  }

  dodajDrzavu(forma:any){
    return this.client.post<any[]>(`http://localhost:8002/api/univerzitet/drzave/${forma}`, forma)
  }
  izmeniDrzavu(forma:any, id:number){
    return this.client.put<any[]>(`http://localhost:8002/api/univerzitet/drzave/${id}/${forma}`,forma)
  }
  deleteDrzavu(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/drzave/delete/${id}`)
  }

  dobaviMesta(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/mesta/drzavaId/${id}`)
  }

  dodajMesta(forma:any, id:number){
    return this.client.post<any[]>(`http://localhost:8002/api/univerzitet/mesta/drzava/${id}/${forma}`, forma)
  }
  izmeniMesta(forma:any, id:number){
    return this.client.put<any[]>(`http://localhost:8002/api/univerzitet/mesta/${id}/${forma}`,forma)
  }
  deleteMesto(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/mesta/delete/${id}`)
  }
  dobaviAdrese(id:number){
    return this.client.get<any[]>(`http://localhost:8002/api/univerzitet/adrese/mestoId/${id}`)
  }

  dodajAdresu(ulica:any,broj:number, id:number){
    return this.client.post<any[]>(`http://localhost:8002/api/univerzitet/adrese/mesto/${id}/${ulica}/${broj}`, ulica)
  }
  izmeniAdresu(ulica:any,broj:number, id:number){
    return this.client.put<any[]>(`http://localhost:8002/api/univerzitet/adrese/${id}/${ulica}/${broj} `,ulica)
  }
  deleteAdreseu(id:number){
    return this.client.delete<any[]>(`http://localhost:8002/api/univerzitet/adrese/delete/${id}`)
  }
}
