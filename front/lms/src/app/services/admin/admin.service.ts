import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private client:HttpClient) { }
  dobaviSveKorisnike(){
    return this.client.get<User[]>("http://localhost:8002/api/korisnik/users/get-all")
  }
  dobaviPretrazeneUsere(forma:any){
    return this.client.get<User[]>(`http://localhost:8002/api/korisnik/users/find/${forma}`)
  }
  delete(id:number){
    return this.client.delete<User[]>(`http://localhost:3000/users/${id}`)
  }

  ukloniRoleKorisniku(id:number,forma:any){
    console.log(forma);
    
    return this.client.delete<User[]>(`http://localhost:8002/api/korisnik/users/remove-role/${id}/${forma}`)
  }

  dodajRoleKorisniku(id:number, forma:any){
    return this.client.put<User[]>(`http://localhost:8002/api/korisnik/users/add-role/${id}`, forma)
  }
}
