export interface Predmet {
    ime: string;
    skracenica: string;
    espb: number;
    opis: string;
    evaluacije:string[];
}
