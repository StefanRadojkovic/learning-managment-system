import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminManagerComponent } from './components/admin-manager/admin-manager.component';
import { AdministracijaAdreseComponent } from './components/admin-manager/administracija-adrese/administracija-adrese.component';
import { IzmeniAdresuComponent } from './components/admin-manager/administracija-adrese/izmeni-adresu/izmeni-adresu.component';
import { IzmeniDrzavuComponent } from './components/admin-manager/administracija-drzave/izmeni-drzavu/izmeni-drzavu.component';
import { AdministracijaMestaComponent } from './components/admin-manager/administracija-mesta/administracija-mesta.component';
import { IzmeniMestoComponent } from './components/admin-manager/administracija-mesta/izmeni-mesto/izmeni-mesto.component';
import { AdministracijaFakultetaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-fakulteta.component';
import { AdministracijaPredmetaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-predmeta/administracija-predmeta.component';
import { IzmeniPredmetComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-predmeta/izmeni-predmet/izmeni-predmet.component';
import { AdministracijaStudijskihProgramaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-studijskih-programa.component';
import { IzmeniStudijskiProgramComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/izmeni-studijski-program/izmeni-studijski-program.component';
import { IzmeniFakultetComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/izmeni-fakultet/izmeni-fakultet.component';
import { IzmeniUniverzitetComponent } from './components/admin-manager/administracija-organizacije/izmeni-univerzitet/izmeni-univerzitet.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { InstrumentEvaluacijeComponent } from './components/nastavnik-manager/instrument-evaluacije/instrument-evaluacije.component';
import { IzmenaInstrumentaEvaluacijeComponent } from './components/nastavnik-manager/instrument-evaluacije/izmena-instrumenta-evaluacije/izmena-instrumenta-evaluacije.component';
import { NastavnikManagerComponent } from './components/nastavnik-manager/nastavnik-manager.component';
import { IzmeniObavestenjaComponent } from './components/nastavnik-manager/nastavnik-obavestenja/izmeni-obavestenja/izmeni-obavestenja.component';
import { NastavnikObavestenjaComponent } from './components/nastavnik-manager/nastavnik-obavestenja/nastavnik-obavestenja.component';
import { StudentDetaljnijeComponent } from './components/nastavnik-manager/studenti-na-predmetu-tabela/student-detaljnije/student-detaljnije.component';
import { StudentiNaPredmetuTabelaComponent } from './components/nastavnik-manager/studenti-na-predmetu-tabela/studenti-na-predmetu-tabela.component';
import { IzmeniSilabusVezbiComponent } from './components/nastavnik-manager/uredi-silabus-vezbi/izmeni-silabus-vezbi/izmeni-silabus-vezbi.component';
import { UrediSilabusVezbiComponent } from './components/nastavnik-manager/uredi-silabus-vezbi/uredi-silabus-vezbi.component';
import { IzmeniIshodComponent } from './components/nastavnik-manager/uredi-silabus/izmeni-ishod/izmeni-ishod.component';
import { UrediSilabusComponent } from './components/nastavnik-manager/uredi-silabus/uredi-silabus.component';
import { RegisterComponent } from './components/register/register.component';
import { ObavestenjaComponent } from './components/studentManager/obavestenja/obavestenja.component';
import { TrenutniPredmetiComponent } from './components/studentManager/trenutni-predmeti.component';
import { FakultetiComponent } from './components/univerziteti/fakulteti/fakulteti.component';
import { JedanStudijskiProgramComponent } from './components/univerziteti/fakulteti/studijski-programi/jedan-studijski-program/jedan-studijski-program.component';
import { StudijskiProgramiComponent } from './components/univerziteti/fakulteti/studijski-programi/studijski-programi.component';
import { UniverzitetiComponent } from './components/univerziteti/univerziteti.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {path:"home", component:HomeComponent},
  {path:"fakulteti/:id", component:FakultetiComponent},
  {path:"studijskiProgrami/:id", component:StudijskiProgramiComponent},
  {path:"studijskiProgram/:id", component:JedanStudijskiProgramComponent},
  {path:"", redirectTo:"home", pathMatch:"full"},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  
  {path:"uredi", component:EditProfileComponent,data:{
    allowedRoles: ["ROLE_STUDENT","ROLE_ADMIN","ROLE_REGISTROVAN_KORISNIK","ROLE_NASTAVNIK"],
    },canActivate: [AuthGuard]},
  {path:"studentManager", component:TrenutniPredmetiComponent, data:{
    allowedRoles: ["ROLE_STUDENT"]
  },canActivate: [AuthGuard]},
  {path:"adminManager", component:AdminManagerComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"nastavnikManager", component:NastavnikManagerComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"urediSilabusPredavanja/:id", component:UrediSilabusComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"urediSilabusVezbi/:id", component:UrediSilabusVezbiComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"urediIshod/:id", component:IzmeniIshodComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},

  {path:"urediInsturmentEvaluacije/:id", component:IzmenaInstrumentaEvaluacijeComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},

  {path:"urediObavestenja/:id", component:NastavnikObavestenjaComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"izmeniObavestenje/:id", component:IzmeniObavestenjaComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},

  {path:"urediIshodVezbi/:id", component:IzmeniSilabusVezbiComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"studentiTabela/:id", component:StudentiNaPredmetuTabelaComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"studentiTabela/:id/student/:id", component:StudentDetaljnijeComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},
  {path:"instrumentEvaluacije/:id", component:InstrumentEvaluacijeComponent, data:{
    allowedRoles: ["ROLE_NASTAVNIK"]
  },canActivate: [AuthGuard]},

  {path:"izmeniDrzavu/:id", component:IzmeniDrzavuComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"mestaTabela/:id", component:AdministracijaMestaComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"izmeniMesto/:id", component:IzmeniMestoComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"adresaTabela/:id", component:AdministracijaAdreseComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"izmeniAdresu/:id", component:IzmeniAdresuComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},

  {path:"izmeniUniverzitet/:id", component:IzmeniUniverzitetComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"fakultetiTabela/:id", component:AdministracijaFakultetaComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"fakultet/:id", component:IzmeniFakultetComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"adminstudijskiProgrami/:id", component:AdministracijaStudijskihProgramaComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"izmenaStudijskogPrograma/:id", component:IzmeniStudijskiProgramComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"administracijaPredmeta/:id", component:AdministracijaPredmetaComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  {path:"izmenaPredmeta/:id", component:IzmeniPredmetComponent, data:{
    allowedRoles: ["ROLE_ADMIN"]
  },canActivate: [AuthGuard]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
