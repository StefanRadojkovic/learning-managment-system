import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input'
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';






import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { TrenutniPredmetiComponent } from './components/studentManager/trenutni-predmeti.component';
import { DialogComponent } from './components/edit-profile/dialog/dialog.component';
import { NastavnikManagerComponent } from './components/nastavnik-manager/nastavnik-manager.component';
import { UrediSilabusComponent } from './components/nastavnik-manager/uredi-silabus/uredi-silabus.component';
import { StudentiNaPredmetuTabelaComponent } from './components/nastavnik-manager/studenti-na-predmetu-tabela/studenti-na-predmetu-tabela.component';
import { AdminManagerComponent } from './components/admin-manager/admin-manager.component';
import { IstorijaStudiranjaComponent } from './components/studentManager/istorija-studiranja/istorija-studiranja.component';
import { IzmeniIshodComponent } from './components/nastavnik-manager/uredi-silabus/izmeni-ishod/izmeni-ishod.component';
import { InstrumentEvaluacijeComponent } from './components/nastavnik-manager/instrument-evaluacije/instrument-evaluacije.component';
import { ObavestenjaComponent } from './components/studentManager/obavestenja/obavestenja.component';
import { UrediSilabusVezbiComponent } from './components/nastavnik-manager/uredi-silabus-vezbi/uredi-silabus-vezbi.component';
import { IzmeniSilabusVezbiComponent } from './components/nastavnik-manager/uredi-silabus-vezbi/izmeni-silabus-vezbi/izmeni-silabus-vezbi.component';
import { IzmenaInstrumentaEvaluacijeComponent } from './components/nastavnik-manager/instrument-evaluacije/izmena-instrumenta-evaluacije/izmena-instrumenta-evaluacije.component';
import { NastavnikObavestenjaComponent } from './components/nastavnik-manager/nastavnik-obavestenja/nastavnik-obavestenja.component';
import { IzmeniObavestenjaComponent } from './components/nastavnik-manager/nastavnik-obavestenja/izmeni-obavestenja/izmeni-obavestenja.component';
import { StudentDetaljnijeComponent } from './components/nastavnik-manager/studenti-na-predmetu-tabela/student-detaljnije/student-detaljnije.component';
import { UniverzitetiComponent } from './components/univerziteti/univerziteti.component';
import { FakultetiComponent } from './components/univerziteti/fakulteti/fakulteti.component';
import { StudijskiProgramiComponent } from './components/univerziteti/fakulteti/studijski-programi/studijski-programi.component';
import { JedanStudijskiProgramComponent } from './components/univerziteti/fakulteti/studijski-programi/jedan-studijski-program/jedan-studijski-program.component';
import { AdministracijaOrganizacijeComponent } from './components/admin-manager/administracija-organizacije/administracija-organizacije.component';
import { IzmeniUniverzitetComponent } from './components/admin-manager/administracija-organizacije/izmeni-univerzitet/izmeni-univerzitet.component';
import { AdministracijaFakultetaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-fakulteta.component';
import { IzmeniFakultetComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/izmeni-fakultet/izmeni-fakultet.component';
import { PretraziStudenteComponent } from './components/nastavnik-manager/pretrazi-studente/pretrazi-studente.component';
import { AdministracijaDrzaveComponent } from './components/admin-manager/administracija-drzave/administracija-drzave.component';
import { AdministracijaMestaComponent } from './components/admin-manager/administracija-mesta/administracija-mesta.component';
import { AdministracijaAdreseComponent } from './components/admin-manager/administracija-adrese/administracija-adrese.component';
import { IzmeniDrzavuComponent } from './components/admin-manager/administracija-drzave/izmeni-drzavu/izmeni-drzavu.component';
import { IzmeniMestoComponent } from './components/admin-manager/administracija-mesta/izmeni-mesto/izmeni-mesto.component';
import { IzmeniAdresuComponent } from './components/admin-manager/administracija-adrese/izmeni-adresu/izmeni-adresu.component';
import { AdministracijaStudijskihProgramaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-studijskih-programa.component';
import { IzmeniStudijskiProgramComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/izmeni-studijski-program/izmeni-studijski-program.component';
import { AdministracijaPredmetaComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-predmeta/administracija-predmeta.component';
import { IzmeniPredmetComponent } from './components/admin-manager/administracija-organizacije/administracija-fakulteta/administracija-studijskih-programa/administracija-predmeta/izmeni-predmet/izmeni-predmet.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    EditProfileComponent,
    TrenutniPredmetiComponent,
    DialogComponent,
    NastavnikManagerComponent,
    UrediSilabusComponent,
    StudentiNaPredmetuTabelaComponent,
    AdminManagerComponent,
    IstorijaStudiranjaComponent,
    IzmeniIshodComponent,
    InstrumentEvaluacijeComponent,
    ObavestenjaComponent,
    UrediSilabusVezbiComponent,
    IzmeniSilabusVezbiComponent,
    IzmenaInstrumentaEvaluacijeComponent,
    NastavnikObavestenjaComponent,
    IzmeniObavestenjaComponent,
    StudentDetaljnijeComponent,
    UniverzitetiComponent,
    FakultetiComponent,
    StudijskiProgramiComponent,
    JedanStudijskiProgramComponent,
    AdministracijaOrganizacijeComponent,
    IzmeniUniverzitetComponent,
    AdministracijaFakultetaComponent,
    IzmeniFakultetComponent,
    PretraziStudenteComponent,
    AdministracijaDrzaveComponent,
    AdministracijaMestaComponent,
    AdministracijaAdreseComponent,
    IzmeniDrzavuComponent,
    IzmeniMestoComponent,
    IzmeniAdresuComponent,
    AdministracijaStudijskihProgramaComponent,
    IzmeniStudijskiProgramComponent,
    AdministracijaPredmetaComponent,
    IzmeniPredmetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatStepperModule,
    MatIconModule,
    MatTableModule,
    MatTabsModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatDividerModule,
    MatPaginatorModule

    
  ],
  providers: [{provide:HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
