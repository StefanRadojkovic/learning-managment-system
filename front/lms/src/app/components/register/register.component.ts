import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input()
  user:any= {
    ime: "",
    jmbg: "",
    username: "",
    password: "",
    roles: []
  }

  forma1 = new FormGroup({
    ime: new FormControl(null, Validators.required),
    jmbg: new FormControl(null, Validators.required),
  });

  forma2 = new FormGroup({
    username: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  });

  

  constructor(private router:Router,private registerService:RegisterService) { }

  ngOnInit(): void {
  }

  submit(){
    console.log(this.user)
    this.registerService.dodajKorisnika(this.user).subscribe(x=>{
      this.router.navigate(["login"])
    })//zasto ovde odma ne mogu da prosledim i postujem korisnika nego moram da koristim event emmitter
  }

  cancel(){
    this.router.navigate(["login"])
  }

}
