import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin/admin.service';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-univerziteti',
  templateUrl: './univerziteti.component.html',
  styleUrls: ['./univerziteti.component.css']
})
export class UniverzitetiComponent implements OnInit {

  elementi: any[] = []

  forma = new FormGroup({
    ime: new FormControl(null),
  });
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private univerzitetServis:UniverzitetService,private router:Router) { }

  ngOnInit(): void {
    this.get()
  }

  detaljnije(id:number){
    this.router.navigate([`/fakulteti/${id}`])
  }

  izmeni(id:number){
    this.router.navigate([`/univerzitet/${id}`])
  }

  displayedColumns: string[] = ['ime', 'datumOsnivanja', 'akcije'];

  get() {
    this.univerzitetServis.dobaviUniverzitete().subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
    }
    )
    
  }
  submit(){
    if(this.forma.value.ime ==""){
      this.get()
    }else{
      this.univerzitetServis.dobaviPretrazeneUniverzitete(this.forma.value.ime).subscribe((univerziteti:any[])=>{
        this.elementi = univerziteti
        this.dataSource = new MatTableDataSource(this.elementi)
      
        console.log(this.elementi);
        
      
    }
    )
  }
  }
}
