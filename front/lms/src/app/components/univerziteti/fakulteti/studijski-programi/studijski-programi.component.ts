import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-studijski-programi',
  templateUrl: './studijski-programi.component.html',
  styleUrls: ['./studijski-programi.component.css']
})
export class StudijskiProgramiComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  delimiter = this.router.url.indexOf("/fakulteti/");
  ruta = this.router.url.slice(0,this.delimiter)
  forma = new FormGroup({
    ime: new FormControl(null),
  });
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }

  detaljnije(id:number){
    this.router.navigate([`/studijskiProgram/${id}`])
  }

  displayedColumns: string[] = ['ime', 'akcije'];

  get() {
    this.univerzitetServis.dobaviStudijskePrograme(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
    }
    )
  }

  back(){
    this.router.navigate(["/home"])
  }

  submit(){
    if(this.forma.value.ime ==""){
      this.get()
    }else{
      this.univerzitetServis.dobaviPretrazeneStudijskePrograme(this.forma.value.ime, this.currentId).subscribe((fakulteti:any[])=>{
      
        this.elementi = fakulteti
        this.dataSource = new MatTableDataSource(this.elementi)
      
        console.log(this.elementi);
        
      }
      )
    }
    
  }

}
