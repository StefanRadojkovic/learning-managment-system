import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-jedan-studijski-program',
  templateUrl: './jedan-studijski-program.component.html',
  styleUrls: ['./jedan-studijski-program.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class JedanStudijskiProgramComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  delimiter = this.router.url.indexOf("/fakulteti/");
  ruta = this.router.url.slice(0,this.delimiter)
  opis = ""

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }

  displayedColumns: string[] = ['ime', 'espb'];

  get() {
    this.univerzitetServis.dobaviPredmete(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      this.elementi.forEach(elem =>
         this.opis = elem.opis
      )
      console.log(this.elementi);
      
    }
    )
  }

  back(){
    this.router.navigate(["/home"])
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue.trim().toLowerCase()+"filter")
    console.log(this.dataSource.filter +"datasource");
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
