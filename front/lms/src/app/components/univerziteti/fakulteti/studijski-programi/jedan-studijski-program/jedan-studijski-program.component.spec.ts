import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JedanStudijskiProgramComponent } from './jedan-studijski-program.component';

describe('JedanStudijskiProgramComponent', () => {
  let component: JedanStudijskiProgramComponent;
  let fixture: ComponentFixture<JedanStudijskiProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JedanStudijskiProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JedanStudijskiProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
