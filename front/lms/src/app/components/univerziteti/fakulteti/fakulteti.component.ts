import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-fakulteti',
  templateUrl: './fakulteti.component.html',
  styleUrls: ['./fakulteti.component.css']
})
export class FakultetiComponent implements OnInit {

  elementi: any[] = []


  forma = new FormGroup({
    ime: new FormControl(null),
  });
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  private routeSub: Subscription;
  currentId:number

  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }

  detaljnije(id:number){
    this.router.navigate([`/studijskiProgrami/${id}`])
  }

  displayedColumns: string[] = ['ime', 'akcije'];

  get() {
    this.univerzitetServis.dobaviFakultete(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
    }
    )
    
  }

  back(){
    this.router.navigate(["/home"])
  }


  submit(){
    if(this.forma.value.ime ==""){
      this.get()
    }else{
      this.univerzitetServis.dobaviPretrazeneFakultete(this.forma.value.ime, this.currentId).subscribe((fakulteti:any[])=>{
      
        this.elementi = fakulteti
        this.dataSource = new MatTableDataSource(this.elementi)
      
        console.log(this.elementi);
        
      }
      )
    }
    
  }
}
