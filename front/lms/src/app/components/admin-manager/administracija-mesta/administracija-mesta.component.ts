import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SifarnikService } from 'src/app/services/sifarnik/sifarnik.service';

@Component({
  selector: 'app-administracija-mesta',
  templateUrl: './administracija-mesta.component.html',
  styleUrls: ['./administracija-mesta.component.css']
})
export class AdministracijaMestaComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  postojiMesto = false
  neMoguceObrisati = false

  forma = new FormGroup({
    naziv: new FormControl(null),
    
  });

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private sifarnikService:SifarnikService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      
    });
    this.get()

  }

  submit(){
    this.sifarnikService.dodajMesta(this.forma.value.naziv, this.currentId).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiMesto = true
      }
      
    });
    
  }
  delete(id:number) {
    this.sifarnikService.deleteMesto(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  detaljnije(id:number){
    this.router.navigate([`/adresaTabela/${id}`])
  }

  displayedColumns: string[] = ['ime', 'akcije'];

  get() {
    this.sifarnikService.dobaviMesta(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      console.log(this.elementi);
      
    }
    )
    
  }

  izmeni(id:number){
    this.router.navigate([`/izmeniMesto/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
