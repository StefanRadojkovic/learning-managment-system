import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaMestaComponent } from './administracija-mesta.component';

describe('AdministracijaMestaComponent', () => {
  let component: AdministracijaMestaComponent;
  let fixture: ComponentFixture<AdministracijaMestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaMestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaMestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
