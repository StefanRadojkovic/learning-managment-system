import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniMestoComponent } from './izmeni-mesto.component';

describe('IzmeniMestoComponent', () => {
  let component: IzmeniMestoComponent;
  let fixture: ComponentFixture<IzmeniMestoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniMestoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniMestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
