import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaDrzaveComponent } from './administracija-drzave.component';

describe('AdministracijaDrzaveComponent', () => {
  let component: AdministracijaDrzaveComponent;
  let fixture: ComponentFixture<AdministracijaDrzaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaDrzaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaDrzaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
