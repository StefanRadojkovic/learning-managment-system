import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SifarnikService } from 'src/app/services/sifarnik/sifarnik.service';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-administracija-drzave',
  templateUrl: './administracija-drzave.component.html',
  styleUrls: ['./administracija-drzave.component.css']
})
export class AdministracijaDrzaveComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  postojiDrzava = false
  neMoguceObrisati = false;

  forma = new FormGroup({
    naziv: new FormControl(null),
    
  });

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private sifarnikService:SifarnikService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      
    });
    this.get()

  }

  submit(){
    this.sifarnikService.dodajDrzavu(this.forma.value.naziv).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiDrzava = true
      }
      
    });
    
  }

  detaljnije(id:number){
    this.router.navigate([`/mestaTabela/${id}`])
  }

  displayedColumns: string[] = ['ime', 'akcije'];

  get() {
    this.sifarnikService.dobaviDrzave().subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      console.log(this.elementi);
      
    }
    )
    
  }
  delete(id:number) {
    this.sifarnikService.deleteDrzavu(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  izmeni(id:number){
    this.router.navigate([`/izmeniDrzavu/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
