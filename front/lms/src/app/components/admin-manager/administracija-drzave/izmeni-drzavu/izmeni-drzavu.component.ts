import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SifarnikService } from 'src/app/services/sifarnik/sifarnik.service';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-izmeni-drzavu',
  templateUrl: './izmeni-drzavu.component.html',
  styleUrls: ['./izmeni-drzavu.component.css']
})
export class IzmeniDrzavuComponent implements OnInit {

  univerzitet:any= {
    ime: "",
    datumOsnivanja: "",
    
  }
  postojiDrzava = false

  forma = new FormGroup({
    naziv: new FormControl(null),
    
  });

  private routeSub: Subscription;
  currentId:number

  constructor(private router:Router,private sifarnikServis:SifarnikService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
  }

  submit(){
    this.sifarnikServis.izmeniDrzavu(this.forma.value.naziv,this.currentId).subscribe((r => {
      this.router.navigate(['/adminManager'])
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiDrzava = true
      }
      
    });
  }

  cancel(){
    this.router.navigate([`/adminManager`])
  }

}
