import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-admin-manager',
  templateUrl: './admin-manager.component.html',
  styleUrls: ['./admin-manager.component.css']
})
export class AdminManagerComponent implements OnInit {

  @Input()
  elementi: any[] = []

  studenti: any[] = []
  roles: string[] = ['ROLE_ADMIN', 'ROLE_REGISTROVAN_KORISNIK', 'ROLE_STUDENT', 'ROLE_NASTAVNIK'];

  forma = new FormGroup({
    naziv: new FormControl(null),
  });

  forma2 = new FormGroup({
    naziv: new FormControl(null),
  });
  formaPretraga = new FormGroup({
    ime: new FormControl(null),
  });

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  
  constructor(private adminServis:AdminService, private router : Router) { }

  ngOnInit(): void {
    this.get()
  }

  displayedColumns: string[] = ['ime', 'username', 'jmbg','roles','actions','actions2'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  home(){
    this.router.navigate(["/home"])
  }

  submit(id:number){
    this.adminServis.dodajRoleKorisniku(id["id"],this.forma.value).subscribe(r => {
      this.get()
    })
  }

  get() {
    this.adminServis.dobaviSveKorisnike().subscribe((users:User[])=>{
      this.elementi = users
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
    })
  }

  deleteRole(id:number){
    console.log(id["id"]);
    console.log(this.forma2.value);
    
    this.adminServis.ukloniRoleKorisniku(id["id"],this.forma2.value.naziv).subscribe(x=>{
      this.get()
    })
  }

  submitPretrage(){
    console.log(this.formaPretraga.value.ime);
    
    if(this.formaPretraga.value.ime ==""){
      this.get()
    }else{
      this.adminServis.dobaviPretrazeneUsere(this.formaPretraga.value.ime).subscribe((users:any[])=>{
      
        this.elementi = users
        this.dataSource = new MatTableDataSource(this.elementi)
      
        console.log(this.elementi);
        
      }
      )
    }
    
  }


}
