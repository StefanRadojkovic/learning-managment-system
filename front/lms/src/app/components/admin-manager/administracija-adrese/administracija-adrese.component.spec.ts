import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaAdreseComponent } from './administracija-adrese.component';

describe('AdministracijaAdreseComponent', () => {
  let component: AdministracijaAdreseComponent;
  let fixture: ComponentFixture<AdministracijaAdreseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaAdreseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaAdreseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
