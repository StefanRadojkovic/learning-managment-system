import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SifarnikService } from 'src/app/services/sifarnik/sifarnik.service';

@Component({
  selector: 'app-administracija-adrese',
  templateUrl: './administracija-adrese.component.html',
  styleUrls: ['./administracija-adrese.component.css']
})
export class AdministracijaAdreseComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  postojiAdresa = false;
  neMoguceObrisati = false

  forma = new FormGroup({
    ulica: new FormControl(null),
    broj: new FormControl(null)
    
  });

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private sifarnikService:SifarnikService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      
    });
    this.get()

  }

  submit(){
    this.sifarnikService.dodajAdresu(this.forma.value.ulica,this.forma.value.broj, this.currentId).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiAdresa = true
      }
      
    });
  }

  delete(id:number) {
    this.sifarnikService.deleteAdreseu(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }


  displayedColumns: string[] = ['ulica','broj', 'akcije'];

  get() {
    this.sifarnikService.dobaviAdrese(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      console.log(this.elementi);
      
    }
    )
    
  }

  izmeni(id:number){
    this.router.navigate([`/izmeniAdresu/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
