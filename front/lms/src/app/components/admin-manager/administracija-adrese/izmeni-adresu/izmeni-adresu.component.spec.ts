import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniAdresuComponent } from './izmeni-adresu.component';

describe('IzmeniAdresuComponent', () => {
  let component: IzmeniAdresuComponent;
  let fixture: ComponentFixture<IzmeniAdresuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniAdresuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniAdresuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
