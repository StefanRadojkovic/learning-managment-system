import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SifarnikService } from 'src/app/services/sifarnik/sifarnik.service';

@Component({
  selector: 'app-izmeni-adresu',
  templateUrl: './izmeni-adresu.component.html',
  styleUrls: ['./izmeni-adresu.component.css']
})
export class IzmeniAdresuComponent implements OnInit {

  univerzitet:any= {
    ulica: "",
    broj: null,
    
  }


  postojiAdresa = false
  forma = new FormGroup({
    ulica: new FormControl(null),
    broj: new FormControl(null),
    
  });

  private routeSub: Subscription;
  currentId:number

  constructor(private router:Router,private sifarnikServis:SifarnikService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
  }

  submit(){
    this.sifarnikServis.izmeniAdresu(this.forma.value.ulica,this.forma.value.broj,this.currentId).subscribe((r => {
      this.router.navigate(['/adminManager'])
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiAdresa = true
      }
      
    });
  }

  cancel(){
    this.router.navigate([`/adminManager`])
  }

}
