import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-izmeni-univerzitet',
  templateUrl: './izmeni-univerzitet.component.html',
  styleUrls: ['./izmeni-univerzitet.component.css']
})
export class IzmeniUniverzitetComponent implements OnInit {


  univerzitet:any= {
    ime: "",
    datumOsnivanja: "",
    
  }
  drzave: any[] = []
  mesta: any[] = []
  adrese: any[] = []
  potvrdjenaDrzava = false;
  potvrdjenoMesto = false;
  potvrdjenaAdresa = false;

  drzava = new FormControl(null);
  mesto = new FormControl(null);
  adresa = new FormControl(null);

  forma = new FormGroup({
    naziv: new FormControl(null),
    
  });

  private routeSub: Subscription;
  currentId:number

  constructor(private router:Router,private univezitetService:UniverzitetService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      this.getDrzave()
    });
  }

  potvrdiDrzavu(){
    this.potvrdjenaDrzava = true
    this.getMesta()
  }
  otkaziDrzavu(){
    this.potvrdjenaDrzava = false
    this.potvrdjenaAdresa = false
    this.potvrdjenoMesto = false
  }
  otkaziMesto(){
    this.potvrdjenoMesto = false
    this.potvrdjenaAdresa = false;
  }
  potvrdiMesto(){
    this.potvrdjenoMesto = true
    this.getAdrese()
  }
  potvrdiAdresu(){
    this.potvrdjenaAdresa = true
  }
  otkaziAdresu(){
    this.potvrdjenaAdresa = false
  }
  getDrzave() {
    this.univezitetService.dobaviDrzave().subscribe((response:any[])=>{
      this.drzave=response   
    }
    )
  }
  getMesta() {
    console.log(this.drzava.value);
    
    this.univezitetService.dobaviMesta(this.drzava.value).subscribe((response:any[])=>{
      this.mesta=response   
    }
    )
  }
  getAdrese() {
    this.univezitetService.dobaviAdrese(this.mesto.value).subscribe((response:any[])=>{
      this.adrese=response   
    }
    )
  }

  submit(){
    this.univezitetService.izmeniUniverzitet(this.currentId, this.forma.value.naziv, this.adresa.value).subscribe(r => {
      this.router.navigate(['/adminManager'])
    })
  }

  cancel(){
    this.router.navigate([`/adminManager`])
  }

}
