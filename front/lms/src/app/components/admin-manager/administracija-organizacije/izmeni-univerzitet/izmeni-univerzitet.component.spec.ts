import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniUniverzitetComponent } from './izmeni-univerzitet.component';

describe('IzmeniUniverzitetComponent', () => {
  let component: IzmeniUniverzitetComponent;
  let fixture: ComponentFixture<IzmeniUniverzitetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniUniverzitetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniUniverzitetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
