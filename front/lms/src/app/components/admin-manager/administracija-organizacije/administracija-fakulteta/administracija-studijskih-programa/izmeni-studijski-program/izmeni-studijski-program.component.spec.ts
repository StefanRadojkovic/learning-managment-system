import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniStudijskiProgramComponent } from './izmeni-studijski-program.component';

describe('IzmeniStudijskiProgramComponent', () => {
  let component: IzmeniStudijskiProgramComponent;
  let fixture: ComponentFixture<IzmeniStudijskiProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniStudijskiProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniStudijskiProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
