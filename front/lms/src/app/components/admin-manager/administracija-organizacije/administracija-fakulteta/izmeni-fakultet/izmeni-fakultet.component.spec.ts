import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniFakultetComponent } from './izmeni-fakultet.component';

describe('IzmeniFakultetComponent', () => {
  let component: IzmeniFakultetComponent;
  let fixture: ComponentFixture<IzmeniFakultetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniFakultetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniFakultetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
