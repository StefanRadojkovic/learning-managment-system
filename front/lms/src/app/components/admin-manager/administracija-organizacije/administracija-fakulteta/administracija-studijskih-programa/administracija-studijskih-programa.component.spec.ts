import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaStudijskihProgramaComponent } from './administracija-studijskih-programa.component';

describe('AdministracijaStudijskihProgramaComponent', () => {
  let component: AdministracijaStudijskihProgramaComponent;
  let fixture: ComponentFixture<AdministracijaStudijskihProgramaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaStudijskihProgramaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaStudijskihProgramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
