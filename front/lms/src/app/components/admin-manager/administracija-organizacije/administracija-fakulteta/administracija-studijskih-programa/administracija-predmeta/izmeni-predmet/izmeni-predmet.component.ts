import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-izmeni-predmet',
  templateUrl: './izmeni-predmet.component.html',
  styleUrls: ['./izmeni-predmet.component.css']
})
export class IzmeniPredmetComponent implements OnInit {

  predmet:any= {
    naziv: "",
    espb:null,
    brojPredavanja:null,
    brojVezbi:null,
    godina:null
    
  }

  forma = new FormGroup({
    naziv: new FormControl(null),
    espb: new FormControl(null),
    brojPredavanja: new FormControl(null),
    brojVezbi: new FormControl(null),
    godina: new FormControl(null),
    
  });
  godine: any[] = []

  private routeSub: Subscription;
  currentId:number

  constructor(private router:Router,private univezitetService:UniverzitetService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
    this.getGodina()
  }
  getGodina() {
    this.univezitetService.dobaviGodineIzmena(this.currentId).subscribe((response:any[])=>{
      this.godine=response
    }
    )
    
  }

  submit(){
    this.predmet.naziv = this.forma.value.naziv
    this.predmet.espb = this.forma.value.espb
    this.predmet.brojPredavanja = this.forma.value.brojPredavanja
    this.predmet.brojVezbi = this.forma.value.brojVezbi
    this.predmet.godina = this.forma.value.godina
    this.univezitetService.izmeniPredmetAdmin(this.currentId, this.predmet).subscribe(r => {
      this.router.navigate([`/administracijaPredmeta/1`])
    })
  }

  cancel(){
    this.router.navigate([`/administracijaPredmeta/1`])
  }

}
