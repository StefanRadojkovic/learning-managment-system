import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-izmeni-studijski-program',
  templateUrl: './izmeni-studijski-program.component.html',
  styleUrls: ['./izmeni-studijski-program.component.css']
})
export class IzmeniStudijskiProgramComponent implements OnInit {

  studijskiProgram:any= {
    naziv: "",
    opis:""
    
  }

  forma = new FormGroup({
    naziv: new FormControl(null),
    opis: new FormControl(null)
    
  });

  private routeSub: Subscription;
  currentId:number

  constructor(private router:Router,private univezitetService:UniverzitetService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
  }

  submit(){
    this.studijskiProgram.naziv = this.forma.value.naziv
    this.studijskiProgram.opis = this.forma.value.opis
    this.univezitetService.izmeniStudijskiProgram(this.currentId, this.studijskiProgram).subscribe(r => {
      this.router.navigate([`/adminstudijskiProgrami/1`])
    })
  }

  cancel(){
    this.router.navigate([`/adminstudijskiProgrami/1`])
  }

}
