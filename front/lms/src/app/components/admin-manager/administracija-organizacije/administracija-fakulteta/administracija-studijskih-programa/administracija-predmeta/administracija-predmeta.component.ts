import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-administracija-predmeta',
  templateUrl: './administracija-predmeta.component.html',
  styleUrls: ['./administracija-predmeta.component.css']
})
export class AdministracijaPredmetaComponent implements OnInit {

  predmet = {
    naziv: "",
    espb: null,
    brojPredavanja: null,
    brojVezbi:null,
    godina:null
  }
  elementi: any[] = []
  forma = new FormGroup({
    naziv: new FormControl(null),
    espb:new FormControl(null),
    brojPredavanja:new FormControl(null),
    brojVezbi:new FormControl(null),
    godina:new FormControl(null),
    
  });
  godine:any[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  private routeSub: Subscription;
  currentId:number
  neMoguceObrisati = false;
  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
    this.getGodina()
  }
  

  displayedColumns: string[] = ['ime','espb', 'brojVezbi','brojPredavanja','godina','akcije'];

  get() {
    this.univerzitetServis.dobaviPredmeteAdmin(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
      
    }
    )
    
  }
  getGodina() {
    this.univerzitetServis.dobaviGodine(this.currentId).subscribe((response:any[])=>{
      this.godine=response
    }
    )
    
  }

  izbrisi(id:number) {
    console.log("delete"+id);
    
    this.univerzitetServis.deletePredmetAdmin(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  submitForme(){
    console.log("SUBMIT");
    
    this.predmet.naziv = this.forma.value.naziv
    this.predmet.espb = this.forma.value.espb
    this.predmet.brojPredavanja = this.forma.value.brojPredavanja
    this.predmet.brojVezbi = this.forma.value.brojVezbi
    this.predmet.godina = this.forma.value.godina
    this.univerzitetServis.dodajPredmetAdmin(this.predmet, this.currentId).subscribe(r => {
      this.get()
    })
  }

  izmeni(id:number) {
    this.router.navigate([`/izmenaPredmeta/${id}`])
  }

  back(){
    this.router.navigate(["/adminstudijskiProgrami/1"])
  }

}
