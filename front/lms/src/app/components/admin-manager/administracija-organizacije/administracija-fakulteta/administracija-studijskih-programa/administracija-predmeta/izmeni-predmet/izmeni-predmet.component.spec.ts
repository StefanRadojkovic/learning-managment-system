import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniPredmetComponent } from './izmeni-predmet.component';

describe('IzmeniPredmetComponent', () => {
  let component: IzmeniPredmetComponent;
  let fixture: ComponentFixture<IzmeniPredmetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniPredmetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniPredmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
