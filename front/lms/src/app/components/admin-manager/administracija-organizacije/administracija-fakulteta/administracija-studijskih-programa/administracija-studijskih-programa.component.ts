import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-administracija-studijskih-programa',
  templateUrl: './administracija-studijskih-programa.component.html',
  styleUrls: ['./administracija-studijskih-programa.component.css']
})
export class AdministracijaStudijskihProgramaComponent implements OnInit {


  studijskiProgram = {
    naziv: "",
    opis: ""
  }
  elementi: any[] = []
  forma = new FormGroup({
    naziv: new FormControl(null),
    opis:new FormControl(null)
    
  });
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  private routeSub: Subscription;
  currentId:number
  neMoguceObrisati = false;
  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }
  

  displayedColumns: string[] = ['ime','opis', 'akcije'];

  get() {
    this.univerzitetServis.dobaviStudijskePrograme(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
      
    }
    )
    
  }

  izbrisi(id:number) {
    console.log("delete"+id);
    
    this.univerzitetServis.deleteStudijskiProgram(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  detaljnije(id:number){
    this.router.navigate([`/administracijaPredmeta/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  submitForme(){
    console.log("SUBMIT");
    
    this.studijskiProgram.naziv = this.forma.value.naziv
    this.studijskiProgram.opis = this.forma.value.opis
    this.univerzitetServis.dodajStudijskiProgram(this.currentId,this.studijskiProgram).subscribe(r => {
      this.get()
    })
  }

  izmeni(id:number) {
    this.router.navigate([`/izmenaStudijskogPrograma/${id}`])
  }

  back(){
    this.router.navigate(["/adminManager"])
  }

}

