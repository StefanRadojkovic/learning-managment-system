import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaPredmetaComponent } from './administracija-predmeta.component';

describe('AdministracijaPredmetaComponent', () => {
  let component: AdministracijaPredmetaComponent;
  let fixture: ComponentFixture<AdministracijaPredmetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaPredmetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaPredmetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
