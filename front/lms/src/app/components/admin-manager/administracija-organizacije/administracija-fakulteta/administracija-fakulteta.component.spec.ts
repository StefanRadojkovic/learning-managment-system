import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracijaFakultetaComponent } from './administracija-fakulteta.component';

describe('AdministracijaFakultetaComponent', () => {
  let component: AdministracijaFakultetaComponent;
  let fixture: ComponentFixture<AdministracijaFakultetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracijaFakultetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracijaFakultetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
