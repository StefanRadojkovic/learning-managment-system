import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-administracija-fakulteta',
  templateUrl: './administracija-fakulteta.component.html',
  styleUrls: ['./administracija-fakulteta.component.css']
})
export class AdministracijaFakultetaComponent implements OnInit {

  elementi: any[] = []
  drzave: any[] = []
  mesta: any[] = []
  adrese: any[] = []
  potvrdjenaDrzava = false;
  potvrdjenoMesto = false;
  potvrdjenaAdresa = false;
  drzava = new FormControl(null);
  mesto = new FormControl(null);
  adresa = new FormControl(null);
  forma = new FormGroup({
    naziv: new FormControl(null),
    
  });
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  private routeSub: Subscription;
  currentId:number
  neMoguceObrisati = false;

  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }


  displayedColumns: string[] = ['ime', 'akcije'];

  get() {
    this.univerzitetServis.dobaviFakultete(this.currentId).subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
      
    }
    )
    this.getDrzave()
    
  }
  potvrdiDrzavu(){
    this.potvrdjenaDrzava = true
    this.getMesta()
  }
  otkaziDrzavu(){
    this.potvrdjenaDrzava = false
    this.potvrdjenaAdresa = false
    this.potvrdjenoMesto = false
  }
  otkaziMesto(){
    this.potvrdjenoMesto = false
    this.potvrdjenaAdresa = false
  }
  potvrdiMesto(){
    this.potvrdjenoMesto = true
    this.getAdrese()
  }
  potvrdiAdresu(){
    this.potvrdjenaAdresa = true
  }
  otkaziAdresu(){
    this.potvrdjenaAdresa = false
  }

  getDrzave() {
    this.univerzitetServis.dobaviDrzave().subscribe((response:any[])=>{
      this.drzave=response   
    }
    )
  }
  getMesta() {
    console.log(this.drzava.value);
    
    this.univerzitetServis.dobaviMesta(this.drzava.value).subscribe((response:any[])=>{
      this.mesta=response   
    }
    )
  }
  getAdrese() {
    this.univerzitetServis.dobaviAdrese(this.mesto.value).subscribe((response:any[])=>{
      this.adrese=response   
    }
    )
  }


  delete(id:number) {
    this.univerzitetServis.deleteFakultet(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  detaljnije(id:number){
    this.router.navigate([`/adminstudijskiProgrami/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  submit(){
    this.univerzitetServis.dodajFakultet(this.currentId,this.forma.value.naziv, this.adresa.value).subscribe(r => {
      this.get()
    })
  }

  izmeni(id:number) {
    this.router.navigate([`/fakultet/${id}`])
  }

  back(){
    this.router.navigate(["/adminManager"])
  }

}
