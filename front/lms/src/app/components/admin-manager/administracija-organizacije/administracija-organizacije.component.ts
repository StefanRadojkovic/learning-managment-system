import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { UniverzitetService } from 'src/app/services/univerzitet/univerzitet.service';

@Component({
  selector: 'app-administracija-organizacije',
  templateUrl: './administracija-organizacije.component.html',
  styleUrls: ['./administracija-organizacije.component.css']
})
export class AdministracijaOrganizacijeComponent implements OnInit {

  elementi: any[] = []

  private routeSub: Subscription;
  currentId:number
  neMoguceObrisati = false
  drzave: any[] = []
  mesta: any[] = []
  adrese: any[] = []
  potvrdjenaDrzava = false;
  potvrdjenoMesto = false;
  potvrdjenaAdresa = false;

  forma = new FormGroup({
    naziv: new FormControl(null),
    
    
  });
  drzava = new FormControl(null);
  mesto = new FormControl(null);
  adresa = new FormControl(null);
  

  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private univerzitetServis:UniverzitetService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
    this.get()
    this.getDrzave()

  }
  potvrdiDrzavu(){
    this.potvrdjenaDrzava = true
    
    this.getMesta()
  }
  otkaziDrzavu(){
    this.potvrdjenaDrzava = false
    this.potvrdjenaAdresa = false
    this.potvrdjenoMesto = false
  }
  otkaziMesto(){
    this.potvrdjenoMesto = false
    this.potvrdjenaAdresa = false
  }
  potvrdiMesto(){
    this.potvrdjenoMesto = true
    this.getAdrese()
  }
  potvrdiAdresu(){
    this.potvrdjenaAdresa = true
  }
  otkaziAdresu(){
    this.potvrdjenaAdresa = false
  }

  submit(){
    this.univerzitetServis.dodajUniverzitet(this.forma.value.naziv, this.adresa.value).subscribe(r => {
      this.get()
    })
  }
  delete(id:number) {
    this.univerzitetServis.deleteUniverzitet(id).subscribe((r => {
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.neMoguceObrisati = true
      }
      
    });
    
  }

  detaljnije(id:number){
    this.router.navigate([`/fakultetiTabela/${id}`])
  }

  displayedColumns: string[] = ['ime', 'datumOsnivanja', 'akcije'];


  getDrzave() {
    this.univerzitetServis.dobaviDrzave().subscribe((response:any[])=>{
      this.drzave=response   
    }
    )
  }
  getMesta() {
    console.log(this.drzava.value);
    
    this.univerzitetServis.dobaviMesta(this.drzava.value).subscribe((response:any[])=>{
      this.mesta=response   
    }
    )
  }
  getAdrese() {
    this.univerzitetServis.dobaviAdrese(this.mesto.value).subscribe((response:any[])=>{
      this.adrese=response   
    }
    )
  }

  get() {
    this.univerzitetServis.dobaviUniverzitete().subscribe((response:any[])=>{
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
    }
    )
    
  }

  izmeni(id:number){
    this.router.navigate([`/izmeniUniverzitet/${id}`])
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
