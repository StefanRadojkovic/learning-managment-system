import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  role = []
  loggedIn = false;
  roleStudent = false
  roleNastavnik = false;
  roleAdmin = false
  constructor(private loginService:LoginService, private router:Router) { }

  ngOnInit(): void {
    this.checkIfLoggedIn();
  }

  checkIfLoggedIn(){
    if(this.loginService.user != null){
      this.loggedIn = true
      this.checkRoleLoggedIn()
    }
  }

  studentManager(){
    this.router.navigate(["/studentManager"])
  }
  nastavnikManager(){
    this.router.navigate(["/nastavnikManager"])
  }
  checkRoleLoggedIn(){
    this.role = this.loginService.user["roles"]
    
    console.log(this.role);
    
    if(this.role.includes("ROLE_STUDENT")){
      this.roleStudent = true
    }
    if(this.role.includes("ROLE_NASTAVNIK")){
      this.roleNastavnik = true
    }
    if(this.role.includes("ROLE_ADMIN")){
      this.roleAdmin = true
    }
  }
  home(){
    this.router.navigate(["/home"])
  }
  register(){
    this.router.navigate(["/register"])
  }
  adminManager(){
    this.router.navigate(["/adminManager"])
  }

  uredi(){
    this.router.navigate(["/uredi"])
  }
  login(){
    this.router.navigate(["/login"])
  }

  odjava(){
    this.loginService.logout()
    this.router.navigate(["/login"])
    
    
  }

}
