import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog,MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import { RegisterService } from 'src/app/services/register/register.service';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  @Input()
  user:any= {
    username: "",
    password: "",
    ime: "",
    jmbg:"",
    roles: []
  }

  forma = new FormGroup({
    username: new FormControl(null),
    password: new FormControl(null),
    ime: new FormControl(null),
    jmbg: new FormControl(null),
    roles:new FormControl(null)
  });

  constructor(private registerService:RegisterService,private loginService:LoginService, private router:Router,public dialog: MatDialog) { }

  ngOnInit(): void {
    console.log(this.loginService.user);
    
    console.log(this.user.username);
    
    
  }

  submit(){
    
    if(this.forma.value.password == null){
      this.user.password = this.loginService.user.password
    }else{
      this.user.password = this.forma.value.password
    }
    if(this.forma.value.ime == null){
      this.user.ime = this.loginService.user.ime
    }else{
      this.user.ime = this.forma.value.ime
    }
    if(this.forma.value.jmbg == null){
      this.user.jmbg = this.loginService.user.jmbg
    }else{
      this.user.jmbg = this.forma.value.jmbg
    }
    console.log(this.user);
    
    
    
    this.user.roles =this.loginService.user.roles
    this.user.username = this.loginService.user.username 
    this.registerService.updateKorisnika(this.loginService.user.id,this.user).subscribe(r => {
      this.router.navigate(['/home'])
    })
  }

  openDialog(){
    this.dialog.open(DialogComponent)
  }

  cancel(){
    this.router.navigate(["/home"])
  }

}
