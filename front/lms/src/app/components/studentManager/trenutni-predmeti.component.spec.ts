import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrenutniPredmetiComponent } from './trenutni-predmeti.component';

describe('TrenutniPredmetiComponent', () => {
  let component: TrenutniPredmetiComponent;
  let fixture: ComponentFixture<TrenutniPredmetiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrenutniPredmetiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrenutniPredmetiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
