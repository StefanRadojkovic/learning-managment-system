import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Predmet } from 'src/app/model/predmet';
import { LoginService } from 'src/app/services/login/login.service';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-istorija-studiranja',
  templateUrl: './istorija-studiranja.component.html',
  styleUrls: ['./istorija-studiranja.component.css']
})
export class IstorijaStudiranjaComponent implements OnInit {
  @Input()
  elementi: any[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  
  prosecnaOcena:number
  ukupanBrojEspbBodova:number = 0
  ukupneOcene:number = 0

  constructor(private predmetiServis:PredmetiService, private router:Router,private loginService:LoginService) { }

  ngOnInit(): void {
    this.predmetiServis.dobaviPolozenePredmete(this.loginService.user.id).subscribe((predmeti:any[])=>{
      this.elementi=predmeti
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      this.racunanjeEspbBodova()
      this.racunanjeProsecneOcene()
      
    }
    )
    
    
  }

  displayedColumns: string[] = ['ime', 'ocena', 'brojPolaganja','espb'];

  racunanjeProsecneOcene(){
    for(let element of this.elementi){
      this.ukupneOcene = this.ukupneOcene + element.konacnaOcena
    }
    this.prosecnaOcena = this.ukupneOcene/this.elementi.length
    
    
  }
  racunanjeEspbBodova(){
    for(let element of this.elementi){
      this.ukupanBrojEspbBodova += element.espb
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
