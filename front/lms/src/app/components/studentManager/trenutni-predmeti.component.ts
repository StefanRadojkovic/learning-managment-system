import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Predmet } from 'src/app/model/predmet';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-trenutni-predmeti',
  templateUrl: './trenutni-predmeti.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./trenutni-predmeti.component.css']
  
})
export class TrenutniPredmetiComponent implements OnInit {

  @Input()
  elementi: any[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;


  

  constructor(private predmetiServis:PredmetiService, private router:Router,private loginService:LoginService){}

  ngOnInit(): void {
    this.predmetiServis.dobaviSveTrenutnoSlusani(this.loginService.user.id).subscribe((response:any[])=>{
      console.log(response);
      this.elementi=response
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
    }
    )
    
  }

  //Zasto ovde ne mogu da dodelim data source u html kao dataSource nego moram direkt elementi
  displayedColumns = ['naziv','espb','brojPredavanja','brojVezbi',];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue.trim().toLowerCase()+"filter")
    console.log(this.dataSource.filter +"datasource");
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  home(){
    this.router.navigate(["/home"])
  }
}

