import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LoginService } from 'src/app/services/login/login.service';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-obavestenja',
  templateUrl: './obavestenja.component.html',
  styleUrls: ['./obavestenja.component.css']
})
export class ObavestenjaComponent implements OnInit {

  @Input()
  elementi: any[] = []


  constructor(private predmetiServis:PredmetiService, private loginService:LoginService) { }

  ngOnInit(): void {
    this.predmetiServis.dobaviObavestenja(this.loginService.user.id).subscribe((response:any[])=>{
      this.elementi=response
      console.log(this.elementi);
      
    }
    )
  }


}