import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  forma = new FormGroup({
    username: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  });
  loginFailed = false;

  constructor(private loginService:LoginService, private router:Router) { }

  ngOnInit(): void {
  }

  submit(){
    this.loginService.login(this.forma.value).subscribe(r => {
      this.router.navigate(['/home'])//ovde cemo dodati home page da se navigira na njega nakon logina
    }, (err) => {
      this.loginFailed = true;
    });
  }

  register(){
    this.router.navigate(["/register"]) //ovde cemo staviti rutu za registraciju
  }

  home(){
    this.router.navigate(["/home"])
  }
}
