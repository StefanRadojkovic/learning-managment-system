import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-pretrazi-studente',
  templateUrl: './pretrazi-studente.component.html',
  styleUrls: ['./pretrazi-studente.component.css']
})
export class PretraziStudenteComponent implements OnInit {

  forma = new FormGroup({
    ime: new FormControl(null),
    brojIndeksa: new FormControl(null),
    godinaUpisa: new FormControl(null),
    prosecnaOcena: new FormControl(null),
  });
  studenti: any[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private predmetiServis:PredmetiService) { }

  displayedColumns: string[] = ['ime', 'brojIndeksa', 'godinaUpisa', 'prosecnaOcena'];

  ngOnInit(): void {
    this.submit()
  }

  submit(){
    this.predmetiServis.dobaviPretrazeneStudente(this.forma.value).subscribe((studenti:any[])=>{
      this.studenti=studenti
      this.dataSource = new MatTableDataSource(this.studenti)
      this.dataSource.paginator = this.paginator;
      
      
    }
    )
  }

}
