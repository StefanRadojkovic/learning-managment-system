import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PretraziStudenteComponent } from './pretrazi-studente.component';

describe('PretraziStudenteComponent', () => {
  let component: PretraziStudenteComponent;
  let fixture: ComponentFixture<PretraziStudenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PretraziStudenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PretraziStudenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
