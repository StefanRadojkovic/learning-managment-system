import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Predmet } from 'src/app/model/predmet';
import { LoginService } from 'src/app/services/login/login.service';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-nastavnik-manager',
  templateUrl: './nastavnik-manager.component.html',
  styleUrls: ['./nastavnik-manager.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class NastavnikManagerComponent implements OnInit {

  @Input()
  elementi: Predmet[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;
  

  


  constructor(private predmetiServis:PredmetiService, private router:Router, private loginService:LoginService){}

  ngOnInit(): void {
    this.predmetiServis.dobaviTrenutnePredmeteNastavnik(this.loginService.user.id).subscribe((predmeti:Predmet[])=>{
      this.elementi=predmeti
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
      
    }
    )
    //console.log(this.elementi);
    
  }

  //Zasto ovde ne mogu da dodelim data source u html kao dataSource nego moram direkt elementi
  columnsToDisplay = ['Ime'];
  columnsToDisplayWithExpand = [...this.columnsToDisplay, 'expand'];
  expandedElement: Predmet | null;
  displayedColumns: string[] = ['ime', 'brojIndeksa', 'godinaUpisa', 'prosecnaOcena'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  home(){
    this.router.navigate(["/home"])
  }
  urediSilabusPredavanja(id:number,predmet:Predmet){
    this.router.navigate([`/urediSilabusPredavanja/${id}`])
  }
  urediSilabusVezbi(id:number,predmet:Predmet){
    this.router.navigate([`/urediSilabusVezbi/${id}`])
  }

  urediObavestenja(id:number){
    this.router.navigate([`/urediObavestenja/${id}`])
  }

  pregledajStudente(id:number){
    this.router.navigate([`/studentiTabela/${id}`])
  }

  instrumentEvaluacije(id:number){
    this.router.navigate([`/instrumentEvaluacije/${id}`])
  }

}
