import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentiNaPredmetuTabelaComponent } from './studenti-na-predmetu-tabela.component';

describe('StudentiNaPredmetuTabelaComponent', () => {
  let component: StudentiNaPredmetuTabelaComponent;
  let fixture: ComponentFixture<StudentiNaPredmetuTabelaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentiNaPredmetuTabelaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentiNaPredmetuTabelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
