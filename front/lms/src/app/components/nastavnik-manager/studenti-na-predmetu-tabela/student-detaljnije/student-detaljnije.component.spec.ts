import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentDetaljnijeComponent } from './student-detaljnije.component';

describe('StudentDetaljnijeComponent', () => {
  let component: StudentDetaljnijeComponent;
  let fixture: ComponentFixture<StudentDetaljnijeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentDetaljnijeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentDetaljnijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
