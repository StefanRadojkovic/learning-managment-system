import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-student-detaljnije',
  templateUrl: './student-detaljnije.component.html',
  styleUrls: ['./student-detaljnije.component.css']
})
export class StudentDetaljnijeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router:Router, private predmetService:PredmetiService) { }


  @Input()
  elementi:any[] = []
  private routeSub: Subscription;
  currentId:number
  delimiter = this.router.url.indexOf("/student/");
  ruta = this.router.url.slice(0,this.delimiter)

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }

  

  back(){
    this.router.navigate(["/nastavnikManager"])
  }

  get(){
    this.predmetService.dobaviPolozenePredmete(this.currentId).subscribe((x=>{
      this.elementi = x
      console.log(this.elementi);
    }))
  }

}
