import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-studenti-na-predmetu-tabela',
  templateUrl: './studenti-na-predmetu-tabela.component.html',
  styleUrls: ['./studenti-na-predmetu-tabela.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class StudentiNaPredmetuTabelaComponent implements OnInit {


  @Input()
  elementi:any[] = []
  dataSource: MatTableDataSource<any>;
  @ViewChild('paginator') paginator: MatPaginator;

  private routeSub: Subscription;
  currentId:number
  ocene:number[] = [6,7,8,9,10]
  ocena = new FormControl(null);

  constructor(private predmetService:PredmetiService,private route: ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }
  back(){
    this.router.navigate(["/nastavnikManager"])
  }

  upisiOcenu(id:number){
    console.log(this.ocena.value);
    console.log(id);
    console.log(this.currentId);
    
    this.predmetService.upisiOcenu(id,this.ocena.value,this.currentId).subscribe((x=>{
      this.get()
      
    }))
    
  }

  columnsToDisplay = ['Ime', 'Prezime', 'Broj Indeksa', 'Prosecna Ocena'];
  displayedColumns: string[] = ['ime', 'username', 'jmbg', 'brojIndeksa'];
  columnsToDisplayWithExpand = [...this.columnsToDisplay, 'expand'];
  expandedElement: any | null;

  detaljnije(id:number){
    this.router.navigate([`studentiTabela/${id}/student/${id}`])
  }

  get(){
    this.predmetService.dobaviStudente(this.currentId).subscribe((x=>{
      this.elementi=x
      console.log(this.elementi);
      
      this.dataSource = new MatTableDataSource(this.elementi)
      this.dataSource.paginator = this.paginator;
      
    }))
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
