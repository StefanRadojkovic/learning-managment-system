import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikManagerComponent } from './nastavnik-manager.component';

describe('NastavnikManagerComponent', () => {
  let component: NastavnikManagerComponent;
  let fixture: ComponentFixture<NastavnikManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NastavnikManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavnikManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
