import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Predmet } from 'src/app/model/predmet';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-instrument-evaluacije',
  templateUrl: './instrument-evaluacije.component.html',
  styleUrls: ['./instrument-evaluacije.component.css']
})
export class InstrumentEvaluacijeComponent implements OnInit {

  @Input()
  elementi: any[] = []

  evaluacije: string[] = ['Projektni Zadatak', 'Test', 'Kolokvijumski Zadatak'];

  forma = new FormGroup({
    opis: new FormControl(null,Validators.required),
    instrument: new FormControl(null, Validators.required),
  });

  evaluacija: any = {
    opis: "",
    instrument: "",
    predmetId:null
  }

  private routeSub: Subscription;
  currentId:number

  constructor(private predmetServis:PredmetiService, private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()
  }

  displayedColumns: string[] = ['opis','instrument',"akcije"];
  dataSource = new MatTableDataSource(this.elementi);

  get() {
    console.log(this.currentId);
    
    this.predmetServis.dobaviInstrumentEvaluacije(this.currentId).subscribe((x=>{
      this.elementi = x
    }))
  }

  delete(id:number){
    this.predmetServis.deleteInstrumentEvaluacije(id).subscribe((x=>{
      this.get()
    }))
  }
  update(id:number){
    this.router.navigate([`/urediInsturmentEvaluacije/${id}`])
  }

  back(){
    this.router.navigate([`/nastavnikManager`])
  }
  submit(){
    this.evaluacija.opis = this.forma.value.opis
    this.evaluacija.instrument = this.forma.value.instrument
    this.evaluacija.predmetId = this.currentId
    this.predmetServis.dodajInstrumentEvaluacije(this.evaluacija).subscribe((x=>{
      this.get()
    }))
  }

}
