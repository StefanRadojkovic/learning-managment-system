import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstrumentEvaluacijeComponent } from './instrument-evaluacije.component';

describe('InstrumentEvaluacijeComponent', () => {
  let component: InstrumentEvaluacijeComponent;
  let fixture: ComponentFixture<InstrumentEvaluacijeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstrumentEvaluacijeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstrumentEvaluacijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
