import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-izmena-instrumenta-evaluacije',
  templateUrl: './izmena-instrumenta-evaluacije.component.html',
  styleUrls: ['./izmena-instrumenta-evaluacije.component.css']
})
export class IzmenaInstrumentaEvaluacijeComponent implements OnInit {

  forma = new FormGroup({
    opis: new FormControl(null, Validators.required),
    instrument: new FormControl(null, Validators.required)
  });

  private routeSub: Subscription;
  currentId:number

  constructor(private predmetServis:PredmetiService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log(params['id']) //log the value of id
      this.currentId = params['id']
    });
  }

  submit(){
    this.predmetServis.updateInstrumentEvaluacije(this.currentId,this.forma.value).subscribe(r => {
      this.router.navigate(['/nastavnikManager'])
    }, (err) => {
      console.log(err);
      
    });
  }
  odustani(){
    this.router.navigate(["/instrumentEvaluacije"])
  }

}
