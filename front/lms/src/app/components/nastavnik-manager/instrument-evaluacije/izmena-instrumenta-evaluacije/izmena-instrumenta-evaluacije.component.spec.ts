import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmenaInstrumentaEvaluacijeComponent } from './izmena-instrumenta-evaluacije.component';

describe('IzmenaInstrumentaEvaluacijeComponent', () => {
  let component: IzmenaInstrumentaEvaluacijeComponent;
  let fixture: ComponentFixture<IzmenaInstrumentaEvaluacijeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmenaInstrumentaEvaluacijeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmenaInstrumentaEvaluacijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
