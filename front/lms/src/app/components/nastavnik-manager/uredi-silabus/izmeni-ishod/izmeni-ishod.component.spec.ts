import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniIshodComponent } from './izmeni-ishod.component';

describe('IzmeniIshodComponent', () => {
  let component: IzmeniIshodComponent;
  let fixture: ComponentFixture<IzmeniIshodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniIshodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniIshodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
