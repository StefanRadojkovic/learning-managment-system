import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-izmeni-ishod',
  templateUrl: './izmeni-ishod.component.html',
  styleUrls: ['./izmeni-ishod.component.css']
})
export class IzmeniIshodComponent implements OnInit {

  forma = new FormGroup({
    opis: new FormControl(null, Validators.required),
    termin: new FormControl(null, Validators.required)
  });

  private routeSub: Subscription;
  currentId:number
  postojiTermin:boolean = false;

  delimiter = this.router.url.indexOf("/urediIshod/");
  ruta = this.router.url.slice(0,this.delimiter)

  constructor(private predmetServis:PredmetiService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log(params['id']) //log the value of id
      this.currentId = params['id']
    });
  }

  submitPredavanja(){
    this.predmetServis.updateIshod(this.currentId,this.forma.value).subscribe(r => {
      this.router.navigate(['/urediSilabusPredavanja'])
    }, (err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiTermin = true
      }
      
    });
  }
  odustani(){
    this.router.navigate([this.ruta])
  }

}
