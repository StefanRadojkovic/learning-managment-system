import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrediSilabusComponent } from './uredi-silabus.component';

describe('UrediSilabusComponent', () => {
  let component: UrediSilabusComponent;
  let fixture: ComponentFixture<UrediSilabusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrediSilabusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrediSilabusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
