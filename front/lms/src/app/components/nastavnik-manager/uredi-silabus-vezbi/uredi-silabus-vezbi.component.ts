import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-uredi-silabus-vezbi',
  templateUrl: './uredi-silabus-vezbi.component.html',
  styleUrls: ['./uredi-silabus-vezbi.component.css']
})
export class UrediSilabusVezbiComponent implements OnInit {

  @Input()
  elementi:any[] = []

  forma = new FormGroup({
    opis: new FormControl(null,Validators.required),
    termin: new FormControl(null, Validators.required),
  });

  ishod: any = {
    opis: "",
    termin: null,
    predmetId:null,
    isPredavanje:false
  }

  private routeSub: Subscription;
  currentId:number
  postojiTermin:boolean = false;
  
  constructor(private predmetServis:PredmetiService, private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.get()
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
      console.log(params['id']);
      
    });
    
  }
  displayedColumns: string[] = ['opis', 'termin', 'akcije'];

  delete(id:number){
    this.predmetServis.deleteIshod(id).subscribe(x=>{
      this.get()
    })
    
  }

  update(element:any, id:number){
    this.router.navigate([`/urediIshodVezbi/${id}`])
  }
  get(){
    this.predmetServis.dobaviIshodeVezbe(1).subscribe((ishodi:any[]) =>{
      this.elementi = ishodi
    })
  }
  back(){
    this.router.navigate(["/nastavnikManager"])
  }

  submit(){
    this.ishod.opis = this.forma.value.opis
    this.ishod.termin = this.forma.value.termin
    this.ishod.predmetId = this.currentId
    console.log(this.ishod);
    
    this.predmetServis.dodajIshod(this.ishod).subscribe((x=>{
      this.get()
    }),(err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiTermin = true
      }
    }
      )
  }

}

