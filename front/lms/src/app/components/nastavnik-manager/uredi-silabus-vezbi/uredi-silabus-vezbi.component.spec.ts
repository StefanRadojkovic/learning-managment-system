import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrediSilabusVezbiComponent } from './uredi-silabus-vezbi.component';

describe('UrediSilabusVezbiComponent', () => {
  let component: UrediSilabusVezbiComponent;
  let fixture: ComponentFixture<UrediSilabusVezbiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrediSilabusVezbiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrediSilabusVezbiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
