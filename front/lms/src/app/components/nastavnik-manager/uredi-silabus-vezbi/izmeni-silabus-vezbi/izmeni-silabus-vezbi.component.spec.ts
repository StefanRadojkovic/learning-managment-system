import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniSilabusVezbiComponent } from './izmeni-silabus-vezbi.component';

describe('IzmeniSilabusVezbiComponent', () => {
  let component: IzmeniSilabusVezbiComponent;
  let fixture: ComponentFixture<IzmeniSilabusVezbiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniSilabusVezbiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniSilabusVezbiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
