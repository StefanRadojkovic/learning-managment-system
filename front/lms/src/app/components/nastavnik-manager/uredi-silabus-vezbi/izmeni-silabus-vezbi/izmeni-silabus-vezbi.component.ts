import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-izmeni-silabus-vezbi',
  templateUrl: './izmeni-silabus-vezbi.component.html',
  styleUrls: ['./izmeni-silabus-vezbi.component.css']
})
export class IzmeniSilabusVezbiComponent implements OnInit {

  forma = new FormGroup({
    opis: new FormControl(null, Validators.required),
    termin: new FormControl(null, Validators.required)
  });

  private routeSub: Subscription;
  currentId:number
  postojiTermin:boolean = false;

  constructor(private predmetServis:PredmetiService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log(params['id']) //log the value of id
      this.currentId = params['id']
    });
  }

  submitVezbi(){
    this.predmetServis.updateIshod(this.currentId,this.forma.value).subscribe(r => {
    }, (err) => {
      console.log(err);
      if(err.status == "400"){
        this.postojiTermin = true
      }
      
    });
  }
  odustani(){
    this.router.navigate(["/nastavnikManager"])
  }

}
