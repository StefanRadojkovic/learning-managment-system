import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-nastavnik-obavestenja',
  templateUrl: './nastavnik-obavestenja.component.html',
  styleUrls: ['./nastavnik-obavestenja.component.css']
})
export class NastavnikObavestenjaComponent implements OnInit {

  @Input()
  elementi:any[] = []

  forma = new FormGroup({
    opis: new FormControl(null,Validators.required),
    title: new FormControl(null, Validators.required),
  });

  obavestenje: any = {
    opis: "",
    title: "",
    predmetId:null,
  }

  private routeSub: Subscription;
  currentId:number
  
  constructor(private predmetServis:PredmetiService, private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
    this.get()

    
  }
  displayedColumns: string[] = ['title', 'opis', 'akcije'];

  delete(id:number){
    this.predmetServis.deleteObavestenja(id).subscribe(x=>{
      this.get()
    })
    
  }

  update(element:any, id:number){
    this.router.navigate([`/izmeniObavestenje/${id}`])
  }
  get(){
    console.log(this.currentId);
    this.predmetServis.dobaviObavestenjaPredmeta(this.currentId).subscribe((obavestenja:any[]) =>{
      this.elementi = obavestenja
    })
  }
  back(){
    this.router.navigate(["/nastavnikManager"])
  }
  submit(){
    this.obavestenje.opis = this.forma.value.opis
    this.obavestenje.title = this.forma.value.title
    
    
    this.obavestenje.predmetId = this.currentId
    
    this.predmetServis.dodajObavestenja(this.obavestenje).subscribe((x=>{
      this.get()
    }))
  }

}
