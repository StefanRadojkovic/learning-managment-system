import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikObavestenjaComponent } from './nastavnik-obavestenja.component';

describe('NastavnikObavestenjaComponent', () => {
  let component: NastavnikObavestenjaComponent;
  let fixture: ComponentFixture<NastavnikObavestenjaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NastavnikObavestenjaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavnikObavestenjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
