import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmeniObavestenjaComponent } from './izmeni-obavestenja.component';

describe('IzmeniObavestenjaComponent', () => {
  let component: IzmeniObavestenjaComponent;
  let fixture: ComponentFixture<IzmeniObavestenjaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmeniObavestenjaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmeniObavestenjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
