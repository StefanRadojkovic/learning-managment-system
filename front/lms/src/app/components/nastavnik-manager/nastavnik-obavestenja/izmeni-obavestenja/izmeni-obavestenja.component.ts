import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PredmetiService } from 'src/app/services/predmeti/predmeti.service';

@Component({
  selector: 'app-izmeni-obavestenja',
  templateUrl: './izmeni-obavestenja.component.html',
  styleUrls: ['./izmeni-obavestenja.component.css']
})
export class IzmeniObavestenjaComponent implements OnInit {

  forma = new FormGroup({
    opis: new FormControl(null, Validators.required),
    title: new FormControl(null, Validators.required)
  });

  obavestenje: any = {
    opis: "",
    title: "",
  }

  private routeSub: Subscription;
  currentId:number

  constructor(private predmetServis:PredmetiService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id']
    });
  }

  submitPredavanja(){
    this.obavestenje.opis = this.forma.value.opis
    this.obavestenje.title = this.forma.value.title
    this.predmetServis.updateObavestenja(this.currentId,this.obavestenje).subscribe(r => {
      this.router.navigate([`/nastavnikManager`])
    }, (err) => {
      console.log(err);
      
    });
  }
  odustani(){
    this.router.navigate([`/nastavnikManager`])
  }

}
